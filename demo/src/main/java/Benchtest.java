
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.engine.software.CPUPainter2D;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.ColorRGB;


public class Benchtest {

    public static void main(String[] args) {

        Color scrollAreaBackgroundColor = new Color(200, 200, 200, 10);
        Color scrollAreaBorderColor = new Color(100, 100, 100, 100);
        BufferedImage bufferedImage = new BufferedImage(1024, 768, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2dScrollArea = (Graphics2D) bufferedImage.getGraphics();
        long start, total;
        for(int i=0;i<3;i++) {
            start = System.nanoTime();
            g2dScrollArea.setColor(scrollAreaBackgroundColor);
            g2dScrollArea.fillRect(0, 0, bufferedImage.getWidth(), bufferedImage.getHeight());
            g2dScrollArea.setColor(scrollAreaBorderColor);
            g2dScrollArea.drawLine(bufferedImage.getWidth() - 1, 0, bufferedImage.getWidth() - 1, bufferedImage.getHeight());
            total = System.nanoTime() - start;
            System.out.println(total + " ns");
        }

        System.out.println("-----------------");

        final science.unlicense.image.api.color.Color scrollAreaBackgroundColor2 = new ColorRGB(200, 200, 200, 10);
        final science.unlicense.image.api.color.Color scrollAreaBorderColor2 = new ColorRGB(100, 100, 100, 100);
        final int width = 1024;
        final int height = 768;
        final Image img = Images.create(new Extent.Long(width, height),Images.IMAGE_TYPE_RGBA);
        final Painter2D painter = new CPUPainter2D(img);
        painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_BUTT));
        for(int i=0;i<3;i++) {
            start = System.nanoTime();
            painter.setPaint(new ColorPaint(scrollAreaBackgroundColor2));
            painter.fill(new Rectangle(0, 0, width,height));
            painter.setPaint(new ColorPaint(scrollAreaBorderColor2));
            painter.stroke(new DefaultLine(width-1, 0, width-1, height));
            total = System.nanoTime() - start;
            System.out.println(total + " ns");
        }

    }

}
