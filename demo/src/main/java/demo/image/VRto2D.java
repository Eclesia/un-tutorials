
package demo.image;

import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.transform.PanaromicTo2DTransform;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.geometry.api.tuple.TupleSpace;
import science.unlicense.geometry.api.tuple.TupleSpaceCursor;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.process.ExtrapolatorWrap;
import science.unlicense.image.api.process.InterpolatorLinear;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.transform.CartesianToSphereTransform;

/**
 *
 * @author Johann Sorel
 */
public class VRto2D {

    private static final CartesianToSphereTransform XYZ_TO_RADIAN = new CartesianToSphereTransform(1);

    public static void main(String[] args) throws IOException {


        final Image image = Images.read(Paths.resolve(new Chars("/home/eclesia/360.png")));
        final TupleGrid srcGrid = image.getTupleBuffer(image.getColorModel());


        final int width = 1280;
        final int height = 960;

        final MonoCamera camera = new MonoCamera();
        camera.setFieldOfView(60);
        camera.setRenderArea(new Rectangle(0, 0, width, height));
        camera.setCameraType(MonoCamera.TYPE_ORTHO);

        double forwardX = 0.0;

        for(int x = 0;x<360;x+=10) {
            forwardX = x-180;

            final Matrix3x3 m = Matrix3x3.createRotation3(Math.toRadians(forwardX), camera.getUpAxis());
            camera.getNodeTransform().getRotation().set(m);
            camera.getNodeTransform().notifyChanged();

            Transform trs = PanaromicTo2DTransform.create(srcGrid.getExtent(), camera, camera.getNodeToRootSpace());


            final Image res = Images.create(image, new long[]{width,height});
            TupleGrid outGrid = res.getTupleBuffer(res.getColorModel());

            TupleSpace ex = new ExtrapolatorWrap(srcGrid);
            ex = new InterpolatorLinear(ex);
            final TupleSpaceCursor srcCursor = ex.cursor();

            final TupleGridCursor dstCursor = outGrid.cursor();
            final Vector2f64 srcPos = new Vector2f64();
            while (dstCursor.next()) {
                Tuple dstPos = dstCursor.coordinate();

                trs.transform(dstPos, srcPos);

                srcCursor.moveTo(srcPos);
                dstCursor.samples().set(srcCursor.samples());
            }

//            Images.write(res, new Chars("png"), Paths.resolve(new Chars("/home/eclesia/360/view"+x+".png")));
        }


    }

}

