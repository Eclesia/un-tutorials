
package demo.image;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Float32;
import science.unlicense.display.api.desktop.FrameManagers;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WGraphicImage;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.impl.process.noise.SimplexNoise;
import science.unlicense.math.impl.Vector2i32;

/**
 * Create an image and fill it with a simplex noise.
 *
 * @author Johann Sorel
 */
public class SimplexNoiseDemo {

    public static void main(String[] args) {

        final Image image = createSimplexNoiseImage(200, 200, 16);

        final UIFrame frame = (UIFrame) FrameManagers.getFrameManager().createFrame(false);
        final WButton lbl = new WButton(new Chars("test"));
        lbl.setGraphic(new WGraphicImage(image));

        frame.getContainer().setLayout(new BorderLayout());
        frame.getContainer().addChild(lbl, BorderConstraint.CENTER);

        frame.setSize(640, 640);
        frame.setVisible(true);
    }

    public static Image createSimplexNoiseImage(int width, int height, int nbWaves){
        final Image image = Images.createCustomBand(new Extent.Long(width, height), 1, Float32.TYPE);
        final TupleGridCursor cursor = image.getRawModel().asTupleBuffer(image).cursor();

        for (int x = 0; x < width; x++){
            for (int y = 0; y < height; y++){
                double noise = SimplexNoise.noise(
                        nbWaves * x / (double)width,
                        nbWaves * y / (double)height);
                //remove this line for real values
                //we offset the values to be in 0-1 range for the colormap
                noise = (noise +1) /2;
                cursor.moveTo(new Vector2i32(x,y));
                cursor.samples().set(0, (float)noise);
            }
        }
        return image;
    }

}
