
package demo.image;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.DefaultDocument;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.process.AbstractImageTaskDescriptor;
import science.unlicense.image.impl.process.detection.CannyOperator;
import science.unlicense.task.api.Task;
import science.unlicense.task.api.TaskDescriptor;
import science.unlicense.task.api.Tasks;

/**
 *
 * @author Johann Sorel
 */
public class CannyDemo {

    public static void main(String[] args) throws Exception {

        final Chars inputFile = new Chars("file:/...");
        final Chars outputFile = new Chars("file:/...");

        Image image = Images.read(Paths.resolve(inputFile));

        final TaskDescriptor desc = Tasks.getDescriptor(new Chars("canny"));
        final Document param = new DefaultDocument(desc.getInputType());
        param.setPropertyValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId(),image);
        param.setPropertyValue(CannyOperator.INPUT_LOWER_THRESHOLD.getId(),2.5f);
        param.setPropertyValue(CannyOperator.INPUT_HIGHER_THRESHOLD.getId(),7.5f);
        param.setPropertyValue(CannyOperator.INPUT_GAUSSIAN_KERNEL_RADIUS.getId(),2f);
        param.setPropertyValue(CannyOperator.INPUT_GAUSSIAN_KERNEL_WIDTH.getId(),16);
        param.setPropertyValue(CannyOperator.INPUT_CONTRAST_NORMALIZED.getId(),false);

        final long before = System.currentTimeMillis();
        final Task op = desc.create();
        op.inputs().set(param);
        Document result = op.perform();
        final Image outimage = (Image) result.getPropertyValue(AbstractImageTaskDescriptor.OUTPUT_IMAGE.getId());
        final long after = System.currentTimeMillis();
        System.out.println("time : "+(after-before)+" ms");

        Images.write(outimage, new Chars("png"), Paths.resolve(outputFile));
    }

}
