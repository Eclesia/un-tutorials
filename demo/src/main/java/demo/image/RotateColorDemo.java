package demo.image;

import science.unlicense.common.api.character.Chars;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.impl.process.paint.RotateColorOperator;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class RotateColorDemo {

    public static void main(String[] args) throws Exception {

        final Chars inputFile = new Chars("file:/...");
        final Chars outputFile = new Chars("file:/...");

        Image image = Images.read(Paths.resolve(inputFile));

        final long before = System.currentTimeMillis();
        image = new RotateColorOperator().execute(image, 120,1f,1f);
        final long after = System.currentTimeMillis();
        System.out.println("time : " + (after - before) + " ms");

        Images.write(image, new Chars("png"), Paths.resolve(outputFile));
    }
}
