
package demo.image;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.impl.process.Carving;
import science.unlicense.concurrent.api.Paths;

/**
 * Demonstration of how to use seam carving on images.
 *
 * @author Johann Sorel
 */
public class CarvingDemo {

    public static void main(String[] args) throws IOException {

        //read source image
        final Image image = Images.read(Paths.resolve(new Chars("file:/...path to original image")));

        //carve image
        final Carving carving = new Carving(image);
        carving.reduceHeight(20);
        final Image carvedImage = carving.getImage();

        //write result image
        Images.write(carvedImage, new Chars("png"), Paths.resolve(new Chars("file:/... path to carved image")));

    }

}
