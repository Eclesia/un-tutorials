
package demo.image;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.image.api.ImageReader;
import science.unlicense.image.api.Images;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.store.Resources;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.image.api.ImageResource;

/**
 * Showing how to access image metadatas.
 *
 * @author Johann Sorel
 */
public class MetadataDemo {

    public static void main(String[] args) throws IOException, StoreException {

        /**
        Metadatas describe the content of the image, each format has it's own
        internal model. Therefor most of the time you will find 2 metadatas.
        - the standard model common to all images
        - the inner format model, which contains more information but has a specific structure.
         */
        final Store store = Images.open(Paths.resolve(new Chars("mod:/images/GRACE_globe_animation.gif")));
        final ImageResource res = (ImageResource) Resources.findFirst(store, ImageResource.class);
        final ImageReader reader = res.createReader();

        //this image has two metamodels
        final Chars[] metamodelNames = reader.getMetadataNames();
        for(int i=0;i<metamodelNames.length;i++){
            final Node metadata = reader.getMetadata(metamodelNames[i]).toNode();
            System.out.println("---------------------------------------");
            System.out.println(metadata);
        }

    }

}
