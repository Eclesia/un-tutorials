
package demo.image;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.DefaultDocument;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.process.AbstractImageTaskDescriptor;
import science.unlicense.image.impl.process.analyze.ImageTraceOperator;

/**
 *   ImageTracer.java (Desktop version with javax.imageio. See ImageTracerAndroid.java for the Android version.) 
 *   Simple raster image tracer and vectorizer written in Java. This is a port of imagetracer.js. 
 *   by András Jankovics 2015
 *   andras@jankovics.net
 *   
 *   Tips:
 *    - color quantization uses randomization to recycle little used colors when colorquantcycles > 1 , 
 *      which means that output will be different every time. Use colorquantcycles = 1 to get deterministic output.
 *    - palette generation uses randomization to make some of the colors. Set numberofcolors to a qubic number 
 *      (e.g. 8, 27 or 64) to get a deterministic palette, or use a custom palette.
 *    - ltres,qtres : lower linear (ltres) and quadratic spline (qtres) error tresholds result 
 *      more details at the cost of longer paths (more segments). Usually 0.5 or 1 is good for both. When tracing
 *      round shapes, lower ltres, e.g. ltres=0.2 qtres=1 will result more curves, thus maybe better quality. Similarly,
 *      ltres=1 qtres=0 is better for polygonal shapes. Values greater than 2 will usually result inaccurate paths.
 *    - pathomit : the length of the shortest path is 4, four corners around one pixel. The default pathomit=8 filters out
 *      isolated one and two pixels for noise reduction. This can be deactivated by setting pathomit=0.
 *    - pal, numberofcolors : custom palette in the format pal=[colornum][4]; or automatic palette 
 *      with the given length. Many colors will result many layers, so longer processing time and more paths, but better 
 *      quality. When using few colors, more colorquantcycles can improve quality.
 *    - mincolorratio : minimum ratio of pixels, below this the color will be randomized. mincolorratio=0.02 for a 10*10 image
 *      means that if a color has less than 10*10 * 0.02 = 2 pixels, it will be randomized.
 *    - colorquantcycles : color quantization will be repeated this many times. When using many colors, this can be a lower value.
 *    - scale : optionally, the SVG output can be scaled, scale=2 means the SVG will have double height and width
 *   
 *   Process overview
 *   ----------------
 *   
 *   // 1. Color quantization
 *   // 2. Layer separation and edge detection
 *   // 3. Batch pathscan
 *   // 4. Batch interpollation
 *   // 5. Batch tracing
 *   // 6. Creating SVG string
 *
 * Origin :
 * https://github.com/jankovicsandras/imagetracerjava
 * 
 * @author jankovics andras (andras@jankovics.net)
 */
public class ImageTracerDemo{
        
    public static void main(String[] args) throws IOException{
        
        final Path pathImg = Paths.resolve(new Chars("file:..."));
        final Path pathSvg = pathImg.getParent().resolve(pathImg.getName().concat(new Chars(".svg")));
        
        final Image img = Images.read(pathImg);
        
        final ImageTraceOperator op = new ImageTraceOperator();
        final Document params = new DefaultDocument(ImageTraceOperator.DESCRIPTOR.getInputType());
        params.setPropertyValue(AbstractImageTaskDescriptor.INPUT_IMAGE.getId(),img);
        params.setPropertyValue(ImageTraceOperator.INPUT_NUMBER_OF_COLORS.getId(),64);
        op.inputs().set(params);
        final Document res = op.perform();
        final String svg = (String) res.getPropertyValue(ImageTraceOperator.OUTPUT_SVG.getId());
        
        final ByteOutputStream out = pathSvg.createOutputStream();
        out.write(svg.getBytes());
        out.flush();
        out.close();        
    }
    
}