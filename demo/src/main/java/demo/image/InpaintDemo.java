package demo.image;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.engine.software.CPUPainter2D;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.impl.process.paint.InpaintOperator;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class InpaintDemo {

    public static void main(String[] args) throws Exception {

        final Chars inputFile = new Chars("file:/...");
        final Chars outputFile = new Chars("file:/...");

        Image image = Images.read(Paths.resolve(inputFile));

        //create a mask
        Image mask = Images.create(image.getExtent(),Images.IMAGE_TYPE_RGB);
        final Rectangle rect = new Rectangle(275, 700, 100, 50);
        final Painter2D painter = new CPUPainter2D(mask);
        painter.setPaint(new ColorPaint(Color.WHITE));
        painter.fill(rect);

        final long before = System.currentTimeMillis();
        image = new InpaintOperator().execute(image, mask, 10);
        final long after = System.currentTimeMillis();
        System.out.println("time : " + (after - before) + " ms");

        Images.write(image, new Chars("png"), Paths.resolve(outputFile));
    }
}
