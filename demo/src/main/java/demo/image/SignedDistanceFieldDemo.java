
package demo.image;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.impl.process.distance.EDTDistanceOperator;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class SignedDistanceFieldDemo {
    
    public static void main(String[] args) throws IOException {
        
        final Path imagePath = Paths.resolve(new Chars("file:..."));
        final Path distImagePath = imagePath.getParent().resolve(new Chars("dist.png"));
        final Image image = Images.read(imagePath);
        final Image distImage = new EDTDistanceOperator().execute(image);
        
        Images.write(distImage, new Chars("png"), distImagePath);
        
    }
    
}
