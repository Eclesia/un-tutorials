
package demo.image;

import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.software.CPUPainter2D;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.impl.Circle;
import science.unlicense.geometry.impl.DefaultTriangle;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.impl.Affine2;
import science.unlicense.math.impl.Vector2f64;

/**
 * This demo shows how to render images, textes and graphics to an image even
 * when a GPU isn't available.
 *
 * @author Johann Sorel
 */
public class CPURendering {

    public static void main(String[] args) throws IOException {

        final Image image = Images.create(new Extent.Long(800, 600),Images.IMAGE_TYPE_RGB);

        final CPUPainter2D painter = new CPUPainter2D(image);

        //geometries
        painter.setPaint(new ColorPaint(Color.RED));
        painter.fill(new DefaultTriangle(new Vector2f64(150, 50), new Vector2f64(10, 200), new Vector2f64(300, 200)));
        painter.setPaint(new ColorPaint(Color.GREEN));
        painter.fill(new Rectangle(100, 150, 200, 400));
        painter.setPaint(new ColorPaint(Color.BLUE));
        painter.fill(new Circle(250, 250, 100));

        //text
        painter.setPaint(new ColorPaint(Color.WHITE));
        painter.setFont(new FontChoice(new Chars[]{new Chars("Tuffy")}, 60, FontChoice.WEIGHT_NONE));
        painter.fill(new Chars("Hello Jogamp comrades !"), 300, 140);
        painter.setFont(new FontChoice(new Chars[]{new Chars("Tuffy")}, 30, FontChoice.WEIGHT_NONE));
        painter.fill(new Chars("Hello Jogamp comrades !"), 300, 200);
        painter.setFont(new FontChoice(new Chars[]{new Chars("Tuffy")}, 12, FontChoice.WEIGHT_NONE));
        painter.fill(new Chars("Hello Jogamp comrades !"), 300, 240);
        painter.setFont(new FontChoice(new Chars[]{new Chars("Tuffy")}, 8, FontChoice.WEIGHT_NONE));
        painter.fill(new Chars("Hello Jogamp comrades !"), 300, 280);

        //image
        final Image icon = Images.read(Paths.resolve(new Chars("mod:/images/tree.png")));
        painter.paint(icon, new Affine2(
                0.5, 0, 400,
                0, 0.5, 200));

        //ensure everything is painted
        painter.flush();
        painter.dispose();

        Images.write(image, new Chars("bmp"), Paths.resolve(new Chars("file:./cpuimage.bmp")));


    }

}
