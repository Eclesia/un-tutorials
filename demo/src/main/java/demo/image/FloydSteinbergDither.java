
package demo.image;

import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.geometry.api.tuple.TupleGrid;
import science.unlicense.geometry.api.tuple.TupleGridCursor;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.ColorRW;
import science.unlicense.image.api.color.ColorSystem;
import science.unlicense.image.api.color.Colors;
import science.unlicense.image.api.model.ImageModel;
import science.unlicense.image.impl.algorithm.FloydSteinbergDither.RGBTriple;
import static science.unlicense.image.impl.algorithm.FloydSteinbergDither.floydSteinbergDither;
import science.unlicense.math.impl.Vector2i32;

/**
 * Dthering demo.
 *
 * Origin :
 * http://en.literateprograms.org/index.php?title=Special:DownloadCode/Floyd-Steinberg_dithering_(Java)&oldid=12476
 *
 */
public class FloydSteinbergDither {


    public static void main(String[] args) throws IOException {

        final Image img = Images.read(Paths.resolve(new Chars(args[0])));
        final ImageModel cm = img.getColorModel();
        final ColorSystem cs = (ColorSystem) cm.getSampleSystem();
        final TupleGridCursor cursor = cm.asTupleBuffer(img).cursor();
        final ColorRW cursorColor = Colors.castOrWrap(cursor.samples(), cs);


        final TupleGrid pixels = img.getColorModel().asTupleBuffer(img);
        final RGBTriple[][] image = new RGBTriple[(int)pixels.getExtent().getL(0)][(int)pixels.getExtent().getL(1)];

        for (int y = 0; y < image.length; y++) {
            for (int x = 0; x < image[0].length; x++) {
                image[y][x] = new RGBTriple();
                cursor.moveTo(new Vector2i32(x,y));
                final int argb = cursorColor.toARGB();
                int[] array = Colors.toRGB(argb, null);
                image[y][x].channels[0] = (byte) array[0];
                image[y][x].channels[1] = (byte) array[1];
                image[y][x].channels[2] = (byte) array[2];
            }
        }

        RGBTriple[] palette = {
            new RGBTriple(149, 91, 110),
            new RGBTriple(176, 116, 137),
            new RGBTriple(17, 11, 15),
            new RGBTriple(63, 47, 69),
            new RGBTriple(93, 75, 112),
            new RGBTriple(47, 62, 24),
            new RGBTriple(76, 90, 55),
            new RGBTriple(190, 212, 115),
            new RGBTriple(160, 176, 87),
            new RGBTriple(116, 120, 87),
            new RGBTriple(245, 246, 225),
            new RGBTriple(148, 146, 130),
            new RGBTriple(200, 195, 180),
            new RGBTriple(36, 32, 27),
            new RGBTriple(87, 54, 45),
            new RGBTriple(121, 72, 72)
        };

        byte[][] result = floydSteinbergDither(image, palette);

        ByteOutputStream raw_out = Paths.resolve(new Chars(args[1])).createOutputStream();
        for (int y = 0; y < result.length; y++) {
            for (int x = 0; x < result[y].length; x++) {
                raw_out.write(palette[result[y][x]].channels, 0, 3);
            }
        }
        raw_out.close();
    }

}
