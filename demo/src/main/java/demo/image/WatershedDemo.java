
package demo.image;

import science.unlicense.image.impl.process.watershed.WatershedGrayLevel;

/**
 *
 * http://www.developpez.net/forums/d746465/autres-langages/algorithmes/contribuez/image-watershed-partage-eaux-niveaux-gris/
 * 
 * @author Xavier Philippeau
 */
public class WatershedDemo {

    public static void main(String[] args) {

        //table with data values between 0 and 255
        int[][] image = null;
        //image dimension
        int width = 0;
        int height = 0;
        //true if searching for bright objects on dark surface, otherwise false
        boolean isBrightOnDark = true;

        final WatershedGrayLevel ws = new WatershedGrayLevel(image,width,height,isBrightOnDark);

        //number of neighbor pixel to explore at each fill
        int step = 2;

        //result, value is true if is a watershed separation
        boolean shed[][] = ws.process(step);


    }

}
