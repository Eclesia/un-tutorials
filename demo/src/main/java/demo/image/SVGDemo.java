

package demo.image;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.desktop.FrameManagers;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.painter2d.ImagePainter2D;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.painter2d.Painters;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WGraphicImage;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.format.svg.RenderState;
import science.unlicense.format.svg.SVGReader;
import science.unlicense.format.svg.model.SVGDocument;
import science.unlicense.format.svg.model.SVGElement;
import science.unlicense.format.svg.model.SVGGraphic;
import science.unlicense.geometry.api.BBox;
import science.unlicense.image.api.Image;

/**
 * Read and render an SVG as image.
 * @author Johann Sorel
 */
public class SVGDemo {

    public static void main(String[] args) throws IOException {

        final SVGReader reader = new SVGReader();
        reader.setInput(Paths.resolve(new Chars("mod:/images/dessin.svg")));
        final SVGDocument doc = reader.read();

        //find the image best size
        final BBox bbox = new BBox(2);
        bbox.setRange(0, 0, 1000);
        bbox.setRange(1, 0, 1000);

        final ImagePainter2D painter = Painters.getPainterManager().createPainter((int)bbox.getSpan(0), (int)bbox.getSpan(1));
        render(painter, doc);

        painter.flush();
        final Image image = painter.getImage();

        final UIFrame frame = (UIFrame) FrameManagers.getFrameManager().createFrame(false);
        final WContainer container = frame.getContainer();
        container.setLayout(new BorderLayout());
        final WLabel label = new WLabel();
        label.setGraphic(new WGraphicImage(image));
        container.addChild(label, BorderConstraint.CENTER);

        frame.setSize(800, 600);
        frame.setVisible(true);

    }

    private static void render(Painter2D painter, SVGElement ele){

        if(ele instanceof SVGGraphic){
            final SVGGraphic graphic = (SVGGraphic) ele;
            final RenderState state = graphic.buildRenderState();
            if(state.fillGeometry!=null){
                painter.setPaint(state.fillPaint);
                painter.fill(state.fillGeometry);
            }
            if(state.strokeGeometry!=null){
                painter.setPaint(state.strokePaint);
                painter.setBrush(state.strokeBrush);
                painter.stroke(state.strokeGeometry);
            }
        }

        final Iterator ite = ele.getChildren().createIterator();
        while (ite.hasNext()) {
            render(painter, (SVGElement)ite.next());
        }
    }

}
