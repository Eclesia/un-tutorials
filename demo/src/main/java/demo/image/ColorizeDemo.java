package demo.image;

import science.unlicense.common.api.character.Chars;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.impl.process.paint.ColorizeOperator;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class ColorizeDemo {

    public static void main(String[] args) throws Exception {

        final Chars inputFile = new Chars("file:/...");
        final Chars outputFile = new Chars("file:/...");

        Image image = Images.read(Paths.resolve(inputFile));

        final long before = System.currentTimeMillis();
        image = new ColorizeOperator().execute(image, 120f,0.5f,0.5f);
        final long after = System.currentTimeMillis();
        System.out.println("time : " + (after - before) + " ms");

        Images.write(image, new Chars("png"), Paths.resolve(outputFile));
    }
}
