
package demo.geo;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.swing.SwingFrameManager;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.visual.WidgetView;
import science.unlicense.engine.ui.widget.WLeaf;
import science.unlicense.format.geojson.GeoJsonConstants;
import science.unlicense.format.geojson.GeoJsonReader;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.impl.TransformedGeometry;
import science.unlicense.geometry.impl.transform.ViewTransform;
import science.unlicense.geospatial.transform.spherical.MercatorTransform;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.Angles;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.transform.ConcatenateTransform;
import science.unlicense.math.api.transform.Transform;
import science.unlicense.math.impl.Affine2;

/**
 *
 * @author Johann Sorel
 */
public class WMap {

    public static void main(String[] args) throws IOException {

        final ViewTransform view = new ViewTransform();
        view.scaleAt(100, new double[2]);

        final UIFrame frame = SwingFrameManager.INSTANCE.createFrame(false);
        frame.getContainer().setLayout(new BorderLayout());

        final WLeaf map = new WLeaf() {};

        final Transform degreeToRadians = new Affine2(Angles.degreeToRadian(1),0,0,0,-Angles.degreeToRadian(1),0);
        final Transform geotrs = new MercatorTransform(0);
        final Affine2 viewTrs = new Affine2();
        final Transform concat = ConcatenateTransform.create(degreeToRadians, ConcatenateTransform.create(geotrs, viewTrs));

        final Path geojsonPath = Paths.resolve(new Chars("file:/./countries.geo.json"));
        map.setView(new WidgetView(map){
            protected void renderSelf(Painter2D painter, BBox innerBBox) {
                painter = painter.derivate(true);
                painter.setPaint(new ColorPaint(Color.BLUE));
                painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_BUTT));

                viewTrs.fromMatrix(view.getSourceToTarget().toMatrix());

                try {
                    final GeoJsonReader reader = new GeoJsonReader();
                    reader.setInput(geojsonPath);

                    while(reader.hasNext()){
                        Document doc = reader.next();

                        PlanarGeometry geom = (PlanarGeometry) doc.getPropertyValue(GeoJsonConstants.PROP_GEOMETRY);
                        geom = TransformedGeometry.create(geom, concat);

                        painter.stroke(geom);
                    }
                    reader.dispose();

                } catch(Exception ex) {
                    ex.printStackTrace();
                }

            }
        });

        frame.getContainer().addChild(map, BorderConstraint.CENTER);


        final boolean[] pressed = new boolean[]{false};
        final double[] mvCoord = new double[]{Double.NaN, Double.NaN};

        map.addEventListener(KeyMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                KeyMessage km = (KeyMessage) event.getMessage();
                if (km.getType() == KeyMessage.TYPE_PRESS) {
                    pressed[0] = true;
                } else if(km.getType() == KeyMessage.TYPE_RELEASE) {
                    pressed[0] = false;
                }
            }
        });

        map.addEventListener(MouseMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                final MouseMessage km = (MouseMessage) event.getMessage();
                final Tuple pos = km.getMousePosition();

                if (km.getType() == MouseMessage.TYPE_ENTER) {
                    map.requestFocus();
                } else if (km.getType() == MouseMessage.TYPE_PRESS) {
                    mvCoord[0] = pos.get(0);
                    mvCoord[1] = pos.get(1);
                } else if(km.getType() == MouseMessage.TYPE_RELEASE) {
                    mvCoord[0] = Double.NaN;
                    mvCoord[1] = Double.NaN;
                } else if(km.getType() == MouseMessage.TYPE_MOVE) {
                    if (Double.isNaN(mvCoord[0])) return;
                    final double[] diff = new double[]{
                        pos.get(0)-mvCoord[0],
                        pos.get(1)-mvCoord[1]
                    };
                    view.translate(diff);
                    mvCoord[0] = pos.get(0);
                    mvCoord[1] = pos.get(1);
                    map.setDirty();
                } else if(km.getType() == MouseMessage.TYPE_WHEEL) {
                    if(pressed[0]) {
                        view.rotateAt(km.getWheelOffset()*0.1, new double[]{pos.get(0),pos.get(1)});
                    } else {
                        view.scaleAt(1+km.getWheelOffset()*-0.1, new double[]{pos.get(0),pos.get(1)});
                    }
                    map.setDirty();
                }
            }
        });

        frame.setSize(800, 600);
        frame.setVisible(true);

    }

}
