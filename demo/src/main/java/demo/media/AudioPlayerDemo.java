

package demo.media;

import com.jogamp.openal.AL;
import java.nio.ByteBuffer;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.number.Endianness;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.display.api.desktop.FrameManagers;
import science.unlicense.display.api.layout.GridLayout;
import science.unlicense.encoding.api.io.ArrayOutputStream;
import science.unlicense.encoding.api.io.DataOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Resources;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.engine.jogl.openal.resource.ALBuffer;
import science.unlicense.engine.jogl.openal.resource.ALSource;
import science.unlicense.engine.ui.component.path.WPathChooser;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.media.api.AudioPacket;
import science.unlicense.media.api.AudioSamples;
import science.unlicense.media.api.AudioTrack;
import science.unlicense.media.api.Media;
import science.unlicense.media.api.MediaPacket;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.media.api.Medias;

/**
 * Play an audio file.
 * See list of media formats.
 *
 * @author Johann Sorel
 */
public class AudioPlayerDemo {

    public static void main(String[] args) throws IOException, StoreException {

        final Path path = WPathChooser.showOpenDialog((Predicate)null);

        final Store store = Medias.open(path);
        final Media media = (Media) Resources.findFirst(store, Media.class);

        //tranform audio in a supported byte buffer
        final AudioTrack meta = (AudioTrack) media.getTracks()[0];
        System.out.println(Arrays.toChars(meta.getChannels()));
        final MediaReadStream reader = media.createReader(null);

        //recode stream in a stereo 16 bits per sample.
        final ArrayOutputStream out = new ArrayOutputStream();
        final DataOutputStream ds = new DataOutputStream(out, Endianness.LITTLE_ENDIAN);

        for(MediaPacket pack=reader.next();pack!=null;pack=reader.next()){
            final AudioSamples samples = ((AudioPacket) pack).getSamples();
            final int[] audiosamples = samples.asPCM(null, 16);
            ds.writeUShort(audiosamples[0]);
            ds.writeUShort(audiosamples[1]);
        }



        final byte[] all = out.getBuffer().toArrayByte();

        //buffer which contains audio datas
        final ALBuffer data = new ALBuffer();
        data.setFormat(AL.AL_FORMAT_STEREO16);
        data.setFrequency((int) meta.getSampleRate());
        data.setSize(all.length);
        data.setData(ByteBuffer.wrap(all));
        data.load();

        //the audio source
        final ALSource source = new ALSource();
        source.setBuffer(data);
        source.load();

        //simple frame for play/pause/stop actions
        final UIFrame frame = (UIFrame) FrameManagers.getFrameManager().createFrame(false);
        final WContainer panel = frame.getContainer();
        panel.setLayout(new GridLayout(1, 3));
        panel.addChild(new WButton(new Chars("Play"), null,new EventListener() {
            public void receiveEvent(Event event) {
                source.play();
            }
        }), null);

        panel.addChild(new WButton(new Chars("Pause"), null,new EventListener() {
            public void receiveEvent(Event event) {
                source.pause();
            }
        }), null);

        panel.addChild(new WButton(new Chars("Stop"), null,new EventListener() {
            public void receiveEvent(Event event) {
                source.stop();
            }
        }), null);
        frame.setVisible(true);

    }

}
