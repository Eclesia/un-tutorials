
package demo.media;

import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.mesh.MediaPlane;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.software.CPUPainter2D;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.media.api.MediaReadParameters;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.media.impl.inmemory.MemoryImageSerie;
import science.unlicense.media.impl.inmemory.MemoryMediaStore;

/**
 * Demonstrate how to create a video from images.
 *
 * @author Johann Sorel
 */
public class VideoFromImagesDemo {

    public static void main(String[] args) {

        //create a in memory media storage
        final MemoryMediaStore store = new MemoryMediaStore();

        //create a video stream and add it in the store
        final MemoryImageSerie video = new MemoryImageSerie();
        store.getStreams().add(video);

        //set the video framerate
        video.setTimeSpacing(1000); //one image per second

        //generate multiple images
        for(int i=0;i<30;i++){
            final Image image = buildImage();
            video.getImages().add(image);
        }

        //create a media reader
        final MediaReadParameters mediaParams = new MediaReadParameters();
        mediaParams.setStreamIndexes(new int[]{0});
        final MediaReadStream videoReader = store.createReader(mediaParams);

        //play our video
        playVideo(videoReader);

    }

    /**
     * Create a random color image.
     */
    private static Image buildImage(){
        final Image image = Images.create(new Extent.Long(400, 400),Images.IMAGE_TYPE_RGBA);
        final Painter2D painter = new CPUPainter2D(image);

        painter.setPaint(new ColorPaint(
                new ColorRGB((float)Math.random(), (float)Math.random(), (float)Math.random())));
        final Rectangle rec = new Rectangle(0, 0, 400, 400);
        painter.fill(rec);
        return image;
    }

    private static void playVideo(MediaReadStream videoReader){

        //create a rendering frame
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(true);
        //build a small scene
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        scene.getChildren().add(camera);
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new RenderPhase(scene,camera,null));

        //create a media plane
        final MediaPlane plane = new MediaPlane(videoReader);
        plane.getNodeTransform().getScale().localScale(20);
        plane.getNodeTransform().notifyChanged();
        scene.getChildren().add(plane);

        //keyboard and mouse control
        final OrbitUpdater control = new OrbitUpdater(frame,camera,plane);
        control.configureDefault();
        plane.getUpdaters().add(control);

        //show the frame
        frame.setSize(640, 480);
        frame.setVisible(true);
    }

}
