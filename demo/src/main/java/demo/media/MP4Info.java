package demo.media;

import java.util.List;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.format.mpeg4.MP4Store;
import science.unlicense.format.mpeg4.api.MetaData;
import science.unlicense.format.mpeg4.api.Movie;
import science.unlicense.format.mpeg4.api.Protection;
import science.unlicense.format.mpeg4.api.Track;
import science.unlicense.format.mpeg4.boxes.Box;

/**
 *
 * @author Alexander Simm
 */
public class MP4Info {

    private static final String USAGE = "usage:\nnet.sourceforge.jaad.MP4Info [options] <infile>\n\n\t-b\talso print all boxes";

    public static void main(String[] args) {
        try {
            if(args.length<1) printUsage();
            else {
                boolean boxes = false;
                final String file;
                if(args.length>1) {
                    if(args[0].equals("-b")) boxes = true;
                    else printUsage();
                    file = args[1];
                }
                else file = args[0];

                final MP4Store cont = new MP4Store(Paths.resolve(new Chars(file)));
                final Movie movie = cont.getMovie();
                System.out.println("Movie:");

                final List<Track> tracks = movie.getTracks();
                Track t;
                for(int i = 0; i<tracks.size(); i++) {
                    t = tracks.get(i);
                    System.out.println("\tTrack "+i+": "+t.getCodec()+" (language: "+t.getLanguage()+", created: "+t.getCreationTime()+")");

                    final Protection p = t.getProtection();
                    if(p!=null) System.out.println("\t\tprotection: "+p.getScheme());
                }

                if(movie.containsMetaData()) {
                    System.out.println("\tMetadata:");
                    final Dictionary data = movie.getMetaData().getAll();
                    final Iterator ite = data.getKeys().createIterator();
                    while (ite.hasNext()) {
                        MetaData.Field<?> key = (MetaData.Field<?>) ite.next();
                        if(key.equals(MetaData.Field.COVER_ARTWORKS)) {
                            final List<?> l = (List<?>) data.getValue(MetaData.Field.COVER_ARTWORKS);
                            System.out.println("\t\t"+l.size()+" Cover Artworks present");
                        }
                        else System.out.println("\t\t"+key.getName()+" = "+data.getValue(key));
                    }
                }

                final List<Protection> protections = movie.getProtections();
                if(protections.size()>0) {
                    System.out.println("\tprotections:");
                    for(Protection p : protections) {
                        System.out.println("\t\t"+p.getScheme());
                    }
                }

                //print all boxes
                if(boxes) {
                    System.out.println("================================");
                    for(Box box : cont.getBoxes()) {
                        printBox(box, 0);
                    }
                }
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            System.err.println("error while reading file: "+e.toString());
        }
    }

    private static void printUsage() {
        System.out.println(USAGE);
        System.exit(1);
    }

    private static void printBox(Box box, int level) {
        System.out.println(box.toString());
    }
}
