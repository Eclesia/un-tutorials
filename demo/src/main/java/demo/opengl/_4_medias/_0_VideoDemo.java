
package demo.opengl._4_medias;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.media.api.MediaReadParameters;
import science.unlicense.media.api.MediaReadStream;
import science.unlicense.media.api.Medias;
import science.unlicense.engine.opengl.mesh.MediaPlane;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.math.api.Angles;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.store.Resources;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.media.api.Media;

/**
 * Minimal scene demo, displaying an image and basic gesture control.
 *
 * @author Johann Sorel
 */
public class _0_VideoDemo {

    public static void main(String[] args) throws IOException, StoreException {

        //create a rendering frame
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera));

        //open a video media
        final Store store = Medias.open(Paths
                .resolve(new Chars("mod:/images/GRACE_globe_animation.gif")));
        final Media media = (Media) Resources.findFirst(store, Media.class);
        final MediaReadParameters mediaParams = new MediaReadParameters();
        mediaParams.setStreamIndexes(new int[]{0});
        final MediaReadStream videoReader = media.createReader(mediaParams);

        //create a media plane
        final MediaPlane plane = new MediaPlane(videoReader);
        plane.getNodeTransform().getScale().localScale(20);
        plane.getNodeTransform().notifyChanged();
        scene.getChildren().add(plane);

        //keyboard and mouse control
        final OrbitUpdater controller = new OrbitUpdater(frame,camera,plane).configureDefault();
        plane.getChildren().add(camera);
        controller.setDistance(40);
        controller.setHorizontalAngle(Angles.degreeToRadian(30));
        controller.setVerticalAngle(Angles.degreeToRadian(25));
        camera.getUpdaters().add(controller);


        //show the frame
        frame.setSize(640, 480);
        frame.setVisible(true);
    }

}
