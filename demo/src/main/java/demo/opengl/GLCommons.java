
package demo.opengl;

import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.buffer.Float32Cursor;
import science.unlicense.common.api.buffer.Int32Cursor;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.model3d.impl.material.Material;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 *
 * @author Johann Sorel
 */
public class GLCommons {

    public static Material createMaterial(Color color) {
        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuse(Color.WHITE);
        return material;
    }

    public static Material createMaterial(Texture2D texture) {
        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuseTexture(new Layer(new UVMapping(texture)));
        return material;
    }

    public static ColorRGB randomColor(){
        return new ColorRGB((float)Math.random(), (float)Math.random(), (float)Math.random(), 1f);
    }

    /**
     * Build a grid mesh. standard ground for demos.
     * @return
     */
    public static DefaultModel buildGround(){
        final Float32Cursor vertices = DefaultBufferFactory.INSTANCE.createFloat32(100*2*2*3).cursor();
        final Float32Cursor normals = DefaultBufferFactory.INSTANCE.createFloat32(100*2*2*3).cursor();
        final Int32Cursor indexes = DefaultBufferFactory.INSTANCE.createInt32(100*2*2*2).cursor();

        final float[] normal = new float[]{0,0,1};
        int i=0;
        for(int x=-50;x<50;x++){
            vertices.write(x).write(0).write(-50);
            vertices.write(x).write(0).write(+50);
            normals.write(normal);
            normals.write(normal);
            indexes.write(i++);
            indexes.write(i++);
        }
        for(int z=-50;z<50;z++){
            vertices.write(-50).write(0).write(z);
            vertices.write(+50).write(0).write(z);
            normals.write(normal);
            normals.write(normal);
            indexes.write(i++);
            indexes.write(i++);
        }

        final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
        shell.setPositions(new VBO(vertices.getBuffer(), 3));
        shell.setNormals(new VBO(normals.getBuffer(), 3));
        shell.setIndex(new IBO(indexes.getBuffer()));
        shell.setRanges(new IndexedRange[]{IndexedRange.LINES(0, (int) indexes.getBuffer().getNumbersSize())});

        final DefaultModel mesh = new DefaultModel();
        mesh.setShape(shell);
        mesh.getMaterials().add(GLCommons.createMaterial(Color.WHITE));

        return mesh;
    }

    public static SceneNode buildCubesScene(){
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);

        //build a small scene
        for(int i=0;i<20;i++){
            final DefaultModel box = new DefaultModel(new BBox(new double[]{-2,-2,-2},new double[]{2,2,2}));
            box.getNodeTransform().getTranslation().setXYZ(
                    Math.random()*20-10,
                    Math.random()*20-10,
                    Math.random()*20-10);
            box.getNodeTransform().notifyChanged();
            box.getNodeTransform().notifyChanged();
            box.getMaterials().add(GLCommons.createMaterial(randomColor()));
            scene.getChildren().add(box);
        }

        return scene;
    }

}
