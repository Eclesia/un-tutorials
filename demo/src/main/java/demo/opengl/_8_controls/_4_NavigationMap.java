
package demo.opengl._8_controls;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.NodeVisitor;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.light.DirectionalLight;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.control.PlayerNavigationMapConstraint;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.engine.opengl.technique.GLTechnique;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.math.api.Angles;
import science.unlicense.model3d.api.Model3Ds;
import science.unlicense.model3d.api.SceneResource;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.model3d.impl.navmap.MeshNavMap;
import science.unlicense.model3d.impl.navmap.NavMap;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 * Example of a navigation map to limit camera movements.
 *
 * @author Johann Sorel
 */
public class _4_NavigationMap {

    public static void main(String[] args) throws IOException, StoreException {

        //create a rendering frame
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera));


        //build the scene
        final DirectionalLight light = new DirectionalLight();
        light.getNodeTransform().getTranslation().setXYZ(0, 0, 6);
        scene.getChildren().add(light);

        final DefaultModel[] map = new DefaultModel[1];
        final Store store = Model3Ds.open(Paths.resolve(new Chars("mod:/store/model3d/navmap.obj")));
        final SceneResource res = (SceneResource) store;
        final Iterator ite = res.getElements().createIterator();
        while(ite.hasNext()){
            final Object candidate = ite.next();
            if(candidate instanceof SceneNode){
                final SceneNode node = (SceneNode) candidate;
                NodeVisitor visitor = new NodeVisitor(){
                    public Object visit(Node node, Object context) {
                        if(node instanceof DefaultModel){
                            map[0] = (DefaultModel) node;
                            ((GLTechnique)map[0].getTechniques().get(0)).getState().setPolygonMode(GLC.POLYGON_MODE.LINE);
                        }
                        return super.visit(node, context);
                    }
                };
                visitor.visit(node, null);

                scene.getChildren().add((SceneNode)candidate);
            }
        }

        //keyboard and mouse control
        final MotionModel crosshair = DefaultMotionModel.createCrosshair3D();
        crosshair.getChildren().add(camera);
        scene.getChildren().add(crosshair);

        final OrbitUpdater controller = new OrbitUpdater(frame,camera,crosshair).configureDefault();
        controller.setDistance(20);
        controller.setHorizontalAngle(Angles.degreeToRadian(30));
        controller.setVerticalAngle(Angles.degreeToRadian(25));
        camera.getUpdaters().add(controller);

        //navigation map constraint
        final NavMap navmap = new MeshNavMap(map[0]);
        final PlayerNavigationMapConstraint cst = new PlayerNavigationMapConstraint(navmap,crosshair);
        camera.getUpdaters().add(cst);


        //show the frame
        frame.setSize(640, 480);
        frame.setVisible(true);
    }

}
