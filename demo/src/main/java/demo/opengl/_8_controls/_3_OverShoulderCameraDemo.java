
package demo.opengl._8_controls;

import demo.opengl.GLCommons;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.impl.DisplayTimerState;
import science.unlicense.display.impl.GestureTrigger;
import science.unlicense.display.impl.updater.FirstPersonUpdater;
import science.unlicense.display.impl.updater.Updater;
import science.unlicense.geometry.api.BBox;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.scene.DefaultModel;

/**
 * Camera placed over the shoulder of the controler object.
 * here we control a small cube.
 *
 * @author Johann Sorel
 */
public class _3_OverShoulderCameraDemo extends AbstractControlDemo {

    public static void main(String[] args) {
        new _3_OverShoulderCameraDemo();
    }

    @Override
    public void buildControl() {

        DefaultModel player = new DefaultModel(new BBox(new double[]{-0.5,0,-2},new double[]{+0.5,1,2}));
        player.getMaterials().add(GLCommons.createMaterial(Color.BLUE));
        player.getNodeTransform().getTranslation().setXYZ(-5, 0, 20);
        player.getNodeTransform().notifyChanged();
        scene.getChildren().add(player);

        //we control the player, we may move and rotate only on X and Z axis
        final FirstPersonUpdater controller = new FirstPersonUpdater(frame, player, new Vector3f64(0, 1, 0), new Vector3f64(1, 0, 0));
        controller.getGestureTasks().add(new FirstPersonUpdater.MoveForward(GestureTrigger.createRepeatKeyTrigger('z')));
        controller.getGestureTasks().add(new FirstPersonUpdater.MoveBackward(GestureTrigger.createRepeatKeyTrigger('s')));
        controller.getGestureTasks().add(new FirstPersonUpdater.MoveLeft(GestureTrigger.createRepeatKeyTrigger('q')));
        controller.getGestureTasks().add(new FirstPersonUpdater.MoveRight(GestureTrigger.createRepeatKeyTrigger('d')));
        controller.getGestureTasks().add(new FirstPersonUpdater.RotateHorizontal(GestureTrigger.createInstantMouseTrigger(MouseMessage.BUTTON_1,MouseMessage.TYPE_MOVE)));
        player.getUpdaters().add(controller);

        //place the camera a little above the player
        player.getChildren().add(camera);
        camera.getNodeTransform().getTranslation().setXYZ(-1.5, 2, 8);
        camera.getNodeTransform().notifyChanged();

        //we want the camera to be able to move a little, like looking around
        final FirstPersonUpdater camControl = new FirstPersonUpdater(frame, player, new Vector3f64(0, 1, 0), new Vector3f64(1, 0, 0));
        camControl.getGestureTasks().add(new FirstPersonUpdater.RotateHorizontal(GestureTrigger.createInstantMouseTrigger(MouseMessage.BUTTON_3,MouseMessage.TYPE_MOVE)));
        camControl.getGestureTasks().add(new FirstPersonUpdater.RotateVertical(GestureTrigger.createInstantMouseTrigger(MouseMessage.BUTTON_1,MouseMessage.TYPE_MOVE)));
        camera.getUpdaters().add(camControl);

         //we don't want the camera to have any roll and a max look up/down of 20°
        camera.getUpdaters().add(new Updater() {
            public void update(DisplayTimerState timing) {

                final Matrix3x3 m = new Matrix3x3(camera.getNodeTransform().getRotation());

                final Vector3f64 forward = new Vector3f64(0,0,1);
                final VectorRW ref = (VectorRW) m.transform(forward);
                double horizontalAngle = forward.shortestAngle(new Vector3f64(ref.getX(),0,ref.getZ()).localNormalize());
                if(ref.getX()<0) horizontalAngle = -horizontalAngle;

                double verticalAngle = ref.shortestAngle(new Vector3f64(ref.getX(),0,ref.getZ()).localNormalize());
                if(ref.getY()<0) verticalAngle = -verticalAngle;
                verticalAngle = Maths.clamp(verticalAngle, Math.toRadians(-20), Math.toRadians(+20));

                //rebuild matrix
                m.setToIdentity();
                if(!Double.isNaN(horizontalAngle)){
                    final Matrix3x3 mh = Matrix3x3.createRotation3(horizontalAngle, camera.getUpAxis());
                    m.localMultiply(mh);
                }
                if(!Double.isNaN(verticalAngle)){
                    final Matrix3x3 mv = Matrix3x3.createRotation3(verticalAngle, camera.getRightAxis());
                    m.localMultiply(mv);
                }
                camera.getNodeTransform().getRotation().set(m);
                camera.getNodeTransform().notifyChanged();
            }
        });

    }

}
