
package demo.opengl._8_controls;

import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.math.api.Angles;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 * Orbit camera demo.
 *
 * @author Johann Sorel
 */
public class _0_OrbitCameraDemo extends AbstractControlDemo {

    public static void main(String[] args) {
        new _0_OrbitCameraDemo();
    }

    @Override
    public void buildControl() {

        final MotionModel crosshair = DefaultMotionModel.createCrosshair3D();
        scene.getChildren().add(crosshair);

        final OrbitUpdater controller = new OrbitUpdater(frame,
                new Vector3f64(0, 1, 0), new Vector3f64(1, 0, 0), camera,crosshair).configureDefault();
        controller.setDistance(20);
        controller.setHorizontalAngle(Angles.degreeToRadian(30));
        controller.setVerticalAngle(Angles.degreeToRadian(25));

        camera.getUpdaters().add(controller);
    }

}
