
package demo.opengl._8_controls;

import science.unlicense.display.impl.DisplayTimerState;
import science.unlicense.display.impl.updater.FirstPersonUpdater;
import science.unlicense.display.impl.updater.Updater;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Vector3f64;

/**
 * Flying camera demo.
 *
 * @author Johann Sorel
 */
public class _2_FPSCameraDemo extends AbstractControlDemo {

    public static void main(String[] args) {
        new _2_FPSCameraDemo();
    }

    @Override
    public void buildControl() {

        final FirstPersonUpdater controller = new FirstPersonUpdater(frame, camera, new Vector3f64(0, 1, 0), new Vector3f64(1, 0, 0));
        controller.configureFirstPersonControls();


        camera.getUpdaters().add(controller);
        camera.getNodeTransform().getTranslation().setXYZ(0, 2, 20);
        camera.getNodeTransform().notifyChanged();


        //we want the camera to have a fixed elevation
        camera.getUpdaters().add(new Updater() {
            public void update(DisplayTimerState timing) {
                camera.getNodeTransform().getTranslation().setY(2);
                camera.getNodeTransform().notifyChanged();
            }
        });

         //we don't want the camera to have any roll and a max look up/down of 40°
        camera.getUpdaters().add(new Updater() {
            public void update(DisplayTimerState timing) {

                final Matrix3x3 m = new Matrix3x3(camera.getNodeTransform().getRotation());

                final Vector3f64 forward = new Vector3f64(0,0,1);
                final VectorRW ref = (VectorRW) m.transform(forward);
                double horizontalAngle = forward.shortestAngle(new Vector3f64(ref.getX(),0,ref.getZ()).localNormalize());
                if(ref.getX()<0) horizontalAngle = -horizontalAngle;

                double verticalAngle = ref.shortestAngle(new Vector3f64(ref.getX(),0,ref.getZ()).localNormalize());
                if(ref.getY()<0) verticalAngle = -verticalAngle;
                verticalAngle = Maths.clamp(verticalAngle, Math.toRadians(-40), Math.toRadians(+40));

                //rebuild matrix
                m.setToIdentity();
                if(!Double.isNaN(horizontalAngle)){
                    final Matrix3x3 mh = Matrix3x3.createRotation3(horizontalAngle, camera.getUpAxis());
                    m.localMultiply(mh);
                }
                if(!Double.isNaN(verticalAngle)){
                    final Matrix3x3 mv = Matrix3x3.createRotation3(verticalAngle, camera.getRightAxis());
                    m.localMultiply(mv);
                }
                camera.getNodeTransform().getRotation().set(m);
                camera.getNodeTransform().notifyChanged();
            }
        });

    }

}
