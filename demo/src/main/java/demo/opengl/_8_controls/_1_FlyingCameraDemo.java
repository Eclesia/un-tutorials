
package demo.opengl._8_controls;

import science.unlicense.display.impl.updater.FirstPersonUpdater;
import science.unlicense.math.impl.Vector3f64;

/**
 * Flying camera demo.
 *
 * @author Johann Sorel
 */
public class _1_FlyingCameraDemo extends AbstractControlDemo {

    public static void main(String[] args) {
        new _1_FlyingCameraDemo();
    }

    @Override
    public void buildControl() {

        final FirstPersonUpdater controller = new FirstPersonUpdater(frame, camera, new Vector3f64(0, 1, 0), new Vector3f64(1, 0, 0));
        controller.configureFlyingControls();

        camera.getUpdaters().add(controller);
        camera.getNodeTransform().getTranslation().setXYZ(0, 3, 20);
        camera.getNodeTransform().notifyChanged();
    }

}
