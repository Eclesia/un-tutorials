

package demo.opengl._8_controls;

import demo.opengl.GLCommons;
import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.light.PointLight;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.mesh.SkyBox;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.image.api.Images;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractControlDemo {

    protected final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
    protected final GLProcessContext context = frame.getContext();
    protected final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
    protected final MonoCamera camera = new MonoCamera();

    protected final DefaultModel box;

    protected AbstractControlDemo(){

        //create a rendering frame
        scene.getChildren().add(camera);
        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera));


        //build a small scene
        box = new DefaultModel(new BBox(new double[]{-2,0,-3},new double[]{5,2,5}));
        scene.getChildren().add(box);
        final PointLight light = new PointLight();
        light.getNodeTransform().getTranslation().setXYZ(0, 10, 0);
        light.getNodeTransform().notifyChanged();
        scene.getChildren().add(light);

        try{
            final SkyBox skyBox = new SkyBox(
                Images.read(Paths.resolve(new Chars("mod:/images/skybox/up.jpg"))),
                Images.read(Paths.resolve(new Chars("mod:/images/skybox/down.jpg"))),
                Images.read(Paths.resolve(new Chars("mod:/images/skybox/left.jpg"))),
                Images.read(Paths.resolve(new Chars("mod:/images/skybox/right.jpg"))),
                Images.read(Paths.resolve(new Chars("mod:/images/skybox/front.jpg"))),
                Images.read(Paths.resolve(new Chars("mod:/images/skybox/back.jpg"))));
            scene.getChildren().add(skyBox);
        }catch(Exception ex){
            ex.printStackTrace();
        }

        final DefaultModel ground = GLCommons.buildGround();
        scene.getChildren().add(ground);

        buildControl();

        //show the frame
        frame.setSize(640, 480);
        frame.setVisible(true);
    }

    public abstract void buildControl();

}
