
package demo.opengl._6_physics;

/**
 * Cloth demo.
 *
 * @author Johann Sorel
 */
public class _2_Cloth extends AbstractPhysicDemo {

    public _2_Cloth() {
    }

    public static void main(String[] args) {
        new _2_Cloth().show();
    }

}
