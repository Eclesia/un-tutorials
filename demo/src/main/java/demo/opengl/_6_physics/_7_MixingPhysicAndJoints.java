

package demo.opengl._6_physics;

import science.unlicense.common.api.character.Chars;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.Capsule;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.impl.technique.DebugRigidBodyTechnique;
import science.unlicense.model3d.impl.technique.DebugSkeletonTechnique;
import science.unlicense.physics.api.World;
import science.unlicense.physics.api.body.BodyGroup;
import science.unlicense.physics.api.body.RigidBody;
import science.unlicense.physics.api.force.Force;
import science.unlicense.physics.api.force.Gravity;
import science.unlicense.physics.api.force.Spring;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 * This demo illustrate how to mix a skeleton, animation and physics.
 * TODO
 *
 *
 * @author Johann Sorel
 */
public class _7_MixingPhysicAndJoints extends AbstractPhysicDemo {

    public _7_MixingPhysicAndJoints() {

        // build a skeleton, something like a fishing rod with a rigid part
        // and the fishing line relying on physic
        //   +-----+-------+------+-----+
        // rod0   rod1  line0  line1 line2

        final Skeleton skeleton = new Skeleton();
        final MotionModel mpm = new DefaultMotionModel(CoordinateSystems.UNDEFINED_3D);
        mpm.setSkeleton(skeleton);


        Joint root = new Joint(3);
        final RigidBody body = new RigidBody(new Sphere(1), 1);
        body.setFixed(true);
        root.getChildren().add(body);

        root.getNodeTransform().getTranslation().setXYZ(0, 10, 0);
        root.getNodeTransform().notifyChanged();
        BodyGroup group = new BodyGroup(new Chars("line"));
        skeleton.getChildren().add(root);
        for(int i=0;i<1;i++){
            root = buildChain(world, root, group);
        }

        skeleton.updateBindPose();
        skeleton.updateInvBindPose();

        mpm.getTechniques().add(new DebugSkeletonTechnique());
        mpm.getTechniques().add(new DebugRigidBodyTechnique());

        scene.getChildren().add(mpm);

        world.addSingularity(new Gravity(-9.8));

    }

    private static Joint buildChain(World world, Joint parent, BodyGroup group){

        final Joint node = new Joint(3);
        node.getNodeTransform().set(new Matrix4x4(
                1,0,0,3,
                0,1,0,-6,
                0,0,1,0,
                0,0,0,1
        ));

        final RigidBody body = new RigidBody(new Capsule(4, 1), 1);
        body.setUpdateMode(RigidBody.UPDATE_PARENT);
        group.add(body);
        node.getChildren().add(body);

        Force force = new Spring((RigidBody) parent.getChildren().get(0), body, 5, 0.5, 6);
        world.addForce(force);

        parent.getChildren().add(node);
        return node;
    }

    public static void main(String[] args) {
        new _7_MixingPhysicAndJoints().show();
    }


}