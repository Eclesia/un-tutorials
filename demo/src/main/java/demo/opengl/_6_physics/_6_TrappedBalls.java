
package demo.opengl._6_physics;

import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.technique.GLTechnique;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.Plane;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.physics.api.body.RigidBody;
import science.unlicense.physics.api.force.Gravity;

/**
 * Minimal physic demo, with a falling ball.
 *
 * @author Johann Sorel
 */
public class _6_TrappedBalls extends AbstractPhysicDemo{

    public _6_TrappedBalls() {

        //create multiple balls
        for(int i=0;i<1;i++){
            createBall(new Vector3f64(rd(),rd(),rd()), new Vector3f64(rdv(),rdv(),rdv()), 0.5, 1);
        }

        //create a closed box
        createPlan(0,new Vector3f64(+1, 0, 0), new Vector3f64( 0, 0, 0));
        createPlan(0,new Vector3f64(-1, 0, 0), new Vector3f64(10, 0, 0));
        createPlan(1,new Vector3f64( 0,+1, 0), new Vector3f64( 0, 0, 0));
        createPlan(1,new Vector3f64( 0,-1, 0), new Vector3f64( 0,10, 0));
        createPlan(2,new Vector3f64( 0, 0,+1), new Vector3f64( 0, 0, 0));
        createPlan(2,new Vector3f64( 0, 0,-1), new Vector3f64( 0, 0,10));

        world.addSingularity(new Gravity(-9.8));

    }

    public static double rd(){
        return (Math.random()*8)+1;
    }

    public static double rdv(){
        return ((Math.random()-0.5) *15);
    }

    private void createBall(Vector3f64 position, Vector3f64 velocity, double size, double mass){
        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuse(Color.BLUE);

        final DefaultModel ball = new DefaultModel(new Sphere(size));
        ball.getMaterials().add(material);
        ball.getNodeTransform().getTranslation().set(position);
        ball.getNodeTransform().notifyChanged();
        final RigidBody ballrb = new RigidBody(new Sphere(size), mass);
        ballrb.getMaterial().setRestitution(1);
        ballrb.getMotion().getVelocity().set(velocity);
        ballrb.getMaterial().setRestitution(0.1);
        ballrb.setUpdateMode(RigidBody.UPDATE_PARENT);
        ball.getChildren().add(ballrb);
        scene.getChildren().add(ball);
    }

    private void createPlan(int axis, Vector3f64 normal, Vector3f64 point){

        final Vector3f64 v0 = point.copy();
        final Vector3f64 v1 = point.copy();
        final Vector3f64 v2 = point.copy();
        final Vector3f64 v3 = point.copy();

        if(axis==0){
            v1.setY(10);
            v2.setY(10);v2.setZ(10);
                        v3.setZ(10);
        }else if(axis==1){
            v1.setX(10);
            v2.setX(10);v2.setZ(10);
                        v3.setZ(10);
        }else if(axis==2){
            v1.setX(10);
            v2.setX(10);v2.setY(10);
                        v3.setY(10);
        }

        final DefaultModel plan = new DefaultModel();
        plan.setShape(DefaultMesh.createPlan(v0,v1,v2,v3));
        GLEngineUtils.makeCompatible(plan);

        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuse(Color.RED);

        ((GLTechnique)plan.getTechniques().get(0)).getState().setPolygonMode(GLC.POLYGON_MODE.LINE);
        plan.getMaterials().add(material);
        final RigidBody rb = new RigidBody(new Plane(normal, point), 0);
        rb.getMaterial().setRestitution(1);
        plan.getChildren().add(rb);
        scene.getChildren().add(plan);
    }


    public static void main(String[] args) {
        new _6_TrappedBalls().show();
    }

}
