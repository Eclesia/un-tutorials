
package demo.opengl._6_physics;

import science.unlicense.display.impl.anim.TransformAnimation;
import science.unlicense.display.impl.anim.TransformTimeSerieHelper;
import science.unlicense.display.impl.updater.AnimationUpdater;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.Plane;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.physics.api.body.RigidBody;
import science.unlicense.physics.api.force.Gravity;
import science.unlicense.physics.api.force.Spring;

/**
 * Minimal physic demo, with a falling ball.
 *
 * @author Johann Sorel
 */
public class _4_Pendulum extends AbstractPhysicDemo{


    public _4_Pendulum() throws OperationException {

        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuse(Color.RED);

        final DefaultModel ball = new DefaultModel(DefaultMesh.createFromGeometry(new Sphere(1), 12, 12));
        ball.getMaterials().add(material);
        ball.getNodeTransform().getTranslation().setXYZ(2,5,2);
        ball.getNodeTransform().notifyChanged();
        //((GLActorTechnique)ball.getRenderers().get(0)).setUseLights(true);
        scene.getChildren().add(ball);
        //attach physic on our ball
        final RigidBody rb = new RigidBody(new Sphere(1), 5);
        rb.getMaterial().setRestitution(0.8);
        rb.setUpdateMode(RigidBody.UPDATE_PARENT);
        ball.getChildren().add(rb);

        //Create pendulum anchor
        final SimpleBlinnPhong.Material anchorMat = SimpleBlinnPhong.newMaterial();
        anchorMat.setDiffuse(Color.BLUE);

        final DefaultModel anchor = new DefaultModel(DefaultMesh.createFromGeometry(new Sphere(0.5), 12, 12));
        anchor.getMaterials().add(anchorMat);
        anchor.getNodeTransform().getTranslation().setXYZ(0,15,0);
        anchor.getNodeTransform().notifyChanged();
        //((GLActorTechnique)anchor.getRenderers().get(0)).setUseLights(true);
        scene.getChildren().add(anchor);
        //attach physic on our ball
        final RigidBody rbAnchor = new RigidBody(new Sphere(0.5), 0);
        rbAnchor.getMaterial().setRestitution(1);
        rbAnchor.setUpdateMode(RigidBody.UPDATE_PARENT);
        anchor.getChildren().add(rbAnchor);

        //the ground
        final SimpleBlinnPhong.Material groundMat = SimpleBlinnPhong.newMaterial();
        groundMat.setDiffuse(new ColorRGB(0, 0, 0, 0));

        final DefaultModel plan = new DefaultModel();
        plan.setShape(DefaultMesh.createPlan(
                new Vector3f64(+100, 0, -100),
                new Vector3f64(-100, 0, -100),
                new Vector3f64(-100, 0, +100),
                new Vector3f64(+100, 0, +100)));
        GLEngineUtils.makeCompatible(plan);
        plan.getMaterials().add(groundMat);
        scene.getChildren().add(plan);
        //ball.getRenderers().add(new GL2DebugRigidBodyTechnique());

        final Plane plane = new Plane(new Vector3f64(0, 1, 0), new Vector3f64(0, 0, 0));
        final RigidBody underRB = new RigidBody(plane, 0);
        underRB.setUpdateMode(RigidBody.UPDATE_PARENT);
        plan.getChildren().add(underRB);

        //spring force
        final Spring spring = new Spring(rb, rbAnchor, 500f, 100f, 3);

        world.setDrag(0.1);
        world.addSingularity(new Gravity(-9.8));
        world.addForce(spring);


        //make the anchor move
        final TransformTimeSerieHelper helper = new TransformTimeSerieHelper(3);
        helper.on(anchor).repeat(0);
        helper.at(0).translation(0, 10, 0);
        helper.at(3000).translation(0, 0, 0);
        helper.at(6000).translation(0, 10, 0);
        final TransformAnimation animation = helper.build();
        animation.start();

        anchor.getUpdaters().add(new AnimationUpdater(animation));

    }


    public static void main(String[] args) throws OperationException {
        new _4_Pendulum().show();
    }

}
