
package demo.opengl._6_physics;

import demo.opengl.GLCommons;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.light.DirectionalLight;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.mesh.SkyBox;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.physic.WorldUpdatePhase;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.image.api.Images;
import science.unlicense.math.api.Angles;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.impl.scene.SceneWorld;

/**
 * Minimal physic demo, with a falling ball.
 *
 * @author Johann Sorel
 */
public class AbstractPhysicDemo {

    protected final GLUIFrame frame          = GLUIFrameManager.INSTANCE.createFrame(false, false);
    protected final GLProcessContext context = frame.getContext();
    protected final MonoCamera camera        = new MonoCamera();
    protected final SceneNode scene          = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
    protected final SceneWorld world         = new SceneWorld(scene);

    public AbstractPhysicDemo() {

        //create a rendering pipeline
        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera));
//        context.getPhases().add(new DebugPhysicPhase(world,camera,null));



        //build a small scene
//        final PointLight light = new PointLight();
//        light.getNodeTransform().getTranslation().setXYZ(8, 8, 0);
//        light.getNodeTransform().notifyChanged();
//        scene.children().add(light);

        final DirectionalLight light = new DirectionalLight();
        light.getNodeTransform().getTranslation().setXYZ(8, 8, 0);
        light.getNodeTransform().notifyChanged();
        scene.getChildren().add(light);

        try{
            final SkyBox skyBox = new SkyBox(
                Images.read(Paths.resolve(new Chars("mod:/images/skybox/up.jpg"))),
                Images.read(Paths.resolve(new Chars("mod:/images/skybox/down.jpg"))),
                Images.read(Paths.resolve(new Chars("mod:/images/skybox/left.jpg"))),
                Images.read(Paths.resolve(new Chars("mod:/images/skybox/right.jpg"))),
                Images.read(Paths.resolve(new Chars("mod:/images/skybox/front.jpg"))),
                Images.read(Paths.resolve(new Chars("mod:/images/skybox/back.jpg"))));
            scene.getChildren().add(skyBox);
        }catch(Exception ex){
            ex.printStackTrace();
        }

        final DefaultModel ground = GLCommons.buildGround();
        scene.getChildren().add(ground);

        //keyboard and mouse control
        final MotionModel crosshair = DefaultMotionModel.createCrosshair3D();
        scene.getChildren().add(crosshair);
        final OrbitUpdater controller = new OrbitUpdater(frame,camera,crosshair).configureDefault();
        controller.setDistance(20);
        controller.setHorizontalAngle(Angles.degreeToRadian(30));
        controller.setVerticalAngle(Angles.degreeToRadian(25));
        crosshair.getChildren().add(camera);
        camera.getUpdaters().add(controller);


        //start physics on first keyboard event
        final WorldUpdatePhase worldUpdate = new WorldUpdatePhase(world);
        worldUpdate.setEnable(false);
        context.getPhases().add(worldUpdate);

        frame.addEventListener(KeyMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                worldUpdate.setEnable(true);
            }
        });

    }

    public void show(){
        frame.setSize(800, 600);
        frame.setVisible(true);
    }

}
