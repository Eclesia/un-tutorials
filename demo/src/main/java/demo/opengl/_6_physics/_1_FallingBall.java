
package demo.opengl._6_physics;

import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.technique.GLActorTechnique;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.Plane;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.physics.api.body.RigidBody;
import science.unlicense.physics.api.force.Gravity;

/**
 * Minimal physic demo, with a falling ball.
 *
 * @author Johann Sorel
 */
public class _1_FallingBall extends AbstractPhysicDemo{

    public _1_FallingBall() throws OperationException {

        //create our ball ------------------------------------------------------
        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuse(Color.BLUE);

        final DefaultModel ball = new DefaultModel(DefaultMesh.createFromGeometry(new Sphere(1), 12, 12));
        ball.getMaterials().add(material);
        ball.getNodeTransform().getTranslation().setXYZ(0,20,0);
        ball.getNodeTransform().notifyChanged();
        //((GLActorTechnique)ball.getRenderers().get(0)).setUseLights(true);
        scene.getChildren().add(ball);
        //attach physic on our ball
        final RigidBody rb = new RigidBody(new Sphere(1), 1);
        rb.getMaterial().setRestitution(0.8);
        rb.setUpdateMode(RigidBody.UPDATE_PARENT);
        //rb.getMotion().getVelocity().setXYZ(2, 0, 2);
        ball.getChildren().add(rb);


        //create the ground
        final SimpleBlinnPhong.Material groundMat = SimpleBlinnPhong.newMaterial();
        groundMat.setDiffuse(new ColorRGB(0, 0, 0, 0));

        final DefaultModel plan = new DefaultModel();
        plan.setShape(DefaultMesh.createPlan(
                new Vector3f64(+100, 0, -100),
                new Vector3f64(-100, 0, -100),
                new Vector3f64(-100, 0, +100),
                new Vector3f64(+100, 0, +100)));
        GLEngineUtils.makeCompatible(plan);
        ((GLActorTechnique)plan.getTechniques().get(0)).setUseLights(true);
        plan.getMaterials().add(groundMat);
        scene.getChildren().add(plan);

        final Plane plane = new Plane(new Vector3f64(0, 1, 0), new Vector3f64(0, 0, 0));

        final RigidBody underRB = new RigidBody(plane, 0);
        underRB.setUpdateMode(RigidBody.UPDATE_PARENT);
        plan.getChildren().add(underRB);

        //create the physic world
        world.getSingularities().add(new Gravity(-9.8));
    }

    public static void main(String[] args) throws OperationException {
        new _1_FallingBall().show();
    }

}
