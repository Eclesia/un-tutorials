

package demo.opengl._6_physics;

import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.impl.Capsule;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.physics.api.body.RigidBody;
import science.unlicense.physics.api.force.Spring;

/**
 *
 * @author Johann Sorel
 */
public class _5_ComplexSystem extends AbstractPhysicDemo{

    public _5_ComplexSystem() {

        final DefaultModel cap1 = new DefaultModel(new Capsule(10, 2));
        final RigidBody cap1rb = new RigidBody(new Capsule(10, 2), 1);
        cap1rb.setUpdateMode(RigidBody.UPDATE_PARENT);
        cap1.getChildren().add(cap1rb);

        final DefaultModel cap2 = new DefaultModel(new Capsule(10, 2));
        final RigidBody cap2rb = new RigidBody(new Capsule(10, 2), 1);
        cap2rb.setUpdateMode(RigidBody.UPDATE_PARENT);
        cap2.getChildren().add(cap2rb);

        cap1.getNodeTransform().getTranslation().setXYZ(+3, 0, 0);
        cap1.getNodeTransform().notifyChanged();
        cap2.getNodeTransform().getTranslation().setXYZ(-3, 0, 0);
        cap2.getNodeTransform().notifyChanged();

        scene.getChildren().add(cap1);
        scene.getChildren().add(cap2);

        createBall(new Vector3f64(0, 2, 10), new Vector3f64(0, 0, -5), 2, 30);
        createBall(new Vector3f64(1, 3, -5), new Vector3f64(0, 0, 0), 1, 1);
        createBall(new Vector3f64(-1, 3, -6), new Vector3f64(0, 0, 0), 1, 1);

        final Spring spring = new Spring(cap1rb, cap2rb, 0.1f, 0.1f, 6);
        world.addForce(spring);

        //create the ground
        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuse(Color.BLUE);

        final DefaultModel plan = new DefaultModel();
        plan.setShape(DefaultMesh.createPlan(
                new Vector3f64(+100, -100, -20),
                new Vector3f64(+100, +100, -20),
                new Vector3f64(-100, +100, -20),
                new Vector3f64(-100, -100, -20)));
        GLEngineUtils.makeCompatible(plan);
        plan.getMaterials().add(material);
        scene.getChildren().add(plan);

        final BBox plane = new BBox(new double[]{-100,-100,-20}, new double[]{100,100,-20});

        final RigidBody underRB = new RigidBody(plane, 0);
        underRB.setUpdateMode(RigidBody.UPDATE_PARENT);
        plan.getChildren().add(underRB);
    }

    private void createBall(Vector3f64 position, Vector3f64 velocity, double size, double mass){
        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuse(Color.BLUE);

        final DefaultModel ball = new DefaultModel(new Sphere(size));
        ball.getMaterials().add(material);
        ball.getNodeTransform().getTranslation().set(position);
        ball.getNodeTransform().notifyChanged();
        final RigidBody ballrb = new RigidBody(new Sphere(size), mass);
        ballrb.getMaterial().setRestitution(1);
        ballrb.getMotion().getVelocity().set(velocity);
        ballrb.setUpdateMode(RigidBody.UPDATE_PARENT);
        ball.getChildren().add(ballrb);

        scene.getChildren().add(ball);
    }



    public static void main(String[] args) {
        new _5_ComplexSystem().show();
    }


}
