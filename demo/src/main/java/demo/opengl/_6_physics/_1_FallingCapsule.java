
package demo.opengl._6_physics;

import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.impl.Capsule;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.Plane;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.physics.api.body.RigidBody;
import science.unlicense.physics.api.force.Gravity;

/**
 * Physic demo to illustrate torque support, with a falling capsule.
 *
 * @author Johann Sorel
 */
public class _1_FallingCapsule extends AbstractPhysicDemo{

    public _1_FallingCapsule() throws OperationException {

        //create our ball ------------------------------------------------------
        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuse(Color.RED);

        final DefaultModel ball = new DefaultModel(DefaultMesh.createFromGeometry(new Capsule(5,1), 12, 12));
        ball.getMaterials().add(material);
        ball.getNodeTransform().getTranslation().setXYZ(0,20,0);
        ball.getNodeTransform().getRotation().set(Matrix3x3.createRotation3(1, new Vector3f64(0,0,1)));
        ball.getNodeTransform().notifyChanged();
        //((GLActorTechnique)ball.getRenderers().get(0)).setUseLights(true);
        scene.getChildren().add(ball);
        //attach physic on our ball
        final RigidBody rb = new RigidBody(new Capsule(5,1), 1);
        rb.getMaterial().setRestitution(0.8);
        rb.setUpdateMode(RigidBody.UPDATE_PARENT);
        //rb.getMotion().getAngularVelocity().setXYZ(Maths.PI, 0, 0);
//        rb.getVelocity().setXYZ(0, 1, 0);
        ball.getChildren().add(rb);


        //create the ground
        final DefaultModel plan = new DefaultModel();
        plan.setShape(DefaultMesh.createPlan(
                new Vector3f64(+100, 0, -100),
                new Vector3f64(-100, 0, -100),
                new Vector3f64(-100, 0, +100),
                new Vector3f64(+100, 0, +100)));
        GLEngineUtils.makeCompatible(plan);
        scene.getChildren().add(plan);

        //final BBox plane = new BBox(new double[]{-100,-100,-100}, new double[]{100,0,100});
        final Plane plane = new Plane(new Vector3f64(0, 1, 0), new Vector3f64(0, 0, 0));

        final RigidBody underRB = new RigidBody(plane, 0);
        underRB.setUpdateMode(RigidBody.UPDATE_PARENT);
        plan.getChildren().add(underRB);

        //create the physic world
        world.getSingularities().add(new Gravity(-9.8));

    }

    public static void main(String[] args) throws OperationException {
        new _1_FallingCapsule().show();
    }

}
