
package demo.opengl._6_physics;

import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.buffer.Float32Cursor;
import science.unlicense.common.api.character.Chars;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.ByVertexColorMapping;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.math.impl.SimilarityNd;
import science.unlicense.math.impl.Quaternion;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.physic.JointKeyFrame;
import science.unlicense.model3d.impl.physic.JointTimeSerie;
import science.unlicense.model3d.impl.physic.SkeletonAnimation;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.Skin;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.physics.api.skeleton.IKSolver;
import science.unlicense.physics.api.skeleton.IKSolverCCD;
import science.unlicense.physics.api.skeleton.InverseKinematic;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 * A inverse kinematic demo.
 *
 * @author Johann Sorel
 */
public class _3_Kinematics extends AbstractPhysicDemo{

    public _3_Kinematics() {


        //joint structure
        //   +--------+--------+       +
        // tail2    tail1  effector  target
        final Joint target = new Joint(3);
        final Joint effector = new Joint(3);
        final Joint tail1 = new Joint(3);
        final Joint tail2 = new Joint(3);
        target.setId(new Chars("target"));
        effector.setId(new Chars("effector"));
        tail2.setId(new Chars("tail2"));
        tail1.setId(new Chars("tail1"));
        tail2.getChildren().add(tail1);
        tail1.getChildren().add(effector);

        tail1.getNodeTransform().getTranslation().setXYZ(5, 0, 0);
        tail1.getNodeTransform().notifyChanged();
        effector.getNodeTransform().getTranslation().setXYZ(5, 0, 0);
        effector.getNodeTransform().notifyChanged();
        target.getNodeTransform().getTranslation().setXYZ(15, 0, 0);
        target.getNodeTransform().notifyChanged();

        //kinematic
        final IKSolver solver = new IKSolverCCD(10, 100, 3);
        final InverseKinematic ik = new InverseKinematic(target, effector, new Joint[]{tail1,tail2}, solver);

        //skeleton
        final Skeleton skeleton = new Skeleton();
        skeleton.getChildren().add(tail2);
        skeleton.getChildren().add(target);
        skeleton.getIks().add(ik);
        skeleton.updateBindPose();
        skeleton.updateInvBindPose();

        final DefaultModel arm = new DefaultModel();
        final Float32Cursor vertices = DefaultBufferFactory.INSTANCE.createFloat32(6*3).cursor();
        vertices.write(0).write(0).write(1);
        vertices.write(0).write(0).write(-1);
        vertices.write(5).write(0).write(1);
        vertices.write(5).write(0).write(-1);
        vertices.write(10).write(0).write(1);
        vertices.write(10).write(0).write(-1);
        final Float32Cursor colors = DefaultBufferFactory.INSTANCE.createFloat32(6*3).cursor();
        colors.write(1).write(0).write(0);
        colors.write(1).write(0).write(0);
        colors.write(0).write(1).write(0);
        colors.write(0).write(1).write(0);
        colors.write(0).write(0).write(1);
        colors.write(0).write(0).write(1);

        final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
        final Skin skin = new Skin();
        skin.setSkeleton(skeleton);
        shell.setPositions(new VBO(vertices.getBuffer(), 3));
        shell.setNormals(new VBO(vertices.getBuffer(), 3));
        shell.setIndex(new IBO(new int[]{     0,1,2, 1,2,3, 2,3,4, 3,4,5 }));
        shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, 12)});
        skin.setMaxWeightPerVertex(1);
        shell.getAttributes().add(Skin.ATT_JOINTS_0, new VBO(new int[]{0,0,1, 0,1,1, 1,1,2, 1,2,2}, 1));
        shell.getAttributes().add(Skin.ATT_WEIGHTS_0, new VBO(new float[]{   1,1,1, 1,1,1, 1,1,1, 1,1,1}, 1));
        arm.setShape(shell);
        arm.setSkin(skin);

        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuseTexture(new Layer(new ByVertexColorMapping(colors.getBuffer(),false)));
        arm.getMaterials().add(material);
        scene.getChildren().add(arm);

        final DefaultModel objective = new DefaultModel();
        final Float32Cursor objvertices = DefaultBufferFactory.INSTANCE.createFloat32(24).cursor();
        objvertices.write(14).write(0).write(1);
        objvertices.write(15).write(0).write(1);
        objvertices.write(15).write(0).write(-1);
        objvertices.write(14).write(0).write(-1);
        final DefaultMesh objshell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
        final Skin objSkin = new Skin();
        objSkin.setSkeleton(skeleton);
        objshell.setPositions(new VBO(objvertices.getBuffer(), 3));
        objshell.setNormals(new VBO(objvertices.getBuffer(), 3));
        objshell.setIndex(new IBO(new int[]{0,1,2,0,2,3}));
        objshell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, 6)});
        objSkin.setMaxWeightPerVertex(1);
        objshell.getAttributes().add(Skin.ATT_JOINTS_0, new VBO(new int[]{3,3,3,3}, 1));
        objshell.getAttributes().add(Skin.ATT_WEIGHTS_0, new VBO(new float[]{1,1,1,1}, 1));
        objective.setShape(objshell);
        objective.setSkin(objSkin);
        scene.getChildren().add(objective);


        final DefaultModel zero = new DefaultModel(new Sphere(1));
        scene.getChildren().add(zero);

        //animation to move the kinematic target
        final JointTimeSerie serie = new JointTimeSerie(target);
        final JointKeyFrame frame0 = new JointKeyFrame(target, 0);
        final JointKeyFrame frame1 = new JointKeyFrame(target, 2000);
        final JointKeyFrame frame2 = new JointKeyFrame(target, 4000);
        final JointKeyFrame frame3 = new JointKeyFrame(target, 6000);
        final JointKeyFrame frame4 = new JointKeyFrame(target, 8000);
        frame0.setValue(SimilarityNd.create(new Quaternion(0,0,0,1), new Vector3f64(1, 1, 1), new Vector3f64(10, -2, 0)));
        frame1.setValue(SimilarityNd.create(new Quaternion(0,0,0,1), new Vector3f64(1, 1, 1), new Vector3f64( 5, 10, 4)));
        frame2.setValue(SimilarityNd.create(new Quaternion(0,0,0,1), new Vector3f64(1, 1, 1), new Vector3f64(-12, 1, 6)));
        frame3.setValue(SimilarityNd.create(new Quaternion(0,0,0,1), new Vector3f64(1, 1, 1), new Vector3f64(-5, -5, -6)));
        frame4.setValue(SimilarityNd.create(new Quaternion(0,0,0,1), new Vector3f64(1, 1, 1), new Vector3f64(10, -2, 0)));
        serie.getFrames().add(frame0);
        serie.getFrames().add(frame1);
        serie.getFrames().add(frame2);
        serie.getFrames().add(frame3);
        serie.getFrames().add(frame4);
        final SkeletonAnimation anim = new SkeletonAnimation(skeleton);
        anim.getSeries().add(serie);
        anim.start();

        arm.getUpdaters().add(anim);
        objective.getUpdaters().add(anim);

    }

    public static void main(String[] args) {
        new _3_Kinematics().show();
    }

}
