
package demo.opengl._6_physics;

import demo.opengl.GLCommons;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.image.api.color.Color;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.physics.api.body.RigidBody;

/**
 * Minimal physic demo, with a bouncing ball.
 *
 * @author Johann Sorel
 */
public class _0_BouncingBall extends AbstractPhysicDemo{

    public _0_BouncingBall() throws OperationException {

        //create our ball ------------------------------------------------------
        final DefaultModel ball = new DefaultModel(DefaultMesh.createFromGeometry(new Sphere(1), 12, 12));
        ball.getMaterials().add(GLCommons.createMaterial(Color.BLUE));
        ball.getNodeTransform().getTranslation().setXYZ(0,8,0);
        ball.getNodeTransform().notifyChanged();
        scene.getChildren().add(ball);
        //attach physic on our ball
        final RigidBody rb = new RigidBody(new Sphere(1), 1);
        rb.getMaterial().setRestitution(1);
        rb.getMotion().getVelocity().setY(1);
        rb.setUpdateMode(RigidBody.UPDATE_PARENT);
        ball.getChildren().add(rb);


        //create under ball
        final DefaultModel under = new DefaultModel(DefaultMesh.createFromGeometry(new Sphere(3), 12, 12));
        under.getNodeTransform().getTranslation().setXYZ(0,0,0);
        under.getNodeTransform().notifyChanged();
        scene.getChildren().add(under);
        //attach physic on our ball
        final RigidBody underRB = new RigidBody(new Sphere(3), 0);
        underRB.setUpdateMode(RigidBody.UPDATE_PARENT);
        under.getChildren().add(underRB);

        //create above ball
        final DefaultModel above = new DefaultModel(DefaultMesh.createFromGeometry(new Sphere(3), 12, 12));
        above.getNodeTransform().getTranslation().setXYZ(0,16,0);
        above.getNodeTransform().notifyChanged();
        scene.getChildren().add(above);
        //attach physic on our ball
        final RigidBody aboveRB = new RigidBody(new Sphere(3), 0);
        aboveRB.setUpdateMode(RigidBody.UPDATE_PARENT);
        above.getChildren().add(aboveRB);

    }

    public static void main(String[] args) throws OperationException {
        new _0_BouncingBall().show();
    }

}
