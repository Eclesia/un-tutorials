

package demo.opengl._6_physics;

import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.Capsule;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.physics.api.body.RigidBody;
import science.unlicense.physics.api.force.Gravity;
import science.unlicense.physics.api.force.Spring;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 * Second mix joint/physic demo.
 *
 * @author Johann Sorel
 */
public class _7_MixingPhysicAndJoints2 extends AbstractPhysicDemo {

    public _7_MixingPhysicAndJoints2() {

        final Skeleton skeleton = new Skeleton();
        final MotionModel mpm = new DefaultMotionModel(CoordinateSystems.UNDEFINED_3D);
        mpm.setSkeleton(skeleton);

        final Joint base = new Joint(3);
        final Joint line = new Joint(3);

        base.getNodeTransform().getTranslation().setXYZ(10, 10, 0);
        base.getNodeTransform().notifyChanged();

        final RigidBody rodRB = new RigidBody(new Capsule(10, 1), 1);
        rodRB.getNodeTransform().notifyChanged();
        rodRB.setFixed(true);
        base.getChildren().add(rodRB);

        line.getNodeTransform().getTranslation().setXYZ(4, 0, 0);
        line.getNodeTransform().notifyChanged();
        base.getChildren().add(line);


        final RigidBody lineRB = new RigidBody(new Sphere(1), 1);
        lineRB.setUpdateMode(RigidBody.UPDATE_PARENT);
        line.getChildren().add(lineRB);


        skeleton.getChildren().add(base);
        skeleton.updateBindPose();
        skeleton.updateInvBindPose();

        final Spring spring = new Spring(rodRB, lineRB, 500, 100f, 4);
        mpm.getConstraints().add(spring);


//        final DebugSkeletonNode node = new DebugSkeletonNode(mpm);
//        node.setJointVisible(true);
//        node.setRigidBodyVisible(true);
//        mpm.getChildren().add(node);

        scene.getChildren().add(mpm);
        world.addSingularity(new Gravity(-9.8));

    }

    public static void main(String[] args) {
        new _7_MixingPhysicAndJoints2().show();
    }


}