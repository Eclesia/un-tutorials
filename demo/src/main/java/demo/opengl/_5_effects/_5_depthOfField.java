

package demo.opengl._5_effects;

import demo.opengl.GLCommons;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.light.AmbientLight;
import science.unlicense.display.impl.light.DirectionalLight;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.DeferredRenderPhase;
import science.unlicense.engine.opengl.phase.FBOResizePhase;
import science.unlicense.engine.opengl.phase.GBO;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.phase.effect.DOFPhase;
import science.unlicense.engine.opengl.phase.effect.DirectPhase;
import science.unlicense.engine.opengl.phase.effect.FastGaussianBlurPhase;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.impl.process.ConvolutionMatrices;
import science.unlicense.image.impl.process.ConvolutionMatrix;
import science.unlicense.math.api.Angles;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 * TODO Depth of Field Demo.
 *
 * @author Johann Sorel
 */
public class _5_depthOfField {

    public static void main(String[] args) throws IOException {

        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final MonoCamera camera = new MonoCamera();

        final Sequence phases = context.getPhases();
        phases.removeAll();

        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);

        final MotionModel crosshair = DefaultMotionModel.createCrosshair3D();
        crosshair.getNodeTransform().getTranslation().setXYZ(3, 1, 26);
        crosshair.getNodeTransform().notifyChanged();
        scene.getChildren().add(crosshair);
        crosshair.getChildren().add(camera);
        final OrbitUpdater controller = new OrbitUpdater(frame,camera,crosshair).configureDefault();
        controller.setDistance(40);
        controller.setHorizontalAngle(Angles.degreeToRadian(190));
        controller.setVerticalAngle(Angles.degreeToRadian(10));
        camera.getUpdaters().add(controller);

        //build ground
        final DefaultModel ground = new DefaultModel();
        ground.setShape(DefaultMesh.createPlan(
                new Vector3f64(-100, 0, -100),
                new Vector3f64(-100, 0, +100),
                new Vector3f64(+100, 0, +100),
                new Vector3f64(+100, 0, -100)));
        GLEngineUtils.makeCompatible(ground);
        ground.getMaterials().add(GLCommons.createMaterial(Color.WHITE));
        scene.getChildren().add(ground);

        //build cubes at regular interval
        for(int i=0;i<10;i++){
            final DefaultModel box = new DefaultModel(new BBox(new double[]{0,0,0},new double[]{3,3,3}));
            box.getNodeTransform().getTranslation().setXYZ(0, 0, i*5);
            box.getNodeTransform().notifyChanged();
            box.getMaterials().add(GLCommons.createMaterial(Color.BLUE));
            scene.getChildren().add(box);
        }

        //lights
        final AmbientLight al = new AmbientLight();
        al.setDiffuse(Color.GRAY_LIGHT);
        scene.getChildren().add(al);

        final DirectionalLight dl = new DirectionalLight();
        scene.getChildren().add(dl);


        final int width = 1280;
        final int height = 1024;

        // 0 : clear the frame main buffer
        phases.add(new ClearPhase());
        // 1 : this phase updates the fbo used in render phases to match the display size
        phases.add(new FBOResizePhase());
        // 2 : update the world properly
        phases.add(new UpdatePhase(scene));

        // 3 : deferred rendering of the scene, extract : diffuse, position_camera, normal_camera
        //configure the texture we will generate
        //configure FBO with our textures
        final GBO fbo1 = new GBO(width, height);
        final Texture2D diffuse       = (Texture2D) fbo1.getDiffuseTexture();
        final Texture2D positionWorld = (Texture2D) fbo1.getPositionTexture();
        final Texture2D depth         = (Texture2D) fbo1.getDepthStencilTexture();


        final RenderPhase deferredRender = new DeferredRenderPhase(scene,camera,fbo1);
        phases.add(new ClearPhase(fbo1));
        phases.add(deferredRender);

        //intermediate FBO
        final FBO fbo2 = new FBO(width, height);
        fbo2.addAttachment(null, GLC.FBO.Attachment.COLOR_0, new Texture2D(width, height,Texture2D.COLOR_RGBA_CLAMPED()));
        final FBO fbo3 = new FBO(width, height);
        fbo3.addAttachment(null, GLC.FBO.Attachment.COLOR_0, new Texture2D(width, height,Texture2D.COLOR_RGBA_CLAMPED()));

        phases.add(new ClearPhase(fbo2));
        phases.add(new ClearPhase(fbo3));

        // 4 : apply horizontal blur on texture
        final ConvolutionMatrix gaussianRamp = ConvolutionMatrices.createGaussian(9, 12);
        final FastGaussianBlurPhase horizontalGaus = new FastGaussianBlurPhase(fbo2,fbo1.getColorTexture(),gaussianRamp,true);
        phases.add(horizontalGaus);
        final FastGaussianBlurPhase verticalGaus = new FastGaussianBlurPhase(fbo3,fbo2.getColorTexture(),gaussianRamp,false);
        phases.add(verticalGaus);

        // 5 : render the FBO on the main output
        final DOFPhase texturePhase = new DOFPhase(diffuse,positionWorld,fbo3.getColorTexture(),1f,20f,20f,30f,20f, camera);
        phases.add(texturePhase);

        // 6 : show the images position and blurred on the right
        final DirectPhase debug1 = new DirectPhase(positionWorld);
        final Matrix4x4 m1 = new Matrix4x4(
                0.25, 0, 0, 0.75,
                0, 0.25, 0, 0.75,
                0, 0, 1, 0,
                0, 0, 0, 1);
        debug1.setTransform(m1);
        final DirectPhase debug2 = new DirectPhase(fbo3.getColorTexture());
        final Matrix4x4 m2 = new Matrix4x4(
                0.25, 0, 0, 0.75,
                0, 0.25, 0, 0.25,
                0, 0, 1, 0,
                0, 0, 0, 1);
        debug2.setTransform(m2);
        phases.add(debug1);
        phases.add(debug2);

        //show the frame
        frame.setSize(width, height);
        frame.setVisible(true);

    }

}
