
package demo.opengl._5_effects;

import demo.opengl.GLCommons;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.light.SpotLight;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.FBOResizePhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.phase.effect.DirectPhase;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.Quad;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.Angles;
import science.unlicense.math.api.Maths;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.scene.DefaultModel;

/**
 * Projected shadows demo.
 * TODO
 *
 * @author Johann Sorel
 */
public class _8_projectedShadow {

    public static void main(String[] args) {

        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final Sequence phases = context.getPhases();
        final SceneNode scene   = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        scene.getChildren().add(camera);

        //build a small scene
        final DefaultModel ground = new DefaultModel(new Quad(100,100));
        ground.getMaterials().add(GLCommons.createMaterial(Color.GRAY_LIGHT));
        ground.getNodeTransform().getRotation().set(Matrix3x3.createRotation3(Maths.HALF_PI, new Vector3f64(1, 0, 0)));
        ground.getNodeTransform().notifyChanged();
        scene.getChildren().add(ground);
        final DefaultModel box = new DefaultModel(new BBox(new double[]{-2,-2,-2},new double[]{2,2,2}));
        box.getNodeTransform().getTranslation().setXYZ(0, 6, 0);
        box.getNodeTransform().notifyChanged();
        scene.getChildren().add(box);
        final SpotLight light = new SpotLight();
        light.getNodeTransform().getTranslation().setXYZ(0, 20, 0);
        light.getNodeTransform().getRotation().set(Matrix3x3.createRotation3(Maths.HALF_PI, new Vector3f64(0, 0, 1)));
        light.getNodeTransform().notifyChanged();
        scene.getChildren().add(light);


        //keyboard and mouse control
        final OrbitUpdater controller = new OrbitUpdater(frame,camera,box).configureDefault();
        controller.setDistance(20);
        controller.setHorizontalAngle(Angles.degreeToRadian(30));
        controller.setVerticalAngle(Angles.degreeToRadian(25));
        camera.getUpdaters().add(controller);


        //we will use a FBO for rendering.
        int width = 1024;
        int height = 768;
        final FBO fbo = new FBO(width, width);
        fbo.addAttachment(null, GLC.FBO.Attachment.COLOR_0, new Texture2D(width, height,Texture2D.COLOR_RGBA_CLAMPED()));
        fbo.addAttachment(null, GLC.FBO.Attachment.DEPTH_STENCIL, new Texture2D(width, height,Texture2D.DEPTH24_STENCIL8()));

        // 0 : clear the frame main buffer
        phases.add(new ClearPhase());
        // 1 : this phase updates the fbo used in render phases to match the display size
        phases.add(new FBOResizePhase());
        // 2 : update the world properly
        phases.add(new UpdatePhase(scene));
        // 3 : render the scene in a FBO
        phases.add(new RenderPhase(scene,camera,fbo));
        // 4 : render
        phases.add(new DirectPhase(fbo.getColorTexture()));

        //show the frame
        frame.setSize(width, height);
        frame.setVisible(true);

    }

}
