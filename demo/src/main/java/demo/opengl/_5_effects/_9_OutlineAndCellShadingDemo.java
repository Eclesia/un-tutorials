
package demo.opengl._5_effects;

import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.light.PointLight;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.engine.opengl.technique.SilhouetteTechnique;
import science.unlicense.engine.opengl.technique.actor.LightsActor;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.image.api.color.Color;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 * Rendering mesh with CellShading and Outline.
 *
 * @author Johann Sorel
 */
public class _9_OutlineAndCellShadingDemo {

    public static void main(String[] args) throws IOException, OperationException {

        //create a rendering frame
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        scene.getChildren().add(camera);
        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera));

        //build the scene
        buildScene(scene);

        final PointLight light = new PointLight();
        light.getNodeTransform().getTranslation().setXYZ(0, 0, 0);
        light.getNodeTransform().notifyChanged();
        scene.getChildren().add(light);

        //keyboard and mouse control
        final MotionModel cameraTarget = DefaultMotionModel.createCrosshair3D();
        scene.getChildren().add(cameraTarget);
        OrbitUpdater controller = new OrbitUpdater(frame,camera,cameraTarget).configureDefault();
        controller.setDistance(10);
        controller.setHorizontalAngle(45);
        controller.setVerticalAngle(45);
        camera.getUpdaters().add(controller);

        //show the frame
        frame.setSize(640, 480);
        frame.setVisible(true);
    }

    public static void buildScene(SceneNode scene) throws IOException, OperationException{

        DefaultModel meshNormal = buildMesh(DefaultMesh.createIcosphere(4), -10);
        DefaultModel meshEffect = buildMesh(DefaultMesh.createIcosphere(4), +10);


        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuse(Color.WHITE);
        material.properties().setPropertyValue(LightsActor.CELLSHADINGSTEPS, 5);
        meshEffect.getMaterials().add(material);
        meshEffect.getTechniques().add(new SilhouetteTechnique(meshEffect, Color.WHITE, 0.01f));

        scene.getChildren().add(meshNormal);
        scene.getChildren().add(meshEffect);
    }

    public static DefaultModel buildMesh(Geometry geom, int offset) throws OperationException{
        final DefaultModel mesh = new DefaultModel(DefaultMesh.createFromGeometry(geom, 20, 20));
        return buildMesh(mesh, offset);
    }

    public static DefaultModel buildMesh(DefaultModel mesh, int offset){
        mesh.getNodeTransform().getTranslation().setXYZ(offset, 0, 0);
        mesh.getNodeTransform().notifyChanged();
        return mesh;
    }

}