
package demo.opengl._5_effects;

import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.light.PointLight;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.FBOResizePhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.phase.effect.FastGaussianBlurPhase;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.image.impl.process.ConvolutionMatrices;
import science.unlicense.image.impl.process.ConvolutionMatrix;
import science.unlicense.math.api.Angles;
import science.unlicense.model3d.impl.scene.DefaultModel;

/**
 * A post effect, using 2 steps fast gaussian blur.
 *
 * @author Johann Sorel
 */
public class _7_fastgaussian {

    public static void main(String[] args) {

        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        scene.getChildren().add(camera);
        final Sequence phases = context.getPhases();

        //build a small scene
        final DefaultModel box = new DefaultModel(new BBox(new double[]{-5,-5,-5},new double[]{5,5,5}));
        scene.getChildren().add(box);
        //keyboard and mouse control
        final OrbitUpdater controller = new OrbitUpdater(frame,camera,box).configureDefault();
        controller.setDistance(20);
        controller.setHorizontalAngle(Angles.degreeToRadian(30));
        controller.setVerticalAngle(Angles.degreeToRadian(25));
        camera.getUpdaters().add(controller);

        final PointLight light = new PointLight();
        light.getNodeTransform().getTranslation().setXYZ(2, 12, -3);
        light.getNodeTransform().notifyChanged();
        scene.getChildren().add(light);

        //you can use an existing convolution matrix
        final ConvolutionMatrix gaussianRamp = ConvolutionMatrices.createGaussian(7, 10);
        //or create a ramp yourself
        //final float[] gaussianRamp = new float[]{
        //    0.05f,
        //    0.09f,
        //    0.12f,
        //    0.15f,
        //    0.18f,
        //    0.15f,
        //    0.12f,
        //    0.09f,
        //    0.05f
        //};

        //we will use a FBO for rendering.
        int width = 1024;
        int height = 768;
        final FBO fbo1 = new FBO(width, width);
        fbo1.addAttachment(null, GLC.FBO.Attachment.COLOR_0, new Texture2D(width, height,Texture2D.COLOR_RGBA_CLAMPED()));
        fbo1.addAttachment(null, GLC.FBO.Attachment.DEPTH_STENCIL, new Texture2D(width, height,Texture2D.DEPTH24_STENCIL8()));
        //intermediate FBO
        final FBO fbo2 = new FBO(width, height);
        fbo2.addAttachment(null, GLC.FBO.Attachment.COLOR_0, new Texture2D(640, 480,Texture2D.COLOR_RGBA_CLAMPED()));

        // 0 : clear the frame main buffer
        phases.add(new ClearPhase(fbo1));
        phases.add(new ClearPhase(fbo2));
        phases.add(new ClearPhase(new float[]{1f,1f,1f,1f}));
        // 1 : this phase updates the fbo used in render phases to match the display size
        phases.add(new FBOResizePhase());
        // 2 : update the world properly
        phases.add(new UpdatePhase(scene));
        // 3 : render the scene in a FBO
        phases.add(new RenderPhase(scene,camera,fbo1));
        // 4 : apply horizontal blur on texture
        phases.add(new FastGaussianBlurPhase(fbo2,fbo1.getColorTexture(),gaussianRamp,true));
        // 5 : apply vertical blur on texture
        phases.add(new FastGaussianBlurPhase(fbo2.getColorTexture(),gaussianRamp,false));


        //show the frame
        frame.setSize(width, height);
        frame.setVisible(true);

    }

}
