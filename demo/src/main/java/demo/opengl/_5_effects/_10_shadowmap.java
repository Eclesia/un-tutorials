

package demo.opengl._5_effects;

import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.light.DirectionalLight;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.phase.effect.DirectPhase;
import science.unlicense.engine.opengl.phase.shadow.LightspacePerspectiveShadowMapsPhase;
import science.unlicense.engine.opengl.phase.shadow.ShadowMap;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.Quad;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.Angles;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 *
 * @author Johann Sorel
 */
public class _10_shadowmap {

    public static void main(String[] args) {

        //create a rendering frame
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        camera.setNearPlane(0.1);
        camera.setFarPlane(1000);
//        camera.getNodeTransform().getTranslation().setXYZ(3, 3, 10);
//        camera.getNodeTransform().notifyChanged();

        //build a small scene
        final DefaultModel box = new DefaultModel(new BBox(new double[]{-5,-5,-5},new double[]{5,5,5}));
        scene.getChildren().add(box);

        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuse(Color.WHITE);

        final DefaultModel plan = new DefaultModel(new Quad(100, 100));
        plan.getMaterials().add(material);
        plan.getNodeTransform().getRotation().set(Matrix3x3.createRotation3(Angles.degreeToRadian(-90), new Vector3f64(1, 0, 0)));
        plan.getNodeTransform().getTranslation().setXYZ(0, -10, 0);
        plan.getNodeTransform().notifyChanged();
        scene.getChildren().add(plan);

        final DirectionalLight light = new DirectionalLight();
        light.getNodeTransform().getRotation().set(Matrix3x3.createRotation3(Angles.degreeToRadian(-50), new Vector3f64(1, 0, 0)));
        light.getNodeTransform().getTranslation().setXYZ(0, 50, 40);
        light.getNodeTransform().notifyChanged();
        scene.getChildren().add(light);

        final ShadowMap sm = new ShadowMap();
        sm.update(camera,light,512);
        //sm.getCamera().getRenderers().add(new DebugCameraRenderer());

        final MotionModel marker = DefaultMotionModel.createXYZMarker();
        light.getChildren().add(marker);

        final MotionModel crosshair = DefaultMotionModel.createCrosshair3D();
        scene.getChildren().add(crosshair);
        crosshair.getChildren().add(camera);

        //keyboard and mouse control
        final OrbitUpdater controller = new OrbitUpdater(frame,camera,crosshair).configureDefault();
        controller.setDistance(20);
        controller.setHorizontalAngle(Angles.degreeToRadian(30));
        controller.setVerticalAngle(Angles.degreeToRadian(25));
        camera.getUpdaters().add(controller);

        final LightspacePerspectiveShadowMapsPhase lights = new LightspacePerspectiveShadowMapsPhase(scene,camera,512);

        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(lights);
        context.getPhases().add(new RenderPhase(scene,camera));

        final DirectPhase debug1 = new DirectPhase(sm.getFbo().getDepthStencilTexture());
        final Matrix4x4 m1 = new Matrix4x4(
                0.25, 0, 0, 0.75,
                0, 0.25, 0, 0.75,
                0, 0, 1, 0,
                0, 0, 0, 1);
        debug1.setTransform(m1);
        context.getPhases().add(debug1);

        //show the frame
        frame.setSize(512, 512);
        frame.setVisible(true);
    }

}
