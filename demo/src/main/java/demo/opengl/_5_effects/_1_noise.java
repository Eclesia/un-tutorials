
package demo.opengl._5_effects;

import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.FBOResizePhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.phase.effect.UniformNoisePhase;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.math.api.Angles;
import science.unlicense.model3d.impl.scene.DefaultModel;

/**
 * A simple post effect, using uniform noise.
 *
 * @author Johann Sorel
 */
public class _1_noise {

    public static void main(String[] args) {

        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final Sequence phases = context.getPhases();
        final SceneNode scene   = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        scene.getChildren().add(camera);

        //build a small scene
        final DefaultModel box = new DefaultModel(new BBox(new double[]{-5,-5,-5},new double[]{5,5,5}));
        scene.getChildren().add(box);
        //keyboard and mouse control
        final OrbitUpdater controller = new OrbitUpdater(frame,camera,box).configureDefault();
        controller.setDistance(20);
        controller.setHorizontalAngle(Angles.degreeToRadian(30));
        controller.setVerticalAngle(Angles.degreeToRadian(25));
        camera.getUpdaters().add(controller);


        //we will use a FBO for rendering.
        int width = 1024;
        int height = 768;
        final FBO fbo = new FBO(width, width);
        fbo.addAttachment(null, GLC.FBO.Attachment.COLOR_0, new Texture2D(width, height,Texture2D.COLOR_RGBA_CLAMPED()));
        fbo.addAttachment(null, GLC.FBO.Attachment.DEPTH_STENCIL, new Texture2D(width, height,Texture2D.DEPTH24_STENCIL8()));

        // 0 : clear the frame main buffer
        phases.add(new ClearPhase());
        phases.add(new ClearPhase(fbo));
        // 1 : this phase updates the fbo used in render phases to match the display size
        phases.add(new FBOResizePhase());
        // 2 : update the world properly
        phases.add(new UpdatePhase(scene));
        // 3 : render the scene in a FBO
        phases.add(new RenderPhase(scene,camera,fbo));
        // 4 : apply a uniform noise on texture
        phases.add(new UniformNoisePhase(fbo.getColorTexture()));

        //show the frame
        frame.setSize(width, height);
        frame.setVisible(true);

    }

}
