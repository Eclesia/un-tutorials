
package demo.opengl._5_effects;

import com.jogamp.opengl.GL4bc;
import com.jogamp.opengl.GLException;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.engine.opengl.phase.AbstractPhase;
import science.unlicense.engine.opengl.phase.Phase;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.geometry.api.system.CoordinateSystems;

/**
 * Understanding engine phases.
 *
 * @author Johann Sorel
 */
public class _0_phases {

    public static void main(String[] args) {

        /*
         Phases are a main part of the Unlicense engine.
         The rendering process is a succesion of phases, each phases having a specific
         objectives. like :
         - updating models and animations
         - properly handeling gesture events at the right time
         - calculating lights fbo
         - updating physics
         - picking
         - and the most important : rendering

         You can create your own phases, like demonstrated here.
         */

        //create a rendering frame
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        scene.getChildren().add(camera);

        //make some custom phases
        final Sequence phases = context.getPhases();
        //the context starts with 0 phases by default :

        /*
         We use old direct operation here for demonstration only, use the latest
         technologie when possible, the Unlicense engine is not aiming for opengl
         version before v4. (at least for now)
         */

        //draw a triangle in this phase
        final Phase phase1 = new AbstractPhase() {
            public void processInt(GLProcessContext context) throws GLException {
                final GL4bc gl = (GL4bc) context.getGL();
                gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT);
                gl.glColor3f(1.0f,0.0f,0.0f);
                gl.glBegin(gl.GL_TRIANGLES);
                    gl.glVertex3f(-0.75f, 0.5f, 0.0f);
                    gl.glVertex3f(-1.0f,-0.5f, 0.0f);
                    gl.glVertex3f(-0.5f,-0.5f, 0.0f);
                gl.glEnd();
            }
        };
        phases.add(phase1);

        //draw a square in this phase
        final Phase phase2 = new AbstractPhase() {
            public void processInt(GLProcessContext context) throws GLException {
                final GL4bc gl = (GL4bc) context.getGL();
                gl.glColor3f(0.0f,0.0f,1.0f);
                gl.glBegin(gl.GL_QUADS);
                    gl.glVertex3f( 0.0f, 0.6f, 0.0f);
                    gl.glVertex3f( 0.6f, 0.6f, 0.0f);
                    gl.glVertex3f( 0.6f, 0.0f, 0.0f);
                    gl.glVertex3f( 0.0f, 0.0f, 0.0f);
                gl.glEnd();
            }
        };
        phases.add(phase2);


        //show the frame
        frame.setSize(640, 480);
        frame.setVisible(true);

    }

}
