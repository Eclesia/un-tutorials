
package demo.opengl._5_effects;

import demo.opengl.GLCommons;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.DeferredRenderPhase;
import science.unlicense.engine.opengl.phase.FBOResizePhase;
import science.unlicense.engine.opengl.phase.GBO;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.phase.effect.DirectPhase;
import science.unlicense.engine.opengl.phase.effect.SSAOBlendPhase;
import science.unlicense.engine.opengl.phase.effect.SSAOPhase;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.Angles;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 * Screen Space Ambiant Occlusion demo.
 *
 * @author Johann Sorel
 */
public class _4_ssao {

    public static void main(String[] args) throws IOException {

        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        final Sequence phases = context.getPhases();
        scene.getChildren().add(buildScene());


        //keyboard and mouse control
        final MotionModel crosshair = DefaultMotionModel.createCrosshair3D();
        scene.getChildren().add(crosshair);
        crosshair.getChildren().add(camera);
        final OrbitUpdater controller = new OrbitUpdater(frame,camera,crosshair).configureDefault();
        controller.setDistance(40);
        controller.setHorizontalAngle(Angles.degreeToRadian(190));
        controller.setVerticalAngle(Angles.degreeToRadian(20));
        camera.getUpdaters().add(controller);

        int width = 1280;
        int height = 1024;

        // 0 : clear the frame main buffer
        phases.add(new ClearPhase());
        // 1 : this phase updates the fbo used in render phases to match the display size
        phases.add(new FBOResizePhase());
        // 2 : update the world properly
        phases.add(new UpdatePhase(scene));

        // 3 : deferred rendering of the scene, extract : diffuse, position_camera, normal_camera
        //configure the texture we will generate
        final GBO gbo = new GBO(width, height);
        final Texture2D texDiffuse        = (Texture2D) gbo.getDiffuseTexture();
        final Texture2D texPositionWorld  = (Texture2D) gbo.getPositionTexture();
        final Texture2D texNormalWorld    = (Texture2D) gbo.getNormalTexture();
        final Texture2D textDepth         = (Texture2D) gbo.getDepthTexture();

        final RenderPhase renderphase = new DeferredRenderPhase(scene,camera,gbo);

        phases.add(new ClearPhase(gbo));
        phases.add(renderphase);

        // 4 : apply ssao
        final Texture2D texSsao  = new Texture2D(width, height, Texture2D.VEC1_FLOAT());
        final FBO ssaoFbo = new FBO(width, height);
        ssaoFbo.addAttachment(null, GLC.FBO.Attachment.COLOR_0, texSsao);
        phases.add(new ClearPhase(ssaoFbo));
        phases.add(new SSAOPhase(ssaoFbo,camera,texPositionWorld,texNormalWorld));

        // 5 : mix ssao with original image
        final Texture2D texFinal  = new Texture2D(width, height, Texture2D.COLOR_RGBA_CLAMPED());
        final FBO finalFbo = new FBO(width, height);
        finalFbo.addAttachment(null, GLC.FBO.Attachment.COLOR_0, texFinal);
        phases.add(new ClearPhase(finalFbo));
        phases.add(new SSAOBlendPhase(finalFbo,texDiffuse,texSsao));

        // 5 : render the FBO on the main output
        phases.add(new DirectPhase(texFinal));

        // 6 : show the images position and blurred on the right
        phases.add(new DirectPhase(null, texPositionWorld, new Matrix4x4(
                        0.25, 0, 0, 0.75,
                        0, 0.25, 0, 0.75,
                        0, 0, 1, 0,
                        0, 0, 0, 1)));
        phases.add(new DirectPhase(null, texNormalWorld, new Matrix4x4(
                        0.25, 0, 0, 0.75,
                        0, 0.25, 0, 0.25,
                        0, 0, 1, 0,
                        0, 0, 0, 1)));
        phases.add(new DirectPhase(null, texSsao, new Matrix4x4(
                        0.25, 0, 0, 0.75,
                        0, 0.25, 0, -0.25,
                        0, 0, 1, 0,
                        0, 0, 0, 1)));

        //show the frame
        frame.setSize(width, height);
        frame.setVisible(true);

    }

    public static SceneNode buildScene(){
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        //build a small scene
        final DefaultModel box = new DefaultModel(new BBox(new double[]{-5,-5,-5},new double[]{5,5,5}));
        box.getMaterials().add(GLCommons.createMaterial(Color.BLUE));
        scene.getChildren().add(box);

        final DefaultModel plan0 = new DefaultModel();
        plan0.setShape(DefaultMesh.createPlan(
                new Vector3f64(-10, 0, -10),
                new Vector3f64(-10, 0, +10),
                new Vector3f64(+10, 0, +10),
                new Vector3f64(+10, 0, -10)));
        plan0.getMaterials().add(GLCommons.createMaterial(Color.RED));
        GLEngineUtils.makeCompatible(plan0);

        final DefaultModel plan1 = new DefaultModel();
        plan1.setShape(DefaultMesh.createPlan(
                new Vector3f64(-10,   0, -10),
                new Vector3f64(-10, +10, -10),
                new Vector3f64(-10, +10, +10),
                new Vector3f64(-10,   0, +10)));
        plan1.getMaterials().add(GLCommons.createMaterial(Color.RED));
        GLEngineUtils.makeCompatible(plan1);

        final DefaultModel plan2 = new DefaultModel();
        plan2.setShape(DefaultMesh.createPlan(
                new Vector3f64(+10,   0, +10),
                new Vector3f64(+10, +10, +10),
                new Vector3f64(+10, +10, -10),
                new Vector3f64(+10,   0, -10)));
        plan2.getMaterials().add(GLCommons.createMaterial(Color.RED));
        GLEngineUtils.makeCompatible(plan2);

        final DefaultModel plan3 = new DefaultModel();
        plan3.setShape(DefaultMesh.createPlan(
                new Vector3f64(-10,   0, +10),
                new Vector3f64(-10, +10, +10),
                new Vector3f64(+10, +10, +10),
                new Vector3f64(+10,   0, +10)));
        plan3.getMaterials().add(GLCommons.createMaterial(Color.RED));
        GLEngineUtils.makeCompatible(plan3);

        scene.getChildren().add(plan0);
        scene.getChildren().add(plan1);
        scene.getChildren().add(plan2);
        scene.getChildren().add(plan3);

        final DefaultModel sphere = new DefaultModel(new Sphere(3));
        sphere.getMaterials().add(GLCommons.createMaterial(Color.GREEN));
        sphere.getNodeTransform().getTranslation().setXYZ(0, 8, 0);
        sphere.getNodeTransform().notifyChanged();
        scene.getChildren().add(sphere);

        return scene;
    }

}