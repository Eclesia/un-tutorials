

package demo.opengl._5_effects;

import demo.opengl.GLCommons;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.FBOResizePhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.phase.effect.RadialBlurPhase;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.math.api.Angles;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 * Radial blur demo.
 *
 * @author Johann Sorel
 */
public class _12_radialblur {

    public static void main(String[] args) throws IOException {

        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();

        final SceneNode scene = GLCommons.buildCubesScene();
        final MonoCamera camera = new MonoCamera();
        final Sequence phases = context.getPhases();

        final MotionModel crosshair = DefaultMotionModel.createCrosshair3D();
        scene.getChildren().add(crosshair);
        final OrbitUpdater controller = new OrbitUpdater(frame,camera,crosshair).configureDefault();
        controller.setDistance(20);
        controller.setHorizontalAngle(Angles.degreeToRadian(30));
        controller.setVerticalAngle(Angles.degreeToRadian(25));
        camera.getUpdaters().add(controller);
        crosshair.getChildren().add(camera);


        //we will use a FBO for rendering.
        int width = 1024;
        int height = 768;
        final FBO fbo = new FBO(width, height);
        fbo.addAttachment(null, GLC.FBO.Attachment.COLOR_0, new Texture2D(width, height,Texture2D.COLOR_RGBA_CLAMPED()));
        fbo.addAttachment(null, GLC.FBO.Attachment.DEPTH_STENCIL, new Texture2D(width, height,Texture2D.DEPTH24_STENCIL8()));

        // 0 : clear the frame main buffer
        phases.add(new ClearPhase(fbo,new float[]{0f,0f,0f,1f}));
        // 1 : this phase updates the fbo used in render phases to match the display size
        phases.add(new FBOResizePhase());
        // 2 : update the world properly
        phases.add(new UpdatePhase(scene));
        // 3 : render the deferred scene in a FBO
        phases.add(new RenderPhase(scene,camera,fbo));
        // 4 : apply bloom on texture
        phases.add(new RadialBlurPhase(fbo.getColorTexture(), new float[]{0.5f,0.5f},20,0.2f,0.9f));

        //show the frame
        frame.setSize(width, height);
        frame.setVisible(true);

    }

}
