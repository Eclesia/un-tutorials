
package demo.opengl._3_animations;

import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.buffer.Float32Cursor;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.light.PointLight;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.math.api.Angles;
import science.unlicense.math.impl.SimilarityNd;
import science.unlicense.math.impl.Quaternion;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.physic.JointKeyFrame;
import science.unlicense.model3d.impl.physic.JointTimeSerie;
import science.unlicense.model3d.impl.physic.SkeletonAnimation;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.Skin;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 * Skinning demo.
 *
 * @author Johann Sorel
 */
public class _0_Skinning {

    public static void main(String[] args) throws OperationException {

        //create a rendering frame
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        scene.getChildren().add(camera);
        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera));

        //light
        final PointLight light = new PointLight();
        light.getNodeTransform().getTranslation().setXYZ(8, 8, 0);
        light.getNodeTransform().notifyChanged();
        scene.getChildren().add(light);


        //joint structure
        //   +--------+
        // root   edge
        final Joint edge = new Joint(3);
        final Joint root = new Joint(3);
        root.getChildren().add(edge);

        edge.getNodeTransform().getTranslation().setXYZ(15, 0, 0);
        edge.getNodeTransform().notifyChanged();

        //skeleton
        final Skeleton skeleton = new Skeleton();
        skeleton.getChildren().add(root);
        skeleton.updateBindPose();
        skeleton.updateInvBindPose();

        final DefaultModel arm = new DefaultModel();
        final Float32Cursor vertices = DefaultBufferFactory.INSTANCE.createFloat32(24).cursor();
        vertices.write(0).write(0).write(1);
        vertices.write(15).write(0).write(1);
        vertices.write(15).write(0).write(-1);
        vertices.write(0).write(0).write(-1);
        final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
        final Skin skin = new Skin();
        skin.setSkeleton(skeleton);
        shell.setPositions(new VBO(vertices.getBuffer(), 3));
        shell.setNormals(new VBO(vertices.getBuffer(), 3));
        shell.setIndex(new IBO(new int[]{0,1,2,0,2,3}));
        shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, 6)});
        skin.setMaxWeightPerVertex(1);
        shell.getAttributes().add(Skin.ATT_JOINTS_0, new VBO(new int[]{0,1,1,0}, 1));
        shell.getAttributes().add(Skin.ATT_WEIGHTS_0, new VBO(new float[]{1,1,1,1}, 1));
        arm.setShape(shell);
        arm.setSkin(skin);
        scene.getChildren().add(arm);



        //keyboard and mouse control
        final DefaultMesh sshell = DefaultMesh.createFromGeometry(new Sphere(1));
        final DefaultModel zero = new DefaultModel(sshell);
        scene.getChildren().add(zero);
        final OrbitUpdater controller = new OrbitUpdater(frame,camera,zero).configureDefault();
        controller.setDistance(20);
        controller.setHorizontalAngle(Angles.degreeToRadian(30));
        controller.setVerticalAngle(Angles.degreeToRadian(25));
        camera.getUpdaters().add(controller);

        //animation to move the kinematic target
        final JointTimeSerie serie = new JointTimeSerie(edge);
        final JointKeyFrame frame0 = new JointKeyFrame(edge, 0);
        final JointKeyFrame frame1 = new JointKeyFrame(edge, 2000);
        final JointKeyFrame frame2 = new JointKeyFrame(edge, 4000);
        frame0.setValue(SimilarityNd.create(new Quaternion(0,0,0,1), new Vector3f64(1, 1, 1), new Vector3f64(15, 0, 0)));
        frame1.setValue(SimilarityNd.create(new Quaternion(0,0,0,1), new Vector3f64(1, 1, 1), new Vector3f64(-15, 0, 0)));
        frame2.setValue(SimilarityNd.create(new Quaternion(0,0,0,1), new Vector3f64(1, 1, 1), new Vector3f64(15, 0, 0)));
        serie.getFrames().add(frame0);
        serie.getFrames().add(frame1);
        serie.getFrames().add(frame2);
        final SkeletonAnimation anim = new SkeletonAnimation(skeleton);
        anim.getSeries().add(serie);
        anim.start();

        arm.getUpdaters().add(anim);

        //show the frame
        frame.setSize(640, 480);
        frame.setVisible(true);
    }

}
