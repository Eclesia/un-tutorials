
package demo.opengl._7_other;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.mesh.particle.ParticuleColorPresenter;
import science.unlicense.engine.opengl.mesh.particle.ParticuleShape;
import science.unlicense.engine.opengl.mesh.particle.ParticuleTexturePresenter;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.math.api.Angles;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 *
 * @author Johann Sorel
 */
public class ParticleDemo {

    public static void main(String[] args) throws IOException {


        //create a rendering frame
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera));

        //build colors emitter
        final ParticuleShape shape = new ParticuleShape();
        shape.setPresenter(new ParticuleColorPresenter(20));
        final DefaultModel emitter = new DefaultModel();
        emitter.setShape(shape);
        scene.getChildren().add(emitter);

        //build textured emitter
        final ParticuleShape shape2 = new ParticuleShape();
        shape2.setPresenter(new ParticuleTexturePresenter(1));
        final DefaultModel emitter2 = new DefaultModel();
        final Image image = Images.read(Paths.resolve(new Chars("mod:/images/unlicense.png")));

        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuseTexture(new Layer(new UVMapping(new Texture2D(image))));
        emitter2.getMaterials().add(material);
        emitter2.setShape(shape2);
        emitter2.getNodeTransform().getTranslation().setXYZ(5, 0, 0);
        emitter2.getNodeTransform().notifyChanged();
        scene.getChildren().add(emitter2);

        //build textured emitter, particule going upward
        final ParticuleShape shape3 = new ParticuleShape();
        final ParticuleTexturePresenter presenter = new ParticuleTexturePresenter(1);
        shape3.setPresenter(presenter);
        shape3.setNbParticule(10);
        shape3.setLifeSpan(10);
        shape3.setGravity(new Vector3f64(0, 1, 0));
        shape3.setSpawnRadius(10);
        final DefaultModel emitter3 = new DefaultModel();
        final SimpleBlinnPhong.Material material3 = SimpleBlinnPhong.newMaterial();
        material3.setDiffuseTexture(new Layer(new UVMapping(new Texture2D(image))));
        emitter3.getMaterials().add(material3);
        emitter3.setShape(shape3);
        emitter3.getNodeTransform().getTranslation().setXYZ(10, 0, 0);
        emitter3.getNodeTransform().notifyChanged();
        scene.getChildren().add(emitter3);


//        final Light light = new PointLight();
//        light.getNodeTransform().getTranslation().setXYZ(10, 20, 10);
//        light.getNodeTransform().notifyChanged();
//        scene.addChild(light);

        //keyboard and mouse control
        final MotionModel crosshair = DefaultMotionModel.createCrosshair3D();
        scene.getChildren().add(crosshair);
        final OrbitUpdater controller = new OrbitUpdater(frame,camera,crosshair).configureDefault();
        controller.setDistance(20);
        controller.setHorizontalAngle(Angles.degreeToRadian(30));
        controller.setVerticalAngle(Angles.degreeToRadian(25));
        camera.getUpdaters().add(controller);
        crosshair.getChildren().add(camera);
        crosshair.setVisible(true);

        //show the frame
        frame.setSize(640, 480);
        frame.setVisible(true);

    }

}