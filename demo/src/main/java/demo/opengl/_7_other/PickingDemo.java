

package demo.opengl._7_other;

import demo.opengl.GLCommons;
import demo.opengl._2_geometries._0_DefaultMeshDemo;
import java.nio.FloatBuffer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.light.Light;
import science.unlicense.display.impl.light.PointLight;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.DeferredRenderPhase;
import science.unlicense.engine.opengl.phase.FBOResizePhase;
import science.unlicense.engine.opengl.phase.GBO;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.phase.effect.DirectPhase;
import science.unlicense.engine.opengl.phase.picking.PickActor;
import science.unlicense.engine.opengl.phase.picking.PickMessage;
import science.unlicense.engine.opengl.phase.picking.PickResetPhase;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.engine.opengl.technique.GLActorTechnique;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Triangle;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.DefaultTriangle;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.math.api.Angles;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 * TODO
 *
 * @author Johann Sorel
 */
public class PickingDemo {

    public static void main(String[] args) throws IOException {

        final int width = 1024;
        final int height = 768;
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();

        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        final Sequence phases = context.getPhases();

        //prepare the fbo
        final GBO gbo = new GBO(width, height);
        final Texture2D texColor = (Texture2D) gbo.getDiffuseTexture();
        final Texture2D texPickMesh = (Texture2D) gbo.getMeshIdTexture();
        final Texture2D texPickVertex = (Texture2D) gbo.getVertexIdTexture();

        final PickResetPhase pickResetPhases = new PickResetPhase(texPickMesh, texPickVertex);

        //build a small scene
        for(int i=0;i<20;i++){
            final DefaultModel box = new DefaultModel(new BBox(new double[]{-2,-2,-2},new double[]{2,2,2}));
            box.getNodeTransform().getTranslation().setXYZ(
                    Math.random()*20-10,
                    Math.random()*20-10,
                    Math.random()*20-10);
            box.setId(new Chars("Cube number "+i));
            box.getNodeTransform().notifyChanged();
            box.getMaterials().add(GLCommons.createMaterial(GLCommons.randomColor()));
            scene.getChildren().add(box);

            //((VBO) box.getShape().getPositions()).setForgetOnLoad(false);
            //((VBO) box.getShape().getIndex()).setForgetOnLoad(false);

            //attach a pick actor on the meshes we want to pick
            //((GLActorTechnique)box.getRenderers().get(0)).getExtShaderActors().add(new PickActor(pickResetPhases, box));
        }

        final Light light1 = new PointLight();
        light1.getNodeTransform().getTranslation().setXYZ(+10, +10, +10);
        light1.getNodeTransform().notifyChanged();
        scene.getChildren().add(light1);
        final Light light2 = new PointLight();
        light2.getNodeTransform().getTranslation().setXYZ(-10, -10, -10);
        light2.getNodeTransform().notifyChanged();
        scene.getChildren().add(light2);

        final MotionModel crosshair = DefaultMotionModel.createCrosshair3D();
        scene.getChildren().add(crosshair);
        crosshair.getChildren().add(camera);
        final OrbitUpdater controller = new OrbitUpdater(frame,camera,crosshair).configureDefault();
        controller.setDistance(20);
        controller.setHorizontalAngle(Angles.degreeToRadian(30));
        controller.setVerticalAngle(Angles.degreeToRadian(25));
        camera.getUpdaters().add(controller);


        // 0 : clear the frame main buffer
        phases.add(new ClearPhase(gbo,new float[]{0f,0f,0f,1f}));
        // 1 : this phase updates the fbo used in render phases to match the display size
        phases.add(new FBOResizePhase());
        // 2 : update the world properly
        phases.add(new UpdatePhase(scene));
        // 3 : render the deferred scene in a FBO
        phases.add(new DeferredRenderPhase(scene,camera,gbo));
        // 4 : render
        phases.add(new DirectPhase(texColor));
        // 5 : picking work
        phases.add(pickResetPhases);


        //make some pick request on mouse press
        frame.addEventListener(MouseMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                final MouseMessage me = (MouseMessage) event.getMessage();
                if(me.getType() == MouseMessage.TYPE_PRESS){
                    pickResetPhases.pickAt(me.getMousePosition(), new EventListener() {
                        public void receiveEvent(Event event) {
                            final PickMessage pe = (PickMessage) event.getMessage();
                            if(pe.getSelection()!=null){
                                final DefaultModel mesh = (DefaultModel) pe.getSelection();
                                final int vid = pe.getPrimitiveId();
                                System.out.println(mesh.getId()+"  primitive id = "+ vid);

                                final int[] vids = pe.findVertexId(context);

                                //draw a sphere where the vertex is
                                final VBO vbo = (VBO) ((DefaultMesh)mesh.getShape()).getPositions();
                                final FloatBuffer fb = (FloatBuffer) vbo.getPrimitiveBuffer();
                                final Triangle triangle = new DefaultTriangle(
                                        new Vector3f64(fb.get(vids[0]*3+0), fb.get(vids[0]*3+1), fb.get(vids[0]*3+2)),
                                        new Vector3f64(fb.get(vids[1]*3+0), fb.get(vids[1]*3+1), fb.get(vids[1]*3+2)),
                                        new Vector3f64(fb.get(vids[2]*3+0), fb.get(vids[2]*3+1), fb.get(vids[2]*3+2)));

                                try{
                                    final DefaultModel t0 = _0_DefaultMeshDemo.buildMesh(DefaultMesh.createIcosphere(0.5),0);
                                    t0.getNodeTransform().getTranslation().set(triangle.getFirstCoord());
                                    t0.getNodeTransform().notifyChanged();
                                    ((GLActorTechnique)t0.getTechniques().get(0)).getExtShaderActors().add(new PickActor(pickResetPhases, t0));
                                    mesh.getChildren().add(t0);

                                    final DefaultModel t1 = _0_DefaultMeshDemo.buildMesh(DefaultMesh.createIcosphere(0.5),0);
                                    t1.getNodeTransform().getTranslation().set(triangle.getSecondCoord());
                                    t1.getNodeTransform().notifyChanged();
                                    ((GLActorTechnique)t1.getTechniques().get(0)).getExtShaderActors().add(new PickActor(pickResetPhases, t1));
                                    mesh.getChildren().add(t1);

                                    final DefaultModel t2 = _0_DefaultMeshDemo.buildMesh(DefaultMesh.createIcosphere(0.5),0);
                                    t2.getNodeTransform().getTranslation().set(triangle.getThirdCoord());
                                    t2.getNodeTransform().notifyChanged();
                                    ((GLActorTechnique)t2.getTechniques().get(0)).getExtShaderActors().add(new PickActor(pickResetPhases, t2));
                                    mesh.getChildren().add(t2);
                                }catch(Exception ex){
                                    ex.printStackTrace();
                                }

                            }else{
                                System.out.println("Nothing selected");
                            }
                        }
                    });
                }

            }
        });


        //show the frame
        frame.setSize(1024, 768);
        frame.setVisible(true);

    }

}
