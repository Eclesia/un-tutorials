
package demo.opengl._7_other;

/**
 *
 * @author Johann Sorel
 */
public class GeometryMaskDemo {

    public static void main(String[] args) {

        for(int y=0;y<4;y++){
            for(int x=0;x<4;x++){
                System.out.print("x"+x+" y"+y+" ");
                maskAt(new int[]{x,y});
            }
        }


    }

    public static void maskAt(int[] coord){
        int px = coord[0]/4;
        int py = coord[1]/4;
        int col = coord[0] - px*4;
        int row = coord[1] - py*4;
        int mask = 1 << ((4*row) + col);
        System.out.println(mask);
    }

//    public static void main(String[] args) throws IOException {
//
//        final Font font = new Font(new Chars[]{new Chars("Unicon")}, 20, Font.WEIGHT_NONE);
//        final Geometry2D glyph = font.getGlyph('\uE01C');
//
//        final Image image = GeometryMaskRasterizer.renderGlyph(glyph);
//        image.getModels().add(ImageModel.MODEL_COLOR, GeometryMaskRasterizer.COLOR_MODEL);
//
//        //rebuild 4x4 image
//        final TupleBuffer b = image.getRawModel().asTupleBuffer(image);
//        final int[] buffer = new int[1];
//        final long width = image.getExtent().getL(0);
//        final long height = image.getExtent().getL(1);
//        final Image all = Images.create(new Extent.Long(width*4, height*4), Images.IMAGE_TYPE_RGB);
//        final TupleBuffer o = all.getRawModel().asTupleBuffer(all);
//        for(int x=0;x<width;x++){
//            for(int y=0;y<height;y++){
//                b.getTupleUShort(new int[]{x,y}, buffer);
//                final int v = buffer[0];
//
//                byte r01 = (byte) ((v & 1) !=0 ? 255 : 0);
//                byte r02 = (byte) ((v & 2) !=0 ? 255 : 0);
//                byte r03 = (byte) ((v & 4) !=0 ? 255 : 0);
//                byte r04 = (byte) ((v & 8) !=0 ? 255 : 0);
//                byte r05 = (byte) ((v & 16) !=0 ? 255 : 0);
//                byte r06 = (byte) ((v & 32) !=0 ? 255 : 0);
//                byte r07 = (byte) ((v & 64) !=0 ? 255 : 0);
//                byte r08 = (byte) ((v & 128) !=0 ? 255 : 0);
//                byte r09 = (byte) ((v & 256) !=0 ? 255 : 0);
//                byte r10 = (byte) ((v & 512) !=0 ? 255 : 0);
//                byte r11 = (byte) ((v & 1024) !=0 ? 255 : 0);
//                byte r12 = (byte) ((v & 2048) !=0 ? 255 : 0);
//                byte r13 = (byte) ((v & 4096) !=0 ? 255 : 0);
//                byte r14 = (byte) ((v & 8192) !=0 ? 255 : 0);
//                byte r15 = (byte) ((v & 16384) !=0 ? 255 : 0);
//                byte r16 = (byte) ((v & 32768) !=0 ? 255 : 0);
//
//                o.setTuple(new int[]{x*4+0,y*4+0}, new byte[]{r01,r01,r01});
//                o.setTuple(new int[]{x*4+1,y*4+0}, new byte[]{r02,r02,r02});
//                o.setTuple(new int[]{x*4+2,y*4+0}, new byte[]{r03,r03,r03});
//                o.setTuple(new int[]{x*4+3,y*4+0}, new byte[]{r04,r04,r04});
//                o.setTuple(new int[]{x*4+0,y*4+1}, new byte[]{r05,r05,r05});
//                o.setTuple(new int[]{x*4+1,y*4+1}, new byte[]{r06,r06,r06});
//                o.setTuple(new int[]{x*4+2,y*4+1}, new byte[]{r07,r07,r07});
//                o.setTuple(new int[]{x*4+3,y*4+1}, new byte[]{r08,r08,r08});
//                o.setTuple(new int[]{x*4+0,y*4+2}, new byte[]{r09,r09,r09});
//                o.setTuple(new int[]{x*4+1,y*4+2}, new byte[]{r10,r10,r10});
//                o.setTuple(new int[]{x*4+2,y*4+2}, new byte[]{r11,r11,r11});
//                o.setTuple(new int[]{x*4+3,y*4+2}, new byte[]{r12,r12,r12});
//                o.setTuple(new int[]{x*4+0,y*4+3}, new byte[]{r13,r13,r13});
//                o.setTuple(new int[]{x*4+1,y*4+3}, new byte[]{r14,r14,r14});
//                o.setTuple(new int[]{x*4+2,y*4+3}, new byte[]{r15,r15,r15});
//                o.setTuple(new int[]{x*4+3,y*4+3}, new byte[]{r16,r16,r16});
//
//            }
//        }
//
//        final Frame frame = FrameManagers.getFrameManager().createFrame(false);
//
//        frame.getContainer().setLayout(new BorderLayout());
//
//        final WGraphicImage img = new WGraphicImage(image);
//        img.setFitting(WGraphicImage.FITTING_CENTERED);
//        frame.getContainer().addChild(img, BorderConstraint.CENTER);
//        frame.setSize(800, 600);
//        frame.setVisible(true);
//
//    }


}
