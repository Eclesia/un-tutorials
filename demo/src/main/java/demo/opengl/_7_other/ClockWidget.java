package demo.opengl._7_other;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import javax.swing.Timer;
import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.math.impl.Vector2f64;

/**
 * Display a clock widget at the bottom right of the desktop.
 *
 * @author Johann Sorel
 */
public class ClockWidget {

    public static void main(String[] args) {


        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        scene.getChildren().add(camera);
        context.getPhases().add(new ClearPhase(new float[]{0f,0f,0f,0f}));
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera));
        frame.setDecoration(null);

        //get the display size
        final Extent ext = frame.getManager().getDisplaySize();

        final WContainer container = frame.getContainer();
        container.setFrame(frame);
        container.setLayout(new BorderLayout());
        //remove the container background
//        WStyle css = container.getStyle().getClass(WContainer.STYLE_ENABLE);
//        css.setProperty(WContainer.STYLE_PROP_BACKGROUND, null);

        //display the clock
        final WLabel label = new WLabel(Chars.EMPTY);
//        css = label.getStyle().getClass(WLabel.STYLE_ENABLE);
//        css.setProperty(WLabel.STYLE_PROP_FONT, new Constant(new Font(new Chars[]{new Chars("Tuffy")}, 60, Font.WEIGHT_NONE)));
        //TODO need a time api and somekind of threading support
        Timer timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Calendar cal = Calendar.getInstance();
                int hour = cal.get(Calendar.HOUR_OF_DAY);
                int min = cal.get(Calendar.MINUTE);
                int sec = cal.get(Calendar.SECOND);
                label.setText(new Chars(hour + ":" + min + ":" + sec));
            }
        });
        timer.setRepeats(true);
        timer.setCoalesce(true);
        timer.start();

        container.addChild(label, BorderConstraint.CENTER);

        frame.setSize(400, 400);
        frame.setOnScreenLocation(new Vector2f64(ext.get(0) - 400, ext.get(1) - 400));
        frame.setVisible(true);



    }
}
