

package demo.opengl._7_other;

import demo.opengl.GLCommons;
import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.Quad;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 * Enable depth sorting.
 *
 * @author Johann Sorel
 */
public class DepthSorting {

    public static void main(String[] args) {

        //build scene
        final Quad quad1 = new Quad(10, 10);
        final DefaultModel mesh1 = new DefaultModel(quad1);
        mesh1.setId(new Chars("red"));
        mesh1.getShape().getBoundingBox();
        mesh1.getMaterials().add(GLCommons.createMaterial(new ColorRGB(1.0f,0.0f,0.0f,0.5f)));

        final Quad quad2 = new Quad(10, 10);
        final DefaultModel mesh2 = new DefaultModel(quad2);
        mesh2.setId(new Chars("green"));
        mesh2.getShape().getBoundingBox();
        mesh2.getMaterials().add(GLCommons.createMaterial(new ColorRGB(0.0f,1.0f,0.0f,0.5f)));
        mesh2.getNodeTransform().getTranslation().setXYZ(0, 0, 2);
        mesh2.getNodeTransform().notifyChanged();

        final Quad quad3 = new Quad(10, 10);
        final DefaultModel mesh3 = new DefaultModel(quad3);
        mesh3.setId(new Chars("blue"));
        mesh3.getShape().getBoundingBox();
        mesh3.getMaterials().add(GLCommons.createMaterial(new ColorRGB(0.0f,0.0f,1.0f,0.5f)));
        mesh3.getNodeTransform().getTranslation().setXYZ(0, 0, 4);
        mesh3.getNodeTransform().notifyChanged();

        final MotionModel crosshair = DefaultMotionModel.createCrosshair3D();
        final MonoCamera camera = new MonoCamera();

        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        scene.getChildren().add(mesh1);
        scene.getChildren().add(mesh2);
        scene.getChildren().add(mesh3);
        scene.getChildren().add(crosshair);
        scene.getChildren().add(camera);


        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();

        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene, camera));

        final OrbitUpdater control = new OrbitUpdater(frame, camera,crosshair).configureDefault();
        camera.getUpdaters().add(control);

        frame.setSize(800, 600);
        frame.setVisible(true);

    }

}
