

package demo.opengl._7_other;

import demo.opengl.GLCommons;
import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.light.DirectionalLight;
import science.unlicense.display.impl.light.Light;
import science.unlicense.display.impl.light.PointLight;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.ReflectionMapping;
import science.unlicense.engine.opengl.mesh.SkyBox;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.phase.reflection.ReflectionPhase;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.Capsule;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.AlphaBlending;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.api.Angles;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 * A Mirror demo.
 *
 * @author Johann Sorel
 */
public class Mirror {

    public static void main(String[] args) throws IOException {

        //create a rendering frame
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        camera.setNearPlane(0.0001);
        camera.setFarPlane(1000);
        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new UpdatePhase(scene));
        final RenderPhase renderPhase = new RenderPhase(scene,camera);
        context.getPhases().add(new ReflectionPhase(renderPhase));
        context.getPhases().add(renderPhase);

        try{
            final SkyBox skyBox = new SkyBox(
                Images.read(Paths.resolve(new Chars("mod:/images/skybox/up.jpg"))),
                Images.read(Paths.resolve(new Chars("mod:/images/skybox/down.jpg"))),
                Images.read(Paths.resolve(new Chars("mod:/images/skybox/left.jpg"))),
                Images.read(Paths.resolve(new Chars("mod:/images/skybox/right.jpg"))),
                Images.read(Paths.resolve(new Chars("mod:/images/skybox/front.jpg"))),
                Images.read(Paths.resolve(new Chars("mod:/images/skybox/back.jpg"))));
            scene.getChildren().add(skyBox);
        }catch(Exception ex){
            ex.printStackTrace();
        }

        //build the scene
        final DefaultModel mesh = new DefaultModel(new Capsule(5, 1));
        mesh.getNodeTransform().getTranslation().setXYZ(0, 6, 0);
        mesh.getNodeTransform().notifyChanged();
        mesh.getMaterials().add(GLCommons.createMaterial(Color.GRAY_NORMAL));
        scene.getChildren().add(mesh);

        final DefaultModel mirror = buildMirror();
        scene.getChildren().add(mirror);

        final Light light = new PointLight();
        light.getNodeTransform().getTranslation().setXYZ(10, 20, 10);
        light.getNodeTransform().notifyChanged();
        scene.getChildren().add(light);

        final DirectionalLight dl = new DirectionalLight();
        scene.getChildren().add(dl);


        //keyboard and mouse control
        final MotionModel crosshair = DefaultMotionModel.createCrosshair3D();
        scene.getChildren().add(crosshair);
        final OrbitUpdater controller = new OrbitUpdater(frame,camera,crosshair).configureDefault();
        controller.setDistance(20);
        controller.setHorizontalAngle(Angles.degreeToRadian(30));
        controller.setVerticalAngle(Angles.degreeToRadian(25));
        camera.getUpdaters().add(controller);
        crosshair.getChildren().add(camera);

        //show the frame
        frame.setSize(640, 480);
        frame.setVisible(true);
    }

    public static DefaultModel buildMirror() throws IOException{
        double size = 10;

        final DefaultModel plan = new DefaultModel();
        plan.setShape(DefaultMesh.createPlan(
                new Vector3f64(-size, 0, -size),
                new Vector3f64(-size, 0, +size),
                new Vector3f64(+size, 0, +size),
                new Vector3f64(+size, 0, -size)));
        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuse(new ColorRGB(255, 255, 255, 100));
        material.setDiffuseTexture(new Layer(new ReflectionMapping(),Layer.TYPE_DIFFUSE,AlphaBlending.DEFAULT));
        plan.getMaterials().add(material);
        GLEngineUtils.makeCompatible(plan);

        return plan;

    }


}
