
package demo.opengl._0_getting_started;

import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.Angles;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 * Minimal scene demo, displaying a cube and basic gesture control.
 *
 * @author Johann Sorel
 */
public class _1_CubeDemo {

    public static void main(String[] args) throws OperationException {

        //create a rendering frame
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        scene.getChildren().add(camera);
        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera));

        //build a small scene
        final DefaultMesh shell = DefaultMesh.createFromGeometry(new BBox(new double[]{-5,-5,-5},new double[]{5,5,5}));
        final DefaultModel box = new DefaultModel();
        box.setShape(shell);
        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuse(Color.WHITE);
        box.getMaterials().add(material);
        scene.getChildren().add(box);

        //keyboard and mouse control
        final MotionModel crosshair = DefaultMotionModel.createCrosshair3D();
        scene.getChildren().add(crosshair);
        crosshair.getChildren().add(camera);
        final OrbitUpdater controller = new OrbitUpdater(frame,camera,crosshair).configureDefault();
        controller.setDistance(20);
        controller.setHorizontalAngle(Angles.degreeToRadian(30));
        controller.setVerticalAngle(Angles.degreeToRadian(25));
        camera.getUpdaters().add(controller);

        //show the frame
        frame.setSize(640, 480);
        frame.setVisible(true);
    }

}
