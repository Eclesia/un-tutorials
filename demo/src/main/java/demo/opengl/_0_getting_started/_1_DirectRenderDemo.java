
package demo.opengl._0_getting_started;

import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.phase.AbstractPhase;
import science.unlicense.gpu.api.GLException;
import science.unlicense.gpu.api.opengl.GL;
import science.unlicense.gpu.api.opengl.GL1;
import science.unlicense.gpu.api.opengl.GLC;

/**
 * Minimal scene demo, displaying a cube and basic gesture control.
 *
 * @author Johann Sorel
 */
public class _1_DirectRenderDemo {

    public static void main(String[] args) {

        //create a rendering frame
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        context.getPhases().add(new PaintPhase());
        
        //show the frame
        frame.setSize(640, 480);
        frame.setVisible(true);
    }
    
    private static class PaintPhase extends AbstractPhase {

        @Override
        protected void processInt(GLProcessContext context) throws GLException {
            super.processInt(context);
            
            final GL gl = context.getGL();
            final GL1 gl1 = gl.asGL1();
            gl1.glViewport(0, 0, 200, 200);
            gl1.glColor3f(1, 0, 0);
            gl1.glBegin(GLC.GL_TRIANGLES);
            gl1.glVertex3f(-1.0f, -0.5f, -4.0f);
            gl1.glVertex3f( 1.0f, -0.5f, -4.0f);
            gl1.glVertex3f( 0.0f,  0.5f, -4.0f);
            gl1.glEnd();
            
        }
        
    }

}
