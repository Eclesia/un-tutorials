
package demo.opengl._0_getting_started;

import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;




/**
 * Minimal opengl frame demo.
 *
 * @author Johann Sorel
 */
public class _0_FrameDemo {

    public static void main(String[] args) {

        //create a rendering frame
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);

        //show the frame
        frame.setSize(640, 480);
        frame.setVisible(true);
    }

}
