
package demo.opengl._2_geometries;

import demo.opengl.GLCommons;
import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.updater.FirstPersonUpdater;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.mesh.SkyBox;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.image.api.Images;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.model3d.impl.scene.DefaultModel;

/**
 * Skybox demo
 *
 * @author Johann Sorel
 */
public class _4_SkyBoxDemo {

    public static void main(String[] args) throws IOException {

        //create a rendering frame
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        scene.getChildren().add(camera);
        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera));

        //keyboard and mouse control
        final FirstPersonUpdater controller = new FirstPersonUpdater(frame, camera, new Vector3f64(0, 1, 0), new Vector3f64(1, 0, 0)).configureFlyingControls();
        camera.getUpdaters().add(controller);
        camera.setFarPlane(500);

        //build the scene
        buildScene(scene,camera);

        //show the frame
        frame.setSize(640, 480);
        frame.setVisible(true);
    }

    public static void buildScene(SceneNode scene, MonoCamera camera) throws IOException{

        //a random ground
        final DefaultModel ground = GLCommons.buildGround();
        scene.getChildren().add(ground);

        final SkyBox skyBox = new SkyBox(
            Images.read(Paths.resolve(new Chars("mod:/images/skybox/up.jpg"))),
            Images.read(Paths.resolve(new Chars("mod:/images/skybox/down.jpg"))),
            Images.read(Paths.resolve(new Chars("mod:/images/skybox/left.jpg"))),
            Images.read(Paths.resolve(new Chars("mod:/images/skybox/right.jpg"))),
            Images.read(Paths.resolve(new Chars("mod:/images/skybox/front.jpg"))),
            Images.read(Paths.resolve(new Chars("mod:/images/skybox/back.jpg"))));
        scene.getChildren().add(skyBox);
    }

}
