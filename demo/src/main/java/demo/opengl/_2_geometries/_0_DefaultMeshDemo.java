
package demo.opengl._2_geometries;

import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.light.PointLight;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.technique.GLTechnique;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.Capsule;
import science.unlicense.geometry.impl.Cone;
import science.unlicense.geometry.impl.Cylinder;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.Disk;
import science.unlicense.geometry.impl.Ellipsoid;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 * Using the default meshes.
 *
 * @author Johann Sorel
 */
public class _0_DefaultMeshDemo {

    public static void main(String[] args) throws OperationException {

        //create a rendering frame
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera));

        //build the scene
        buildScene(scene);

        final PointLight light = new PointLight();
        light.getNodeTransform().getTranslation().setXYZ(8, 8, 0);
        light.getNodeTransform().notifyChanged();
        scene.getChildren().add(light);

        //keyboard and mouse control
        final MotionModel cameraTarget = DefaultMotionModel.createCrosshair3D();
        scene.getChildren().add(cameraTarget);
        OrbitUpdater controller = new OrbitUpdater(frame,camera,cameraTarget).configureDefault();
        controller.setDistance(10);
        controller.setHorizontalAngle(45);
        controller.setVerticalAngle(45);
        camera.getUpdaters().add(controller);
        cameraTarget.getChildren().add(camera);

        //show the frame
        frame.setSize(640, 480);
        frame.setVisible(true);
    }

    public static void buildScene(SceneNode scene) throws OperationException{

        scene.getChildren().add(buildMesh(new BBox(new Extent.Double(5, 5,5)), -20));
        scene.getChildren().add(buildMesh(DefaultMesh.createIcosphere(4),    -10));
        scene.getChildren().add(buildMesh(new Disk(3),           0));
        scene.getChildren().add(buildMesh(new Sphere(3),        10));
        scene.getChildren().add(buildMesh(new Ellipsoid(2,6,2), 20));
        scene.getChildren().add(buildMesh(new Capsule(4,2),     30));
        scene.getChildren().add(buildMesh(new Cylinder(4,2),    40));
        scene.getChildren().add(buildMesh(new Cone(4,2),        50));

    }

    public static DefaultModel buildMesh(Geometry geom, int offset) throws OperationException{
        final DefaultMesh shell = DefaultMesh.createFromGeometry(geom,20,20);
        final DefaultModel mesh = DefaultModel.createFromGeometry(shell);
        return buildMesh(mesh, offset);
    }

    public static DefaultModel buildMesh(DefaultModel mesh, int offset){
        ((GLTechnique)mesh.getTechniques().get(0)).getState().setPolygonMode(GLC.POLYGON_MODE.LINE);
        mesh.getNodeTransform().getTranslation().setXYZ(offset, 0, 0);
        mesh.getNodeTransform().notifyChanged();
        return mesh;
    }

}
