
package demo.opengl._2_geometries;

import science.unlicense.common.api.collection.primitive.FloatSequence;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.light.PointLight;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.mesh.InstancingShell;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.engine.opengl.technique.GLTechnique;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 * Using the default instancing mechanism.
 *
 * @author Johann Sorel
 */
public class _7_InstancingDemo {

    public static void main(String[] args) throws OperationException {

        //create a rendering frame
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera));

        //build the scene
        buildScene(scene);

        final PointLight light = new PointLight();
        light.getNodeTransform().getTranslation().setXYZ(8, 8, 0);
        light.getNodeTransform().notifyChanged();
        scene.getChildren().add(light);

        //keyboard and mouse control
        final MotionModel cameraTarget = DefaultMotionModel.createCrosshair3D();
        scene.getChildren().add(cameraTarget);
        OrbitUpdater controller = new OrbitUpdater(frame,camera,cameraTarget).configureDefault();
        controller.setDistance(10);
        controller.setHorizontalAngle(45);
        controller.setVerticalAngle(45);
        camera.getUpdaters().add(controller);
        cameraTarget.getChildren().add(camera);

        //show the frame
        frame.setSize(640, 480);
        frame.setVisible(true);
    }

    public static void buildScene(SceneNode scene) throws OperationException{

        final DefaultMesh shell = DefaultMesh.createFromGeometry(new BBox(new Extent.Double(5, 5, 5)));
        final DefaultModel base = new DefaultModel(shell);

        final InstancingShell ishell = new InstancingShell();
        ishell.set((DefaultMesh) base.getShape());

        final FloatSequence seq = new FloatSequence();
        final Matrix4x4 inst1 = new Matrix4x4(
                1,0,0,0,
                0,1,0,0,
                0,0,1,0,
                0,0,0,1);
        final Matrix4x4 inst2 = new Matrix4x4(
                1,0,0,10,
                0,1,0,0,
                0,0,1,0,
                0,0,0,1);
        final Matrix4x4 inst3 = new Matrix4x4(
                1,0,0,0,
                0,1,0,10,
                0,0,1,0,
                0,0,0,1);
        final Matrix4x4 inst4 = new Matrix4x4(
                1,0,0,0,
                0,1,0,0,
                0,0,1,10,
                0,0,0,1);
        seq.put(inst1.toArrayFloatColOrder());
        seq.put(inst2.toArrayFloatColOrder());
        seq.put(inst3.toArrayFloatColOrder());
        seq.put(inst4.toArrayFloatColOrder());
        final float[] off = seq.toArrayFloat();

        final VBO inst = new VBO(off, 16);
        inst.setInstancingDividor(1);
        ishell.setInstanceTransforms(inst);


        final DefaultModel mesh = new DefaultModel();
        ((GLTechnique)mesh.getTechniques().get(0)).getState().setPolygonMode(GLC.POLYGON_MODE.LINE);
        mesh.setShape(ishell);

        scene.getChildren().add(mesh);
    }

}
