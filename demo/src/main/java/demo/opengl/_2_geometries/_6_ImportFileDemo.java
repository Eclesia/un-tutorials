
package demo.opengl._2_geometries;

import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.light.Attenuation;
import science.unlicense.display.impl.light.PointLight;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.engine.ui.component.path.WPathChooser;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.model3d.api.Model3Ds;
import science.unlicense.model3d.api.SceneResource;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 * Open a 3d file demo.
 *
 * @author Johann Sorel
 */
public class _6_ImportFileDemo {

    public static void main(String[] args) throws IOException, StoreException {

        //create a rendering frame
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        camera.setNearPlane(0.01);
        camera.setFarPlane(10000);
        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera));

        //build the scene
        buildScene(scene);

        final PointLight light = new PointLight();
        light.setAttenuation(new Attenuation(1, 0, 0));
        light.getNodeTransform().getTranslation().setXYZ(0, 0, 10);
        light.getNodeTransform().notifyChanged();
        scene.getChildren().add(light);

        //keyboard and mouse control
        final MotionModel cameraTarget = DefaultMotionModel.createCrosshair3D();
        scene.getChildren().add(cameraTarget);
        OrbitUpdater controller = new OrbitUpdater(frame,camera,cameraTarget).configureDefault();
        controller.setDistance(10);
        controller.setHorizontalAngle(45);
        controller.setVerticalAngle(45);
        camera.getUpdaters().add(controller);
        cameraTarget.getChildren().add(camera);

        //show the frame
        frame.setSize(640, 480);
        frame.setVisible(true);
    }

    public static void buildScene(SceneNode scene) throws IOException, StoreException{

        final Path input = WPathChooser.showOpenDialog((Predicate)null);
        final Store store = Model3Ds.open(input);
        final SceneResource res = (SceneResource) store;
        final Iterator ite = res.getElements().createIterator();
        while(ite.hasNext()){
            final Object candidate = ite.next();
            if(candidate instanceof SceneNode){
                scene.getChildren().add((Node) candidate);
            }
        }

    }

}
