
package demo.opengl._2_geometries;

import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.ByVertexColorMapping;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.math.api.Angles;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 * Creating custom meshes.
 *
 * @author Johann Sorel
 */
public class _1_CustomMeshDemo {

    public static void main(String[] args) {

        //create a rendering frame
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        scene.getChildren().add(camera);
        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera));

        //build the scene
        buildScene(scene);

        //keyboard and mouse control
        final OrbitUpdater controller = new OrbitUpdater(frame,camera,scene).configureDefault();
        controller.setDistance(20);
        controller.setHorizontalAngle(Angles.degreeToRadian(30));
        controller.setVerticalAngle(Angles.degreeToRadian(25));
        camera.getUpdaters().add(controller);

        //show the frame
        frame.setSize(640, 480);
        frame.setVisible(true);
    }

    public static void buildScene(SceneNode scene){

        //a triangle ///////////////////////////////////////////////////////////
//        final Mesh triangle = buildTriangle();
//        scene.addChild(triangle);

        final DefaultModel fan = buildTriangleFan();
        scene.getChildren().add(fan);

    }

    public static DefaultModel buildTriangle(){

        //build the shell
        final VBO vertices = new VBO(new float[]{
            0,0,0,
            0,1,0,
            1,0,0}, 3);
        final VBO normals = new VBO(new float[]{
            0,0,1,
            0,0,1,
            0,0,1}, 3);
        final IBO indexes = new IBO(new int[]{
            0,1,2});

        final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
        shell.setPositions(vertices);
        shell.setNormals(normals);
        shell.setIndex(indexes);
        shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLES(0, 3)});

        //build the material
        final float[] colors = new float[]{
            1,0,0,
            0,1,0,
            0,0,1};

        final ByVertexColorMapping paint = new ByVertexColorMapping();
        paint.setColors(DefaultBufferFactory.wrap(colors));
        paint.setHasAlpha(false);

        final DefaultModel triangle = new DefaultModel();
        triangle.setShape(shell);


        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuseTexture(new Layer(paint));
        triangle.getMaterials().add(material);

        return triangle;
    }

    public static DefaultModel buildTriangleFan(){

        double step = (Math.PI/6);
        double angle1 = 1 * step;
        double angle2 = 2 * step;
        double angle3 = 3 * step;
        double angle4 = 4 * step;
        double angle5 = 5 * step;
        //build the shell
        final VBO vertices = new VBO(new float[]{
            0,0,0,
            (float)Math.cos(angle1),(float)Math.sin(angle1),0,
            (float)Math.cos(angle2),(float)Math.sin(angle2),0,
            (float)Math.cos(angle3),(float)Math.sin(angle3),0,
            (float)Math.cos(angle4),(float)Math.sin(angle4),0,
            (float)Math.cos(angle5),(float)Math.sin(angle5),0
            }, 3);
        final VBO normals = new VBO(new float[]{
            0,0,1,
            0,0,1,
            0,0,1,
            0,0,1,
            0,0,1,
            0,0,1}, 3);
        final IBO indexes = new IBO(new int[]{
            0,1,2,
            3,
            4,
            5,
            6,
            7});

        final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
        shell.setPositions(vertices);
        shell.setNormals(normals);
        shell.setIndex(indexes);
        shell.setRanges(new IndexedRange[]{IndexedRange.TRIANGLE_FAN(0, 8)});

        //build the material
        final float[] colors = new float[]{
            1,1,1,
            0,0,0,
            1,0,0,
            0,1,0,
            0,0,1,
            1,1,0};

        final ByVertexColorMapping paint = new ByVertexColorMapping();
        paint.setColors(DefaultBufferFactory.wrap(colors));
        paint.setHasAlpha(false);

        final DefaultModel triangle = new DefaultModel();
        triangle.setShape(shell);

        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuseTexture(new Layer(paint));
        triangle.getMaterials().add(material);

        return triangle;
    }

}
