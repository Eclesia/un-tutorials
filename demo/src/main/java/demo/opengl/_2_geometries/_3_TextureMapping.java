

package demo.opengl._2_geometries;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.light.PointLight;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.CubeMapping;
import science.unlicense.engine.opengl.material.mapping.DualParaboloidMapping;
import science.unlicense.engine.opengl.material.mapping.SphericalMapping;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.Capsule;
import science.unlicense.geometry.impl.Sphere;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.TextureCube;
import science.unlicense.image.api.Images;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 * Various texture mapping solutions.
 *
 * @author Johann Sorel
 */
public class _3_TextureMapping {

    public static void main(String[] args) throws IOException, OperationException {

        //create a rendering frame
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera));

        //build the scene
        buildSphericalMappings(scene);
        buildCubeMappings(scene);
        buildDualParaMappings(scene);

        final PointLight light = new PointLight();
        light.getNodeTransform().getTranslation().setXYZ(8, 8, 0);
        light.getNodeTransform().notifyChanged();
        scene.getChildren().add(light);

        //keyboard and mouse control
        final MotionModel cameraTarget = DefaultMotionModel.createCrosshair3D();
        scene.getChildren().add(cameraTarget);
        OrbitUpdater controller = new OrbitUpdater(frame,new Vector3f64(0, 1, 0), new Vector3f64(1, 0, 0), camera, cameraTarget).configureDefault();
        controller.setDistance(40);
        controller.setHorizontalAngle(Math.toRadians(45));
        controller.setVerticalAngle(Math.toRadians(20));
        camera.getUpdaters().add(controller);
        cameraTarget.getChildren().add(camera);

        //show the frame
        frame.setSize(640, 480);
        frame.setVisible(true);
    }

    public static void buildSphericalMappings(SceneNode scene) throws IOException, OperationException{
        scene.getChildren().add(buildTextureSphericalMesh(new Sphere(3),        -10));
        scene.getChildren().add(buildTextureSphericalMesh(new BBox(new double[]{-3,-3,-3},new double[]{3,3,3}), 0));
        scene.getChildren().add(buildTextureSphericalMesh(new Capsule(4,2),     10));
    }

    public static void buildDualParaMappings(SceneNode scene) throws IOException, OperationException{
        scene.getChildren().add(buildTextureDualParaMesh(new Sphere(3),        -10));
        scene.getChildren().add(buildTextureDualParaMesh(new BBox(new double[]{-3,-3,-3},new double[]{3,3,3}), 0));
        scene.getChildren().add(buildTextureDualParaMesh(new Capsule(4,2),     10));
    }

    public static void buildCubeMappings(SceneNode scene) throws IOException, OperationException{
        scene.getChildren().add(buildTextureCubeMesh(new Sphere(3),        -10));
        scene.getChildren().add(buildTextureCubeMesh(new BBox(new double[]{-3,-3,-3},new double[]{3,3,3}), 0));
        scene.getChildren().add(buildTextureCubeMesh(new Capsule(4,2),     10));
    }

    public static DefaultModel buildTextureSphericalMesh(Geometry geom, int offset) throws IOException, OperationException{
        final DefaultMesh shell = DefaultMesh.createFromGeometry(geom,20,20);
        final DefaultModel mesh = new DefaultModel(shell);
        mesh.getNodeTransform().getTranslation().setXYZ(offset, 10, 0);
        mesh.getNodeTransform().notifyChanged();

        final Texture2D spherical = new Texture2D(Images.read(Paths.resolve(new Chars("mod:/images/skybox/down.jpg"))));
        final SphericalMapping mapping = new SphericalMapping(spherical);

        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuseTexture(new Layer(mapping));
        mesh.getMaterials().add(material);

        return mesh;
    }

    public static DefaultModel buildTextureDualParaMesh(Geometry geom, int offset) throws IOException, OperationException{
        final DefaultMesh shell = DefaultMesh.createFromGeometry(geom,20,20);
        final DefaultModel mesh = new DefaultModel(shell);
        mesh.getNodeTransform().getTranslation().setXYZ(offset, -10, 0);
        mesh.getNodeTransform().notifyChanged();

        final Texture2D front = new Texture2D(Images.read(Paths.resolve(new Chars("mod:/images/skybox/front.jpg"))));
        final Texture2D back = new Texture2D(Images.read(Paths.resolve(new Chars("mod:/images/skybox/back.jpg"))));
        final DualParaboloidMapping mapping = new DualParaboloidMapping(front,back);

        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuseTexture(new Layer(mapping));
        mesh.getMaterials().add(material);

        return mesh;
    }

    public static DefaultModel buildTextureCubeMesh(Geometry geom, int offset) throws IOException, OperationException{
        final DefaultMesh shell = DefaultMesh.createFromGeometry(geom,20,20);
        final DefaultModel mesh = new DefaultModel(shell);
        mesh.getNodeTransform().getTranslation().setXYZ(offset, 0, 0);
        mesh.getNodeTransform().notifyChanged();

        final TextureCube cube = new TextureCube();
        cube.setImagePositiveX(Images.read(Paths.resolve(new Chars("mod:/images/skybox/right.jpg"))));
        cube.setImagePositiveY(Images.read(Paths.resolve(new Chars("mod:/images/skybox/up.jpg"))));
        cube.setImagePositiveZ(Images.read(Paths.resolve(new Chars("mod:/images/skybox/front.jpg"))));
        cube.setImageNegativeX(Images.read(Paths.resolve(new Chars("mod:/images/skybox/left.jpg"))));
        cube.setImageNegativeY(Images.read(Paths.resolve(new Chars("mod:/images/skybox/down.jpg"))));
        cube.setImageNegativeZ(Images.read(Paths.resolve(new Chars("mod:/images/skybox/back.jpg"))));
        final CubeMapping mapping = new CubeMapping(cube);

        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuseTexture(new Layer(mapping));
        mesh.getMaterials().add(material);
        return mesh;
    }

}
