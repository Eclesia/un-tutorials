
package demo.opengl._2_geometries;

import science.unlicense.common.api.buffer.DefaultBufferFactory;
import science.unlicense.common.api.buffer.Float32Cursor;
import science.unlicense.common.api.buffer.Int32Cursor;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.updater.FirstPersonUpdater;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.mesh.BillBoard;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.api.Maths;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 * Using billboard meshes.
 *
 * @author Johann Sorel
 */
public class _2_BillboardDemo {

    public static void main(String[] args) throws IOException {

        //create a rendering frame
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        scene.getChildren().add(camera);
        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera));

        //this demo is in full screen, we quit when echap is pressed.
        frame.addEventListener(KeyMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                KeyMessage ke = (KeyMessage) event.getMessage();
                if(ke.getCodePoint() == 27){ //27 == echap
                    System.exit(0);
                }
            }
        });

        //keyboard and mouse control
        final FirstPersonUpdater controller = new FirstPersonUpdater(frame, camera, new Vector3f64(0, 1, 0), new Vector3f64(1, 0, 0)).configureFlyingControls();
        camera.getUpdaters().add(controller);
        camera.setFarPlane(500);

        //build the scene
        buildScene(scene,camera);

        //show the frame
        frame.setSize(800, 600);
        frame.setVisible(true);
    }

    public static void buildScene(SceneNode scene, MonoCamera camera) throws IOException{

        //a random ground
        final DefaultModel ground = buildGround();
        scene.getChildren().add(ground);

        //place multiple tree on our scene
        final Texture2D treeImage = new Texture2D(Images.read(
                Paths.resolve(new Chars("mod:/images/tree.png"))));

        for(int i=0;i<500;i++){
            final SceneNode tree = buildTree(camera,treeImage);
            tree.getNodeTransform().getTranslation().set(new Vector3f64((Math.random()*100)-50, 0, (Math.random()*100)-50));
            tree.getNodeTransform().notifyChanged();
            scene.getChildren().add(tree);
        }

    }

    public static SceneNode buildTree(MonoCamera camera, Texture2D treeImage) throws IOException{

        final BillBoard billboard = new BillBoard();
        billboard.setSize(new float[]{2f,2f});
        billboard.setPoints(new VBO(new float[]{0,0,0}, 3));
        billboard.setScreenCS(false);
        billboard.setFixedAxis(new Vector3f64(0, 1, 0));

        final DefaultModel mesh = new DefaultModel();
        mesh.setShape(billboard);
        final double scale = 3* Maths.clamp(Math.random()*1.4, 0.3, 2);
        mesh.getNodeTransform().getScale().localScale(scale);
        mesh.getNodeTransform().notifyChanged();

        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuseTexture(new Layer(new UVMapping(treeImage)));
        mesh.getMaterials().add(material);

        final SceneNode node = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        node.getChildren().add(mesh);

        return node;
    }

    public static DefaultModel buildGround(){
        final Float32Cursor vertices = DefaultBufferFactory.INSTANCE.createFloat32(100*2*2*3).cursor();
        final Float32Cursor normals = DefaultBufferFactory.INSTANCE.createFloat32(100*2*2*3).cursor();
        final Int32Cursor indexes = DefaultBufferFactory.INSTANCE.createInt32(100*2*2*2).cursor();

        final float[] normal = new float[]{0,0,1};
        int i=0;
        for(int x=-50;x<50;x++){
            vertices.write(x).write(0).write(-50);
            vertices.write(x).write(0).write(+50);
            normals.write(normal);
            normals.write(normal);
            indexes.write(i++);
            indexes.write(i++);
        }
        for(int z=-50;z<50;z++){
            vertices.write(-50).write(0).write(z);
            vertices.write(+50).write(0).write(z);
            normals.write(normal);
            normals.write(normal);
            indexes.write(i++);
            indexes.write(i++);
        }

        final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
        shell.setPositions(new VBO(vertices.getBuffer(), 3));
        shell.setNormals(new VBO(normals.getBuffer(), 3));
        shell.setIndex(new IBO(indexes.getBuffer()));
        shell.setRanges(new IndexedRange[]{IndexedRange.LINES(0, (int) indexes.getBuffer().getNumbersSize())});

        final DefaultModel mesh = new DefaultModel();
        mesh.setShape(shell);


        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuse(new ColorRGB(170, 150, 60));
        mesh.getMaterials().add(material);

        return mesh;
    }

}
