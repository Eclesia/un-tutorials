
package demo.opengl._2_geometries;

import demo.opengl.GLCommons;
import demo.ui.WidgetShowcase;
import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.DisplayTimerState;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.updater.FirstPersonUpdater;
import science.unlicense.display.impl.updater.Updater;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.mesh.SkyBox;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.widget.WGLPlane;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.image.api.Images;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.scene.DefaultModel;

/**
 * Embeding widgets in a 3D scene.
 *
 * @author Johann Sorel
 */
public class _5_WidgetsDemo {

    public static void main(String[] args) throws Exception {

        //create a rendering frame
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        scene.getChildren().add(camera);
        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera));

        //an FPS like camera control : see chapter 8
        buildCameraControl(frame, camera);

        //build the scene
        buildScene(frame,scene,camera);

        //show the frame
        frame.setSize(1024, 768);
        frame.setVisible(true);
    }

    public static void buildScene(GLUIFrame frame, SceneNode scene, MonoCamera camera) throws IOException, Exception{

        //a random ground
        final DefaultModel ground = GLCommons.buildGround();
        scene.getChildren().add(ground);

        final SkyBox skyBox = new SkyBox(
            Images.read(Paths.resolve(new Chars("mod:/images/skybox/up.jpg"))),
            Images.read(Paths.resolve(new Chars("mod:/images/skybox/down.jpg"))),
            Images.read(Paths.resolve(new Chars("mod:/images/skybox/left.jpg"))),
            Images.read(Paths.resolve(new Chars("mod:/images/skybox/right.jpg"))),
            Images.read(Paths.resolve(new Chars("mod:/images/skybox/front.jpg"))),
            Images.read(Paths.resolve(new Chars("mod:/images/skybox/back.jpg"))));
        scene.getChildren().add(skyBox);

        //build widgets panel
        final WGLPlane plane = new WGLPlane(1000,1000,camera);
        frame.addEventListener(MouseMessage.PREDICATE, plane);
        frame.addEventListener(KeyMessage.PREDICATE, plane);
        scene.getChildren().add(plane);
        plane.getNodeTransform().getScale().setXYZ(5, 5, 1);
        plane.getNodeTransform().getTranslation().setXYZ(0, 2, 0);
        plane.getNodeTransform().notifyChanged();

        final WContainer container = plane.getContainer();
        container.setFrame(frame);
        WidgetShowcase.fillContainer(container);

    }

    public static void buildCameraControl(GLUIFrame frame, final MonoCamera camera){
        //keyboard and mouse control
        final FirstPersonUpdater controller = new FirstPersonUpdater(frame, camera, new Vector3f64(0, 1, 0), new Vector3f64(1, 0, 0)).configureFirstPersonControls();
        camera.getNodeTransform().getTranslation().setXYZ(0, 5, 20);
        camera.getNodeTransform().notifyChanged();
        camera.getUpdaters().add(controller);
        camera.setFarPlane(500);

        //we want the camera to have a fixed elevation
        camera.getUpdaters().add(new Updater() {
            public void update(DisplayTimerState timing) {
                camera.getNodeTransform().getTranslation().setY(5);
                camera.getNodeTransform().notifyChanged();
            }
        });

         //we don't want the camera to have any roll and a max look up/down of 40°
        camera.getUpdaters().add(new Updater() {
            @Override
            public void update(DisplayTimerState timing) {

                final Matrix3x3 m = new Matrix3x3(camera.getNodeTransform().getRotation());

                final Vector3f64 forward = new Vector3f64(0,0,1);
                final VectorRW ref = (VectorRW) m.transform(forward);
                double horizontalAngle = forward.shortestAngle(new Vector3f64(ref.getX(),0,ref.getZ()).localNormalize());
                if(ref.getX()<0) horizontalAngle = -horizontalAngle;

                double verticalAngle = ref.shortestAngle(new Vector3f64(ref.getX(),0,ref.getZ()).localNormalize());
                if(ref.getY()<0) verticalAngle = -verticalAngle;
                verticalAngle = Maths.clamp(verticalAngle, Math.toRadians(-40), Math.toRadians(+40));

                //rebuild matrix
                m.setToIdentity();
                if(!Double.isNaN(horizontalAngle)){
                    final Matrix3x3 mh = Matrix3x3.createRotation3(horizontalAngle, camera.getUpAxis());
                    m.localMultiply(mh);
                }
                if(!Double.isNaN(verticalAngle)){
                    final Matrix3x3 mv = Matrix3x3.createRotation3(verticalAngle, camera.getRightAxis());
                    m.localMultiply(mv);
                }
                camera.getNodeTransform().getRotation().set(m);
                camera.getNodeTransform().notifyChanged();
            }
        });
    }

}
