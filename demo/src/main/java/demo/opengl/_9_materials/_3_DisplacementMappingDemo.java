
package demo.opengl._9_materials;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.light.DirectionalLight;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.engine.opengl.technique.DebugFaceNormalTechnique;
import science.unlicense.engine.opengl.tessellation.DisplacementTessellator;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.math.api.Angles;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.engine.opengl.mesh.GLModel;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 * Displacement Mapping
 *
 * @author Johann Sorel
 */
public class _3_DisplacementMappingDemo {

    public static void main(String[] args) throws IOException {

        //create a rendering frame
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        camera.setFarPlane(10000);
        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera));

        //build a small scene
        final DefaultMesh idxGeom = DefaultMesh.createPlan(
                new Vector3f64(-10, 0,-10),
                new Vector3f64(-10, 0, 10),
                new Vector3f64( 10, 0, 10),
                new Vector3f64( 10, 0,-10));
        idxGeom.setUVs(new VBO(new float[]{0,0,1,0,1,1,0,1},2));
        Mesh.calculateTangents(idxGeom);

        final GLModel plan = new GLModel();
        plan.setShape(idxGeom);
        GLEngineUtils.makeCompatible(plan);
        scene.getChildren().add(plan);

        final Image imageDiffuse = Images.read(Paths.resolve(new Chars("mod:/images/bumpDiffuse.png")));
        final UVMapping textDiffuse = new UVMapping(new Texture2D(imageDiffuse));

        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuseTexture(new Layer(textDiffuse));
        plan.getMaterials().add(material);

        final Image imageDisplacement = Images.read(Paths.resolve(new Chars("mod:/images/bumpDisplacement.png")));
        final DisplacementTessellator tessellator = new DisplacementTessellator(new Texture2D(imageDisplacement));
        tessellator.setFactor(1);
        tessellator.setResolution(40);
        plan.setTessellator(tessellator);

        plan.getTechniques().add(new DebugFaceNormalTechnique());

        final DirectionalLight dl = new DirectionalLight();
        scene.getChildren().add(dl);

        //keyboard and mouse control
        final MotionModel crosshair = DefaultMotionModel.createCrosshair3D();
        scene.getChildren().add(crosshair);
        crosshair.getChildren().add(camera);
        final OrbitUpdater controller = new OrbitUpdater(frame,camera,crosshair).configureDefault();
        controller.setDistance(20);
        controller.setHorizontalAngle(Angles.degreeToRadian(30));
        controller.setVerticalAngle(Angles.degreeToRadian(25));
        camera.getUpdaters().add(controller);

        //show the frame
        frame.setSize(640, 480);
        frame.setVisible(true);
    }

}
