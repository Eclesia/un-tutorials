
package demo.opengl._9_materials;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.light.PointLight;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.UVMapping;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.math.api.Angles;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 * Minimal scene demo, displaying an image and basic gesture control.
 *
 * @author Johann Sorel
 */
public class _2_BumpMappingDemo {

    public static void main(String[] args) throws IOException {

        //create a rendering frame
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera));

        //build a small scene
        final DefaultMesh idxGeom = DefaultMesh.createPlan(
                new Vector3f64(-10, -10, 0),
                new Vector3f64(10, -10, 0),
                new Vector3f64(10, 10, 0),
                new Vector3f64(-10, 10, 0));
        final DefaultModel plan = new DefaultModel();
        plan.setShape(idxGeom);
        GLEngineUtils.makeCompatible(plan);
        idxGeom.setUVs(new VBO(new float[]{0,0,1,0,1,1,0,1},2));
        Mesh.calculateTangents(idxGeom);
        scene.getChildren().add(plan);

        final Image imageDiffuse = Images.read(Paths.resolve(new Chars("mod:/images/bumpDiffuse.png")));
        final Image imageNormal = Images.read(Paths.resolve(new Chars("mod:/images/bumpNormal.png")));
        final UVMapping textDiffuse = new UVMapping(new Texture2D(imageDiffuse));
        final UVMapping textNormal = new UVMapping(new Texture2D(imageNormal));

        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuseTexture(new Layer(textDiffuse));
        material.setNormalTexture(new Layer(textNormal));
        plan.getMaterials().add(material);

        final PointLight pl = new PointLight();
        pl.getNodeTransform().getTranslation().setXYZ(0, 0, 10);
        pl.getAttenuation().setConstant(1);
        pl.getAttenuation().setLinear(0);
        pl.getAttenuation().setQuadratic(0);
        pl.getNodeTransform().notifyChanged();
        scene.getChildren().add(pl);

        //keyboard and mouse control
        final MotionModel crosshair = DefaultMotionModel.createCrosshair3D();
        scene.getChildren().add(crosshair);
        crosshair.getChildren().add(camera);
        final OrbitUpdater controller = new OrbitUpdater(frame,camera,crosshair).configureDefault();
        controller.setDistance(20);
        controller.setHorizontalAngle(Angles.degreeToRadian(30));
        controller.setVerticalAngle(Angles.degreeToRadian(25));
        camera.getUpdaters().add(controller);

        //show the frame
        frame.setSize(640, 480);
        frame.setVisible(true);
    }

}
