
package demo.opengl._9_materials;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.material.Layer;
import science.unlicense.engine.opengl.material.mapping.DistanceFieldMapping;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.gpu.impl.opengl.resource.TextureModel;
import science.unlicense.gpu.impl.opengl.resource.TextureUtils;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.math.api.Angles;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 * Distance field texture demo.
 *
 * @author Johann Sorel
 */
public class _1_DistanceFieldDemo {

    public static void main(String[] args) throws IOException {

        //create a rendering frame
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        camera.setFarPlane(10000);
        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera));

        //build a small scene
        final DefaultModel plan = new DefaultModel();
        plan.setShape(DefaultMesh.createPlan(
                new Vector3f64(-10, 0,-10),
                new Vector3f64(-10, 0, 10),
                new Vector3f64( 10, 0, 10),
                new Vector3f64( 10, 0,-10)));
        GLEngineUtils.makeCompatible(plan);
        ((DefaultMesh) plan.getShape()).setUVs(new VBO(new float[]{0,0,1,0,1,1,0,1},2));
        scene.getChildren().add(plan);
        final Image imageDiffuse = Images.read(Paths.resolve(new Chars("file:/home/eclesia/dist.png")));
        final TextureModel model = TextureUtils.toTextureInfo(imageDiffuse, false);
        model.setFormat(GLC.Texture.Format.RED);
        model.setInternalFormat(GLC.Texture.InternalFormat.R8);
        model.setParameter(GLC.Texture.Parameters.MAG_FILTER.KEY, GLC.Texture.Parameters.MAG_FILTER.NEAREST);
        model.setParameter(GLC.Texture.Parameters.MIN_FILTER.KEY, GLC.Texture.Parameters.MIN_FILTER.NEAREST);

        final Texture2D texture = new Texture2D(imageDiffuse, model);
        final DistanceFieldMapping textDiffuse = new DistanceFieldMapping(texture);

        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuseTexture(new Layer(textDiffuse));
        plan.getMaterials().add(material);

        //keyboard and mouse control
        final MotionModel crosshair = DefaultMotionModel.createCrosshair3D();
        scene.getChildren().add(crosshair);
        crosshair.getChildren().add(camera);
        final OrbitUpdater controller = new OrbitUpdater(frame,camera,crosshair).configureDefault();
        controller.setDistance(20);
        controller.setHorizontalAngle(Angles.degreeToRadian(30));
        controller.setVerticalAngle(Angles.degreeToRadian(25));
        camera.getUpdaters().add(controller);

        //show the frame
        frame.setSize(640, 480);
        frame.setVisible(true);
    }

}
