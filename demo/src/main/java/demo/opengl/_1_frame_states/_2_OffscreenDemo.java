
package demo.opengl._1_frame_states;

import com.jogamp.opengl.GLException;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.DefaultGLProcessContext;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.AbstractFboPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.gpu.api.opengl.GLSource;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.GLUtilities;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.model3d.impl.scene.DefaultModel;

/**
 * This demo shows how to make an offscreen rendering.
 *
 * @author Johann Sorel
 */
public class _2_OffscreenDemo {

    public static void main(String[] args) throws IOException, OperationException {
        int width = 800;
        int height = 600;

        //change this to a proper path
        final Chars outputImage = new Chars("file:/home/eclesia/test.png");

        //OpenGL offscreen drawable.
        final GLSource source = GLUtilities.createOffscreenSource(width, height);
        final GLProcessContext context = new DefaultGLProcessContext(source);

        //prepare the output FBO
        final FBO fbo = new FBO(width, height);
        fbo.addAttachment(null, GLC.FBO.Attachment.COLOR_0, new Texture2D(width, height,Texture2D.COLOR_RGBA_CLAMPED()));
        fbo.addAttachment(null, GLC.FBO.Attachment.DEPTH_STENCIL, new Texture2D(width, height,Texture2D.DEPTH24_STENCIL8()));


        //build the scene
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        final DefaultMesh shell = DefaultMesh.createFromGeometry(new BBox(new double[]{-5,-5,-5},new double[]{5,5,5}));
        final DefaultModel box = new DefaultModel(shell);
        camera.getNodeTransform().getTranslation().setXYZ(0,10,30);
        camera.getNodeTransform().notifyChanged();
        scene.getChildren().add(camera);
        scene.getChildren().add(box);


        //prepare the rendering phases
        //phases are explained in ./_5_effects/_0_phases.java
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera,fbo));
        context.getPhases().add(new AbstractFboPhase() {
            protected void processInternal(GLProcessContext ctx) throws GLException {
                //get the result image
                fbo.getColorTexture().loadOnSystemMemory(ctx.getGL());
                final Image image = fbo.getColorTexture().getImage();
                try {
                    //write it on disk
                    Images.write(image, new Chars("png"), Paths.resolve(outputImage));
                } catch (IOException ex) {
                    Loggers.get().log(ex, Logger.LEVEL_WARNING);
                }
            }
        });

        //render it
        source.render();

    }

}
