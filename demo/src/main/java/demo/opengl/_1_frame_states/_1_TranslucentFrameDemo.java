
package demo.opengl._1_frame_states;

import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.light.PointLight;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.math.api.Angles;
import science.unlicense.model3d.impl.scene.DefaultModel;

/**
 * Demonstrate a transparent frame.
 *
 * @author Johann Sorel
 */
public class _1_TranslucentFrameDemo {

    public static void main(String[] args) throws OperationException {

        //create a rendering frame
        //asking for a non-opaque bachground
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        scene.getChildren().add(camera);
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new ClearPhase(new float[]{0f,0f,0f,0f}));
        context.getPhases().add(new RenderPhase(scene,camera));
        //remove frame border decoration
        frame.setDecoration(null);

        //build a small scene
        //set scene background to transparent
        final DefaultMesh shell = DefaultMesh.createFromGeometry(new BBox(new double[]{-5,-5,-5},new double[]{5,5,5}));
        final DefaultModel box = new DefaultModel(shell);
        scene.getChildren().add(box);

        final PointLight light = new PointLight();
        scene.getChildren().add(light);
        light.getNodeTransform().getTranslation().setXYZ(0, 10, 0);
        light.getNodeTransform().notifyChanged();


        //keyboard and mouse control
        final OrbitUpdater controller = new OrbitUpdater(frame,camera,box).configureDefault();
        controller.setDistance(20);
        controller.setHorizontalAngle(Angles.degreeToRadian(30));
        controller.setVerticalAngle(Angles.degreeToRadian(25));
        camera.getUpdaters().add(controller);

        //show the frame
        frame.setSize(800, 600);
        frame.setVisible(true);
    }

}
