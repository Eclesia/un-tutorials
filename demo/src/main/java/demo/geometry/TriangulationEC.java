
package demo.geometry;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.api.desktop.FrameManagers;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.display.api.font.FontContext;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.visual.ContainerView;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.geometry.impl.triangulate.EarClipping;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.TupleRW;

/**
 * Ear clipping triangulation.
 *
 * @author Johann Sorel
 */
public class TriangulationEC {

    public static void main(String[] args) throws OperationException {

        final FontChoice font = new FontChoice(new Chars[]{new Chars("Tuffy")}, 200, FontChoice.WEIGHT_NONE);
        final PlanarGeometry glyph = FontContext.getResolver(font.getFamilies()[0]).getFont(font).getGlyph('K');

        final EarClipping clipper = new EarClipping();
        final Sequence triangles = clipper.triangulate(glyph);

        final WContainer container = new WContainer();
        container.setView(new ContainerView(container){
            protected void renderSelf(Painter2D painter, BBox innerBBox) {
                super.renderSelf(painter, innerBBox);
                painter.setPaint(new ColorPaint(Color.BLACK));
                painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_ROUND));

                for(int k=0,n=triangles.getSize();k<n;k++){
                    TupleRW[] points = (TupleRW[]) triangles.get(k);
                    painter.stroke(new DefaultLine(points[0], points[1]));
                    painter.stroke(new DefaultLine(points[1], points[2]));
                    painter.stroke(new DefaultLine(points[2], points[0]));
                }
            }
        });

        final UIFrame frame = (UIFrame) FrameManagers.getFrameManager().createFrame(false);
        final WContainer base = frame.getContainer();
        base.setLayout(new BorderLayout());
        base.addChild(container, BorderConstraint.CENTER);
        frame.setSize(640,480);
        frame.setVisible(true);

    }

}