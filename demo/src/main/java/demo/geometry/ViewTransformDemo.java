
package demo.geometry;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import javax.swing.JFrame;
import javax.swing.JPanel;
import science.unlicense.geometry.impl.transform.ViewTransform;

public class ViewTransformDemo {

    public static void main(String[] args) throws Exception {

        final ViewTransform view = new ViewTransform();
        view.getSourceToTarget().set(0, 0, 2);
        view.getSourceToTarget().set(1, 1, 2);

        JFrame frame = new JFrame();

        final JPanel panel = new JPanel(){
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);

                final Rectangle rect = new Rectangle(100, 100, 100, 100);
                g.setColor(Color.red);
                Graphics2D g2d = (Graphics2D) g;
                g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

                AffineTransform trs = new AffineTransform(
                view.getSourceToTarget().get(0, 0),
                view.getSourceToTarget().get(1, 0),
                view.getSourceToTarget().get(0, 1),
                view.getSourceToTarget().get(1, 1),
                view.getSourceToTarget().get(0, 2),
                view.getSourceToTarget().get(1, 2));

                g2d.setTransform(trs);
                g2d.setStroke(new BasicStroke(1));
                g2d.draw(rect);

            }

        };

        final boolean[] pressed = new boolean[]{false};
        panel.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                pressed[0] = true;
            }

            @Override
            public void keyReleased(KeyEvent e) {
                pressed[0] = false;
            }
        });

        panel.addMouseWheelListener(new MouseWheelListener() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                if(pressed[0]) {
                    view.rotateAt(e.getWheelRotation()*0.1, new double[]{e.getX(),e.getY()});
                } else {
                    view.scaleAt(1+e.getWheelRotation()*-0.1, new double[]{e.getX(),e.getY()});
                }
                panel.repaint();
            }
        });

        final double[] mvCoord = new double[]{Double.NaN, Double.NaN};
        panel.addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
                final double[] diff = new double[]{
                    e.getX()-mvCoord[0],
                    e.getY()-mvCoord[1]
                };

                view.translate(diff);
                mvCoord[0] = e.getX();
                mvCoord[1] = e.getY();
                panel.repaint();
            }

            @Override
            public void mouseMoved(MouseEvent e) {

            }
        });
        panel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {
                mvCoord[0] = e.getX();
                mvCoord[1] = e.getY();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                mvCoord[0] = Double.NaN;
                mvCoord[1] = Double.NaN;
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        panel.setFocusable(true);
        panel.requestFocus();

        frame.setContentPane(panel);
        frame.setSize(800, 600);
        frame.setVisible(true);

    }

}