
package demo.geometry;

import science.unlicense.common.api.collection.Sequence;
import science.unlicense.display.api.desktop.FrameManagers;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.visual.ContainerView;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.impl.DefaultPoint;
import science.unlicense.geometry.api.Point;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.geometry.impl.triangulate.Delaunay;
import science.unlicense.image.api.color.Color;

/**
 *
 * @author Xavier Philippeau
 * @author Johann Sorel
 */
public class Triangulation {

    public static void main(String[] args) {

        final Delaunay delaunay = new Delaunay();

        /* insert random points */
        for(int i=0;i<100;i++){
            delaunay.insertPoint( new DefaultPoint(Math.random()*400,Math.random()*400) );
        }

        /* get edges segments */
        final Sequence edges = delaunay.computeEdges();

        /* get triangles */
        final Sequence triangles = delaunay.computeTriangles();

        /* get voronoi cells */
        final Sequence voronoi = delaunay.computeVoronoi();

        final WContainer container = new WContainer();
        container.setView(new ContainerView(container){
            protected void renderSelf(Painter2D painter, BBox innerBBox) {
                super.renderSelf(painter, innerBBox);
                painter.setPaint(new ColorPaint(Color.GRAY_NORMAL));
                painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_ROUND));

                for(int k=0,n=edges.getSize();k<n;k++){
                    Point[] points = (Point[]) edges.get(k);
                    for(int i=0;i<points.length-1;i++){
                        final PlanarGeometry geom = new DefaultLine(points[i].getCoordinate(), points[i+1].getCoordinate());
                        painter.stroke(geom);
                    }
                }
            }
        });

        final UIFrame frame = (UIFrame) FrameManagers.getFrameManager().createFrame(false);
        final WContainer base = frame.getContainer();
        base.setLayout(new BorderLayout());
        base.addChild(container, BorderConstraint.CENTER);
        frame.setSize(640,480);
        frame.setVisible(true);

    }

}