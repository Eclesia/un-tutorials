
package demo.program;

import science.unlicense.code.so.pe.PEReader;
import science.unlicense.code.so.pe.PortableExecutable;
import science.unlicense.code.so.pe.SectionHeader;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.engine.ui.component.path.WPathChooser;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;

/**
 * Demonstrate how to read a Windows Portable Executable file.
 *
 * @author Johann Sorel
 */
public class WindowsPEReadingDemo {

    public static void main(String[] args) throws IOException {

        final Path path = WPathChooser.showOpenDialog((Predicate)null);

        final PEReader reader = new PEReader();
        reader.setInput(path);

        final PortableExecutable exec = reader.read();

        //list all available sections
        for(SectionHeader sc : exec.sectionHeaders){
            System.out.println(sc.Name + " "+sc.SizeOfRawData);
        }


    }

}
