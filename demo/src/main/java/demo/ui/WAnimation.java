
package demo.ui;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.anim.DefaultTimer;
import science.unlicense.display.api.desktop.FrameManagers;
import science.unlicense.display.api.layout.AbsoluteLayout;
import science.unlicense.display.impl.anim.TransformAnimation;
import science.unlicense.display.impl.anim.TransformTimeSerieHelper;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.math.api.Angles;

/**
 *
 * @author Johann Sorel
 */
public class WAnimation {

    public static void main(String[] args) {

        final UIFrame frame = (UIFrame) FrameManagers.getFrameManager().createFrame(false);
        final WContainer container = frame.getContainer();
        container.setLayout(new AbsoluteLayout());

        final Widget space = new WButton(new Chars("Hello you"));
        space.getNodeTransform().setToTranslation(new double[]{0,0});
        space.setInlineStyle(new Chars("background : {fill-paint:colorfill(#FF0000);};"));
        container.getChildren().add(space);

        TransformAnimation anim = new TransformTimeSerieHelper()
                .on(space).repeat(0).timer(new DefaultTimer(20))
                .at(0).translation(-200,-200)
                .at(2000).translation(100, 50).rotation(Angles.degreeToRadian(180))
                .at(4000).translation(-150, 200).scale(10)
                .at(8000).translation(-200, -200)
                .build();
        anim.start();

        frame.setSize(800, 600);
        frame.setVisible(true);
    }


}
