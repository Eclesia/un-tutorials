package demo.ui;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.model.tree.DefaultNamedNode;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.display.api.layout.AbsoluteLayout;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.CircularLayout;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.display.api.layout.GridLayout;
import science.unlicense.display.api.layout.LineLayout;
import science.unlicense.display.api.layout.SplitConstraint;
import science.unlicense.display.api.layout.StackConstraint;
import science.unlicense.display.api.layout.StackLayout;
import science.unlicense.display.api.layout.WeightConstraint;
import science.unlicense.display.api.layout.WeightLayout;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.swing.SwingFrameManager;
import science.unlicense.engine.ui.component.WButtonBar;
import science.unlicense.engine.ui.component.WColorChooser;
import science.unlicense.engine.ui.component.WCrumbBar;
import science.unlicense.engine.ui.component.WDateField;
import science.unlicense.engine.ui.component.path.WPathChooser;
import science.unlicense.engine.ui.component.path.WPathField;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.ievent.ActionMessage;
import science.unlicense.engine.ui.model.Column;
import science.unlicense.engine.ui.model.DefaultColumn;
import science.unlicense.engine.ui.model.DefaultRowModel;
import science.unlicense.engine.ui.model.DefaultTreeColumn;
import science.unlicense.engine.ui.model.NumberSliderModel;
import science.unlicense.engine.ui.model.NumberSpinnerModel;
import science.unlicense.engine.ui.model.ObjectPresenter;
import science.unlicense.engine.ui.model.RowModel;
import science.unlicense.engine.ui.model.TreeRowModel;
import science.unlicense.engine.ui.style.SystemStyle;
import science.unlicense.engine.ui.visual.WidgetView;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WCheckBox;
import science.unlicense.engine.ui.widget.WColorField;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WGraphicGeometry;
import science.unlicense.engine.ui.widget.WGraphicText;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WMatrixField;
import science.unlicense.engine.ui.widget.WPopup;
import science.unlicense.engine.ui.widget.WRangeSlider;
import science.unlicense.engine.ui.widget.WScrollBar;
import science.unlicense.engine.ui.widget.WSelect;
import science.unlicense.engine.ui.widget.WSeparator;
import science.unlicense.engine.ui.widget.WSlider;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.WSpinner;
import science.unlicense.engine.ui.widget.WSplitContainer;
import science.unlicense.engine.ui.widget.WTabContainer;
import science.unlicense.engine.ui.widget.WTable;
import science.unlicense.engine.ui.widget.WTextArea;
import science.unlicense.engine.ui.widget.WTextField;
import science.unlicense.engine.ui.widget.WTreeTable;
import science.unlicense.engine.ui.widget.WTupleField;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.engine.ui.widget.frame.WDesktop;
import science.unlicense.engine.ui.widget.menu.WMenuButton;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.impl.Circle;
import science.unlicense.geometry.impl.CircularString;
import science.unlicense.geometry.impl.Geometries;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.api.Maths;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.Vector3f64;

/**
 * Display a frame of all available widgets in differents states.
 *
 * @author Johann Sorel
 */
public class WidgetShowcase {

//    private static final Image ICON;
//    static {
//        try {
//            ICON = Images.read(Paths.resolve(new Chars("mod:/images/unlicense.png")));
//        } catch (IOException ex) {
//            throw new RuntimeException(ex.getMessage(), ex);
//        }
//    }

    public static void main(String[] args) throws Exception {
        new WidgetShowcase();
    }

    public WidgetShowcase() throws Exception {

        final UIFrame frame = SwingFrameManager.INSTANCE.createFrame(false);
//        final UIFrame frame = (UIFrame) FrameManagers.getFrameManager().createFrame(false);
        final WContainer container = frame.getContainer();


        final WGraphicText lbl = new WGraphicText(new Chars("test"));

//        container.addChild(lbl, BorderConstraint.CENTER);
        fillContainer(container);

        frame.setSize(1024, 960);
        frame.setVisible(true);
    }

    public static WContainer create() throws Exception {
        final WContainer container = new WContainer(new BorderLayout());
        fillContainer(container);
        return container;
    }

    public static void fillContainer(WContainer container) throws Exception{

        container.setLayout(new BorderLayout());

        final WButtonBar toolbar = new WButtonBar();
        container.addChild(toolbar, BorderConstraint.TOP);
        toolbar.addChild(new WLabel(new Chars("Theme    :")), FillConstraint.builder().coord(0, 0).build());
        toolbar.addChild(createThemeButton(SystemStyle.THEME_LIGHT, new Chars(" Light ")), FillConstraint.builder().coord(1, 0).build());
        toolbar.addChild(createThemeButton(SystemStyle.THEME_DARK, new Chars(" Dark ")), FillConstraint.builder().coord(2, 0).build());


        final WTabContainer tabs = new WTabContainer();
        container.addChild(tabs, BorderConstraint.CENTER);

        tabs.addTab(createLabelPane(), new WLabel(new Chars("Classics")));
        tabs.addTab(createMathPane(), new WLabel(new Chars("Maths")));
//        tabs.addTab(new WFontPreviewPane(), new WLabel(new Chars("Fonts")));
        tabs.addTab(createTablePane(), new WLabel(new Chars("Tables")));
        tabs.addTab(createTreeTablePane(), new WLabel(new Chars("TreeTables")));
        //tabs.addTab(createCursorPane(), new WLabel(new Chars("Cursors")));
        //tabs.addTab(createEffectPane(), new WLabel(new Chars("Effects")));
        tabs.addTab(createFramePane(), new WLabel(new Chars("Frames")));
        //tabs.addTab(createPopupPane(), new WLabel(new Chars("Popups")));
        //tabs.addTab(createRibbonPane(), new WLabel(new Chars("Ribbons")));
        //tabs.addTab(createGeometryPane(), new WLabel(new Chars("Geometries")));
        tabs.addTab(createColorPane(), new WLabel(new Chars("Color chooser")));
        tabs.addTab(createPathChooserPane(), new WLabel(new Chars("Path chooser")));
//        tabs.addTab(new WChartsPane(), new WLabel(new Chars("Charts")));
        tabs.addTab(createTabsPane(), new WLabel(new Chars("Tabs")));
        tabs.addTab(createLayoutPane(), new WLabel(new Chars("Layouts")));
        tabs.addTab(new WPainterPane(), new WLabel(new Chars("Painter2D")));
    }

    private static WMenuButton createThemeButton(final Chars path, Chars name){
        final WMenuButton button = new WMenuButton(name,null,new EventListener(){
            @Override
            public void receiveEvent(Event event) {
//                    WStyle style = RSReader.readStyle(Paths.resolve(path));
//                    for(int i=0;i<style.getRules().getSize();i++){
//                        final StyleDocument r = (StyleDocument) style.getRules().get(i);
//                        final StyleDocument doc = SystemStyle.INSTANCE.getRule(r.getName());
//                        if (doc == null) {
//                            SystemStyle.INSTANCE.getRules().add(r);
//                        } else {
//                            WidgetStyles.mergeDoc(doc, r, false);
//                        }
//                    }
//
//                    SystemStyle.INSTANCE.getRules().replaceAll(style.getRules());
                    //SystemStyle.INSTANCE.notifyChanged();
            }
        });
        return button;
    }

    public static WContainer createLabelPane(){
        final FormLayout layout = new FormLayout(6,6);
        layout.setColumnSize(1, 200);
        layout.setColumnSize(2, FormLayout.SIZE_AUTO);

        final WContainer pane = new WContainer(layout);
        pane.setInlineStyle(new Chars("margin:20"));

        int y=0;

        final WLabel header = new WLabel(new Chars("Unlicense-lib description page"));
        //header.getStyle().addRules(new Chars("{Subs : {\nfilter:true\nfont:{\nfamily : font('Mona' 16 'bold')\n}\n}}"));
        pane.addChild(header, FillConstraint.builder().coord(0, y++).fill().span(2, 1).build());

        final WLabel lblName = new WLabel(new Chars("Name : "));
        pane.addChild(lblName,FillConstraint.builder().coord(0, y).build());
        final WLabel valName = new WLabel(new Chars("Unlicense-lib"));
        pane.addChild(valName,FillConstraint.builder().coord(1, y++).build());

        final WLabel lblPurpose = new WLabel(new Chars("Objective : "));
        pane.addChild(lblPurpose,FillConstraint.builder().coord(0, y).build());
        final WTextField valPurpose = new WTextField(new Chars("rewrite it all"));
        pane.addChild(valPurpose,FillConstraint.builder().coord(1, y++).fill().span(2, 1).build());

        final WLabel lblAbstract = new WLabel(new Chars("Abstract : "));
        pane.addChild(lblAbstract,FillConstraint.builder().coord(0, y).build());
        final WTextArea valAbstract = new WTextArea(new Chars("Do things, many things, a lot of things, and why not everything ? that would be fun.\n"
                + "No seriously, really everything, you won't need any other lib once it's done. You will see."));
        pane.addChild(valAbstract,FillConstraint.builder().coord(1, y++).fill().span(2, 1).build());

        final WLabel lblCreation = new WLabel(new Chars("Creation : "));
        pane.addChild(lblCreation,FillConstraint.builder().coord(0, y).build());
        final WDateField valCreation = new WDateField();
        pane.addChild(valCreation,FillConstraint.builder().coord(1, y++).build());

        final WLabel lblColor = new WLabel(new Chars("Color : "));
        pane.addChild(lblColor,FillConstraint.builder().coord(0, y).build());
        final WColorField valColor = new WColorField();
        valColor.setColor(Color.BLUE);
        pane.addChild(valColor,FillConstraint.builder().coord(1, y++).build());

        final WLabel lblFolder = new WLabel(new Chars("Folder : "));
        pane.addChild(lblFolder,FillConstraint.builder().coord(0, y).build());
        final WPathField valFolder = new WPathField();
        pane.addChild(valFolder,FillConstraint.builder().coord(1, y++).build());

        final WLabel lblType = new WLabel(new Chars("Type : "));
        pane.addChild(lblType,FillConstraint.builder().coord(0, y).build());
        final WSelect valType = new WSelect(new DefaultRowModel(Collections.singletonSequence("Library")));
        pane.addChild(valType,FillConstraint.builder().coord(1, y++).build());

        final WLabel lblActive = new WLabel(new Chars("Active : "));
        pane.addChild(lblActive,FillConstraint.builder().coord(0, y).build());
        final WCheckBox valActive = new WCheckBox();
        valActive.setCheck(true);
        pane.addChild(valActive,FillConstraint.builder().coord(1, y++).build());

//        final WLabel lblProgress = new WLabel(new Chars("State : "));
//        pane.addChild(lblProgress,FillConstraint.builder().coord(0, y).build());
//        final WLoader valLoader = new WLoader(new Chars("Work in progress"));
//        valLoader.setGraphicPlacement(WLoader.GRAPHIC_LEFT);
//        pane.addChild(valLoader,FillConstraint.builder().coord(1, y++).build());

        final WLabel lblState = new WLabel(new Chars("Progress state : "));
        pane.addChild(lblState,FillConstraint.builder().coord(0, y).build());
        final WSlider valState = new WSlider(new NumberSliderModel(Double.class, 0, 100, 40));
        pane.addChild(valState,FillConstraint.builder().coord(1, y++).build());

        final WLabel lblStd = new WLabel(new Chars("Standard deviations : "));
        pane.addChild(lblStd,FillConstraint.builder().coord(0, y).build());
        final WRangeSlider valStd = new WRangeSlider(new NumberSliderModel(Float.class, 0, 100, 0));
        valStd.setInnerRange(true);
        valStd.setValueMin(30f);
        valStd.setValueMax(80f);
        pane.addChild(valStd,FillConstraint.builder().coord(1, y++).build());

        final WLabel lblHScroll = new WLabel(new Chars("Scrolls left and right : "));
        pane.addChild(lblHScroll,FillConstraint.builder().coord(0, y).build());
        final WScrollBar valHScroll = new WScrollBar();
        valHScroll.setHorizontal(true);
        valHScroll.setHandleVisible(true);
        valHScroll.setOverrideExtents(new Extents(500, 15));
        pane.addChild(valHScroll,FillConstraint.builder().coord(1, y++).fill().span(2, 1).build());

        final WLabel lblVScroll = new WLabel(new Chars("Even up and down : "));
        pane.addChild(lblVScroll,FillConstraint.builder().coord(0, y).build());
        final WScrollBar valVScroll = new WScrollBar();
        valVScroll.setHorizontal(false);
        valVScroll.setHandleVisible(true);
        valVScroll.setOverrideExtents(new Extents(15, 80));
        pane.addChild(valVScroll,FillConstraint.builder().coord(1, y++).build());

        final WLabel lblPhilosophy = new WLabel(new Chars("Philosophy : "));
        pane.addChild(lblPhilosophy,FillConstraint.builder().coord(0, y).build());

        final WCrumbBar bar = new WCrumbBar();
        final WCrumbBar.WCrumb c0 = new WCrumbBar.WCrumb();
        c0.addChild(new WLabel(new Chars("EVERYONE ")),BorderConstraint.CENTER);
        final WCrumbBar.WCrumb c1 = new WCrumbBar.WCrumb();
        c1.addChild(new WLabel(new Chars("LIKES ")),BorderConstraint.CENTER);
        final WCrumbBar.WCrumb c2 = new WCrumbBar.WCrumb();
        c2.addChild(new WLabel(new Chars("FRENCH ")),BorderConstraint.CENTER);
        final WCrumbBar.WCrumb c3 = new WCrumbBar.WCrumb();
        c3.addChild(new WLabel(new Chars("CHEESE !")),BorderConstraint.CENTER);

        bar.addChild(c0, FillConstraint.builder().coord(0, 0).build());
        bar.addChild(c1, FillConstraint.builder().coord(1, 0).build());
        bar.addChild(c2, FillConstraint.builder().coord(2, 0).build());
        bar.addChild(c3, FillConstraint.builder().coord(3, 0).build());

        pane.addChild(bar,FillConstraint.builder().coord(1, y++).fill().span(2, 1).build());

        final WLabel lblTags = new WLabel(new Chars("Tags : "));
        pane.addChild(lblTags,FillConstraint.builder().coord(0, y).build());

        final GridLayout grid = new GridLayout(1, 3);
        final WContainer paneTags = new WContainer(grid);
        pane.addChild(paneTags, FillConstraint.builder().coord(1, y++).fill().span(2, 1).build());

        final Chars uniconFontStyle = new Chars("{\nsubs : {\nfilter:true\nfont:{\nfamily : font('Unicon' 24 'none')\n}\n}\n}");
        final WGraphicText clickableIcon = new WGraphicText(new Chars("\ue012"));
        //clickableIcon.getStyle().addRules(uniconFontStyle);
        final WLabel clickLbl = new WLabel(new Chars("Clickable"),clickableIcon);
        paneTags.getChildren().add(clickLbl);
        final WGraphicText touchableIcon = new WGraphicText(new Chars("\ue01A"));
        //touchableIcon.getStyle().addRules(uniconFontStyle);
        final WLabel touchLbl = new WLabel(new Chars("Touchable"),touchableIcon);
        paneTags.getChildren().add(touchLbl);
        final WGraphicText manyMoreIcon = new WGraphicText(new Chars("\ue001"));
        //manyMoreIcon.getStyle().addRules(uniconFontStyle);
        final WLabel manyMoreLbl = new WLabel(new Chars("And many more"),manyMoreIcon);
        paneTags.getChildren().add(manyMoreLbl);

        final WLabel lblOffer = new WLabel(new Chars("Time unlimited offer : "));
        pane.addChild(lblOffer,FillConstraint.builder().coord(0, y).build());

        final WButton butBoom = new WButton(new Chars("FOR FREE"));
//        butBoom.getStyle().addRules(new Chars("{subs : {"+
//                "filter:true\n"
//                + "margin:50\n"
//                + "font :{\n"
//                    + "family : font('Mona' 20 'bold')\n"
//                    + "fill-paint : colorfill(rgb(255 255 255))\n"
//                + "}\n"
//                + "graphic.0 :{\n"
//                    + "zorder       : -2\n"
//                    + "margin       : 42\n"
//                    + "brush        : plainbrush(1 'round')\n"
//                    + "brush-paint  : colorfill(rgb(0 0 0))\n"
//                    + "fill-paint   : colorfill(rgb(0 0 0))\n"
//                    + "transform    : affineTransform(200 0 0 100 -95 -45)\n"
//                    + "shape:\"'POLYGON((0.07421875 0.453125,0.00390625 0.3828125 ,0.13671875 0.37890625 ,0.01953125 0.23828125 ,0.1484375 0.265625 ,0.109375 0.01953125 ,0.234375 0.1875 ,0.33203125 0.03125 ,0.359375 0.16015625 ,0.4609375 0.01171875 ,0.5546875 0.16015625 ,0.71875 0.0078125 ,0.71875 0.15625 ,0.91015625 0.03125 ,0.85546875 0.2109375 ,0.98828125 0.22265625 ,0.8828125 0.4375 ,0.98046875 0.5390625 ,0.890625 0.65625 ,0.9921875 0.796875 ,0.87109375 0.78125 ,0.92578125 0.97265625 ,0.77734375 0.875 ,0.734375 0.98046875 ,0.65625 0.87890625 ,0.5546875 1 ,0.421875 0.8671875 ,0.33984375 0.984375 ,0.2109375 0.87109375 ,0.08203125 0.98828125 ,0.10546875 0.84375 ,0.015625 0.8359375 ,0.140625 0.7421875 ,0.01953125 0.63671875 ,0.1328125 0.53125 ,0.07421875 0.453125))'\"\n"
//                + "}\n"
//                + "graphic.1 :{\n"
//                    + "zorder       : -1\n"
//                    + "margin       : 40\n"
//                    + "brush        : plainbrush(2 'round')\n"
//                    + "brush-paint  : colorfill(rgb(255 255 255))\n"
//                    + "transform    : affineTransform(200 0 0 100 -100 -50)\n"
//                    + "fill-paint   : lineargradientfill('%' 0 0 1 0 0 rgb(255 0 0) 1 rgb(100 40 40))\n"
//                    + "shape:\"'POLYGON((0.07421875 0.453125,0.00390625 0.3828125 ,0.13671875 0.37890625 ,0.01953125 0.23828125 ,0.1484375 0.265625 ,0.109375 0.01953125 ,0.234375 0.1875 ,0.33203125 0.03125 ,0.359375 0.16015625 ,0.4609375 0.01171875 ,0.5546875 0.16015625 ,0.71875 0.0078125 ,0.71875 0.15625 ,0.91015625 0.03125 ,0.85546875 0.2109375 ,0.98828125 0.22265625 ,0.8828125 0.4375 ,0.98046875 0.5390625 ,0.890625 0.65625 ,0.9921875 0.796875 ,0.87109375 0.78125 ,0.92578125 0.97265625 ,0.77734375 0.875 ,0.734375 0.98046875 ,0.65625 0.87890625 ,0.5546875 1 ,0.421875 0.8671875 ,0.33984375 0.984375 ,0.2109375 0.87109375 ,0.08203125 0.98828125 ,0.10546875 0.84375 ,0.015625 0.8359375 ,0.140625 0.7421875 ,0.01953125 0.63671875 ,0.1328125 0.53125 ,0.07421875 0.453125))'\"\n"
//                + "}}}"
//        ));
        pane.addChild(butBoom, FillConstraint.builder().coord(1, y++).fill().span(2, 1).build());


        final WLabel lblActions = new WLabel(new Chars("Actions : "));
        pane.addChild(lblActions,FillConstraint.builder().coord(0, y).build());

        final GridLayout grid2 = new GridLayout(1,3,10,0);
        final WContainer paneActions = new WContainer(grid2);
        pane.addChild(paneActions, FillConstraint.builder().coord(1, y++).fill().span(2, 1).build());

        final WButton like = new WButton(new Chars("I Love it"));
        like.setInlineStyle(new Chars("color-main:rgb(220 60 80)"));
        final WButton dislike = new WButton(new Chars("I don't like it"));
        final WButton send = new WButton(new Chars("Send to a friend"));
        final WGraphicText sendIcon = new WGraphicText(new Chars("\ue010"));
        //sendIcon.getStyle().addRules(uniconFontStyle);
        send.setGraphic(sendIcon);

        dislike.setEnable(false);
        paneActions.getChildren().add(like);
        paneActions.getChildren().add(dislike);
        paneActions.getChildren().add(send);

        return pane;
    }


    public static WContainer createMathPane(){
        final FormLayout layout = new FormLayout(6,6);
        layout.setColumnSize(1, 200);
        layout.setColumnSize(2, FormLayout.SIZE_AUTO);

        final WContainer pane = new WContainer(layout);
        pane.setInlineStyle(new Chars("margin:20"));

        int y=0;

        final WLabel header = new WLabel(new Chars("Unlicense-lib description page"));
        //header.getStyle().addRules(new Chars("{Subs : {\nfilter:true\nfont:{\nfamily : font('Mona' 16 'bold')\n}\n}}"));
        pane.addChild(header, FillConstraint.builder().coord(0, y++).fill().span(2, 1).build());

        final WLabel lblNumber = new WLabel(new Chars("Number : "));
        pane.addChild(lblNumber,FillConstraint.builder().coord(0, y).build());
        final WSpinner valNumber = new WSpinner(new NumberSpinnerModel(Double.class, 3.14, 0, 100, 1));
        pane.addChild(valNumber,FillConstraint.builder().coord(1, y++).build());

        final WLabel lblTuple = new WLabel(new Chars("Tuple : "));
        pane.addChild(lblTuple,FillConstraint.builder().coord(0, y).build());
        final WTupleField valTuple = new WTupleField();
        valTuple.setTuple(new Vector3f64(5, 10, 15));
        pane.addChild(valTuple,FillConstraint.builder().coord(1, y++).build());

        final WLabel lblMatrix = new WLabel(new Chars("Matrix : "));
        pane.addChild(lblMatrix,FillConstraint.builder().coord(0, y).build());
        final WMatrixField valMatrix = new WMatrixField();
        valMatrix.setMatrix(new Matrix4x4(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16));
        pane.addChild(valMatrix,FillConstraint.builder().coord(1, y++).build());




        return pane;
    }


    private static WContainer createPane(String title){
        final WContainer pane = new WContainer(new FormLayout());
        ((FormLayout)pane.getLayout()).setDefaultColumnSpace(10);
        ((FormLayout)pane.getLayout()).setDefaultRowSpace(10);
        ((FormLayout)pane.getLayout()).setColumnSize(10, FormLayout.SIZE_EXPAND);

        WLabel lbl = new WLabel(new Chars(title));
        lbl.setInlineStyle(new Chars(
                "margin:4\n"+
                "graphic:{\n radius:6\n brush:plainbrush(0 'round')\n fill-paint:colorfill(@color-main)\n margin:3\n }"));

        pane.addChild(lbl, FillConstraint.builder().coord(0, 0).build());
        pane.addChild(new WSeparator(), FillConstraint.builder().coord(1, 0).fill(true, true).span(10, 1).build());
        return pane;
    }

    private static WContainer createTablePane(){
        final WContainer paneTable = new WContainer();
        paneTable.setLayout(new BorderLayout());

        final Sequence datas = new ArraySequence();
        for(int i=0;i<100;i++){
            datas.add(new Object[]{new Chars("data "+i),Math.random(),Math.random()});
        }

        final NumberFormat nf = new DecimalFormat("0.##");
        final ObjectPresenter p = new ObjectPresenter() {
            public Widget createWidget(Object candidate) {
                WLabel lbl = new WLabel();
                if(candidate instanceof Number){
                    lbl.setText(new Chars(nf.format((Double)candidate)));
                    lbl.setHorizontalAlignment(WLabel.HALIGN_RIGHT);
                }else{
                    lbl.setText(CObjects.toChars(candidate));
                }
                return lbl;
            }
        };

        final RowModel rowModel = new DefaultRowModel(datas);
        final WTable table = new WTable();

        final Column col1 = new DefaultColumn(new Chars("a column with text value"),null){
            public Object getCellValue(Object candidate) {
                return ((Object[])candidate)[0];
            }
            public ObjectPresenter getPresenter() {
                return p;
            }
        };
        final Column col2 = new DefaultColumn(new Chars("numeric values"),null){
            public Object getCellValue(Object candidate) {
                return ((Object[])candidate)[1];
            }
            public ObjectPresenter getPresenter() {
                return p;
            }
        };
        table.getColumns().add(col1);
        table.getColumns().add(col2);

        for(int i=0;i<20;i++){
            final Column colN = new DefaultColumn(new Chars("numeric "+i),null){
                public Object getCellValue(Object candidate) {
                    return ((Object[])candidate)[2];
                }
                public ObjectPresenter getPresenter() {
                    return p;
                }
            };
            table.getColumns().add(colN);
        }
        table.setRowModel(rowModel);


        paneTable.addChild(table, BorderConstraint.CENTER);

        return paneTable;
    }

    private static WContainer createTreeTablePane(){
        final WContainer paneTreeTable = new WContainer(new BorderLayout());

        final WTreeTable treetable = createTreeTable();
        paneTreeTable.addChild(treetable, BorderConstraint.CENTER);
        return paneTreeTable;
    }

    private static WTreeTable createTreeTable(){
        DefaultNamedNode root = new DefaultNamedNode(true);
        root.setName(new Chars("Root"));
        root.setValue(new Object[]{null, ""});
        for(int i=0;i<10;i++){
            DefaultNamedNode child1 = new DefaultNamedNode(true);
            child1.setName(new Chars("Child "+i));
            child1.setValue(new Object[]{null, "bla bla bla ..."});
            for(int k=0;k<4;k++){
                DefaultNamedNode child2 = new DefaultNamedNode(false);
                child2.setName(new Chars("Child "+i+" "+k));
                child2.setValue(new Object[]{new ColorRGB(
                        (float)Math.random(), (float)Math.random(), (float)Math.random()), "bla bla bla ..."});
                child1.getChildren().add(child2);
            }
            root.getChildren().add(child1);
        }


        final TreeRowModel treeModel = new TreeRowModel(root);
        treeModel.setFold(root, true);
        for(int i=0;i<10;i++){
            if(Math.random()>0.5){
                treeModel.setFold((Node) root.getChildren().get(i),true);
            }
        }
        final Column treeCol = new DefaultTreeColumn(null, new WLabel(new Chars(" tree ")), null);
        treeCol.setBestWidth(250);

        final ObjectPresenter p = new ObjectPresenter() {
            public Widget createWidget(Object candidate) {
                WLabel lbl = new WLabel();
                if(candidate instanceof Color){
                    int r = (int) (((Color)candidate).getRed() * 255);
                    int g = (int) (((Color)candidate).getGreen() * 255);
                    int b = (int) (((Color)candidate).getBlue() * 255);
                    lbl.setInlineStyle(new Chars("background:{fill-paint:rgb("+r+" "+g+" "+b+")\n}"));
                }
                return lbl;
            }
        };
        final DefaultColumn column1 = new DefaultColumn(new Chars("color"),null) {
            public Object getCellValue(Object candidate) {
                DefaultNamedNode node = (DefaultNamedNode) candidate;
                return ((Object[])node.getValue())[0];
            }
            public ObjectPresenter getPresenter() {
                return p;
            }
        };
        column1.setBestWidth(80);
        final Column column2 = new DefaultColumn(new Chars("description"),null) {
            public Object getCellValue(Object candidate) {
                DefaultNamedNode node = (DefaultNamedNode) candidate;
                return ((Object[])node.getValue())[1];
            }
        };
        column2.setBestWidth(80);

        final WTreeTable treetable = new WTreeTable();
        treetable.setRowModel(treeModel);
        treetable.getColumns().add(treeCol);
        treetable.getColumns().add(column2);
        treetable.getColumns().add(column1);

        return treetable;
    }

//    private static WContainer createCursorPane(){
//        final WContainer pane = new WContainer(new FormLayout());
//        ((FormLayout)pane.getLayout()).setDefaultColumnSize(FormLayout.SIZE_EXPAND);
//        ((FormLayout)pane.getLayout()).setColumnSize(0, FormLayout.SIZE_EXPAND);
//        ((FormLayout)pane.getLayout()).setDefaultColumnSpace(0);
//        ((FormLayout)pane.getLayout()).setDefaultRowSpace(0);
//
//        int y=0;
//
//        // LABELS //////////////////////////////////////////////////////////////
//        final WContainer paneLabel = createPane("CURSORS");
//
//        final WLabel lblEnable = new WLabel(new Chars("label with a mouse over cursor"));
//        lblEnable.setEnable(true);
//        lblEnable.setCursor(new ImageCursor(ICON,new Vector2f64(12, 12)));
//
//        paneLabel.addChild(lblEnable, FillConstraint.builder().coord(0, 1).build());
//        pane.addChild(paneLabel, FillConstraint.builder().coord(0, y++).build());
//
//        return pane;
//    }

    private static WContainer createEffectPane(){
        final WContainer pane = new WContainer(new BorderLayout());
        pane.setInlineStyle(new Chars("effect:'blur';"));

        final WButton button = new WButton(new Chars("Blurred button"));
        pane.addChild(button,BorderConstraint.CENTER);

        return pane;
    }

    private static WContainer createFramePane(){
        final WDesktop paneFrame = new WDesktop();

        WLabel lbl1 = new WLabel(new Chars("hello world"));
        UIFrame frame1 = paneFrame.addFrame();
        frame1.setSize(250, 150);
        frame1.setTitle(new Chars("Frame 1"));
        frame1.getContainer().setLayout(new BorderLayout());
        frame1.getContainer().addChild(lbl1, BorderConstraint.CENTER);

        UIFrame frame2 = paneFrame.addFrame();
        frame2.setSize(350, 400);
        frame2.setClosable(false);
        frame2.setMaximizable(true);
        frame2.setMinimizable(false);
        frame2.setOnScreenLocation(new Vector2f64(-100, -100));
        frame2.setTitle(new Chars("Frame 2"));
        frame2.getContainer().setLayout(new BorderLayout());
        frame2.getContainer().addChild(createTreeTable(), BorderConstraint.CENTER);


        return paneFrame;
    }

    private static WContainer createPopupPane(){
        final WContainer panePopup = new WContainer(new FormLayout());

        final WPopup popup = new WPopup();
        popup.setLayout(new BorderLayout());

        final WLabel lbl = new WLabel(new Chars("Hello Popup"));
        popup.addChild(lbl, BorderConstraint.CENTER);

        final WButton button = new WButton(new Chars("Click me to see popup"));

        button.addEventListener(ActionMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                popup.showAt(button, 0, 0);
            }
        });

        panePopup.addChild(button, FillConstraint.builder().coord(0, 0).fill(true, true).build());

        return panePopup;
    }

    private static WContainer createRibbonPane(){
        final WContainer paneRibbon = new WContainer();
        return paneRibbon;
    }

    private static WContainer createGeometryPane(){
        final WContainer paneGeometry = new WContainer(new AbsoluteLayout());
        final WGraphicGeometry geomCircle = new WGraphicGeometry();
        geomCircle.setGeometry(new Circle(30, 30, 1));
        geomCircle.getNodeTransform().setToTranslation(new double[]{0,0});
        geomCircle.setOverrideExtents(new Extents(200, 200));
        paneGeometry.getChildren().add(geomCircle);

        final WGraphicGeometry geomCircularCurve = new WGraphicGeometry();
        geomCircularCurve.setGeometry(new CircularString(Geometries.toTupleBuffer(new Tuple[]{
            new Vector2f64(50, 50),
            new Vector2f64(25, 25),
            new Vector2f64(50, 0),
            new Vector2f64(75, 25),
            new Vector2f64(50, 50)})));
        geomCircularCurve.getNodeTransform().setToTranslation(new double[]{100,100});
        geomCircularCurve.setOverrideExtents(new Extents(200, 200));
        paneGeometry.getChildren().add(geomCircularCurve);

        final WGraphicText text = new WGraphicText(new Chars("Hello world ! ! !"));
        text.getNodeTransform().setToTranslation(new double[]{-100,-100});
        text.setOverrideExtents(new Extents(100,100));
        paneGeometry.getChildren().add(text);

        return paneGeometry;
    }

    private static WContainer createColorPane(){

        final WContainer paneColor = new WContainer(new StackLayout());

        final WColorChooser wcc = new WColorChooser();
        paneColor.addChild(wcc, new StackConstraint(1));
        return paneColor;
    }

    private static WContainer createPathChooserPane(){
        final WContainer paneColor = new WContainer(new BorderLayout());
        WPathChooser chooser = new WPathChooser();
        paneColor.addChild(chooser, BorderConstraint.CENTER);
        return paneColor;
    }

    private static WContainer createTabsPane(){
        final WContainer paneTabs = new WContainer();
        paneTabs.setLayout(new GridLayout(2, 2,5,5));

        paneTabs.getChildren().add(createTabPane(WTabContainer.TAB_POSITION_TOP));
        paneTabs.getChildren().add(createTabPane(WTabContainer.TAB_POSITION_BOTTOM));
        paneTabs.getChildren().add(createTabPane(WTabContainer.TAB_POSITION_LEFT));
        paneTabs.getChildren().add(createTabPane(WTabContainer.TAB_POSITION_RIGHT));

        return paneTabs;
    }

    private static WTabContainer createTabPane(int tabPosition){
        final WTabContainer tc = new WTabContainer();
        tc.setTabPosition(tabPosition);
        for(int i=0;i<4;i++){
            final WLabel content = new WLabel(new Chars("tab content "+i));
            final WContainer header = new WContainer(new FormLayout(10,10));
            header.setInlineStyle(new Chars("background:none"));
            final WLabel text = new WLabel(new Chars("tab."+i));
            final WButton action2 = new WButton(new Chars("X"));
            header.addChild(text, FillConstraint.builder().coord(0, 0).fill().build());
            header.addChild(action2, FillConstraint.builder().coord(2, 0).fill().build());

            tc.addTab(content, header);
        }
        return tc;
    }

    private static WContainer createLayoutPane() throws IOException{
        final WTabContainer paneLayouts = new WTabContainer();
        paneLayouts.setTabPosition(WTabContainer.TAB_POSITION_LEFT);

        final WContainer borderPane = new WContainer(new BorderLayout());
        borderPane.addChild(new WColor(new Extent.Double(100, 100), Color.RED), BorderConstraint.TOP);
        borderPane.addChild(new WColor(new Extent.Double(100, 100), Color.GREEN), BorderConstraint.BOTTOM);
        borderPane.addChild(new WColor(new Extent.Double(100, 100), Color.BLUE), BorderConstraint.CENTER);
        borderPane.addChild(new WColor(new Extent.Double(100, 100), Color.GRAY_NORMAL), BorderConstraint.LEFT);
        borderPane.addChild(new WColor(new Extent.Double(100, 100), Color.GRAY_LIGHT), BorderConstraint.RIGHT);
        paneLayouts.addTab(borderPane, new WLabel(new Chars("Border")));

        final WContainer gridPane = new WContainer(new GridLayout(3,3));
        gridPane.getChildren().add(new WColor(new Extent.Double(1, 1), Color.RED));
        gridPane.getChildren().add(new WColor(new Extent.Double(1, 1), Color.GRAY_NORMAL));
        gridPane.getChildren().add(new WColor(new Extent.Double(1, 1), Color.GREEN));
        gridPane.getChildren().add(new WColor(new Extent.Double(1, 1), Color.TRS_WHITE));
        gridPane.getChildren().add(new WColor(new Extent.Double(1, 1), Color.GRAY_LIGHT));
        gridPane.getChildren().add(new WColor(new Extent.Double(1, 1), Color.GRAY_DARK));
        gridPane.getChildren().add(new WColor(new Extent.Double(1, 1), Color.BLACK));
        gridPane.getChildren().add(new WColor(new Extent.Double(1, 1), Color.BLUE));
        gridPane.getChildren().add(new WColor(new Extent.Double(1, 1), Color.WHITE));
        paneLayouts.addTab(gridPane, new WLabel(new Chars("Grid")));

        final WContainer linePane = new WContainer(new LineLayout());
        for(int i=0;i<100;i++){
            double width = Maths.random(60, 200);
            double height = Maths.random(30, 60);
            linePane.getChildren().add(new WColor(new Extent.Double(width, height),
                    new ColorRGB((int)Maths.random(0, 255),
                              (int)Maths.random(0, 255),
                              (int)Maths.random(0, 255))));
        }
        paneLayouts.addTab(linePane, new WLabel(new Chars("Lines")));


        final WContainer circularPane = new WContainer(new CircularLayout());
        circularPane.addChild(new WColor(new Extent.Double(120, 40), Color.RED),        null);
        circularPane.addChild(new WColor(new Extent.Double(100, 40), Color.GRAY_DARK),  null);
        circularPane.addChild(new WColor(new Extent.Double(80, 80), Color.GRAY_LIGHT), null);
        circularPane.addChild(new WColor(new Extent.Double(80, 40), Color.BLUE),       null);
        circularPane.addChild(new WColor(new Extent.Double(60, 60), Color.BLACK),      null);
        circularPane.addChild(new WColor(new Extent.Double(60, 60), Color.GREEN),      null);
        circularPane.addChild(new WColor(new Extent.Double(20, 20), Color.GREEN),      null);
        circularPane.addChild(new WColor(new Extent.Double(120, 40), Color.RED),        null);
        circularPane.addChild(new WColor(new Extent.Double(120, 60), Color.GRAY_DARK),  null);
        circularPane.addChild(new WColor(new Extent.Double(80, 30), Color.GRAY_LIGHT), null);
        circularPane.addChild(new WColor(new Extent.Double(40, 100), Color.RED),        null);
        circularPane.addChild(new WColor(new Extent.Double(20, 60), Color.GRAY_LIGHT), null);
        paneLayouts.addTab(circularPane, new WLabel(new Chars("Circular")));

        final WContainer formPane = new WContainer();
        final FormLayout formLayout = new FormLayout();
        formLayout.setColumnSize(3, FormLayout.SIZE_EXPAND);
        formLayout.setRowSize(4, FormLayout.SIZE_EXPAND);
        formPane.setLayout(formLayout);
        formPane.addChild(new WColor(new Extent.Double(60, 20), Color.RED),        FillConstraint.builder().coord(0, 0).fill().build());
        formPane.addChild(new WColor(new Extent.Double(120, 20), Color.GRAY_DARK), FillConstraint.builder().coord(1, 0).fill().build());
        formPane.addChild(new WColor(new Extent.Double(40, 40), Color.GRAY_LIGHT), FillConstraint.builder().coord(0, 1).fill().span(4, 1).build());
        formPane.addChild(new WColor(new Extent.Double(350, 350), Color.BLUE),     FillConstraint.builder().coord(0, 2).fill().span(2, 2).build());
        formPane.addChild(new WColor(new Extent.Double(30, 30), Color.BLACK),      FillConstraint.builder().coord(2, 2).fill().build());
        formPane.addChild(new WColor(new Extent.Double(30, 30), Color.GREEN),      FillConstraint.builder().coord(2, 3).fill().build());
        formPane.addChild(new WColor(new Extent.Double(10, 10), Color.GREEN),      FillConstraint.builder().coord(1, 4).fill().build());
        formPane.addChild(new WColor(new Extent.Double(60, 20), Color.RED),        FillConstraint.builder().coord(0, 5).fill().build());
        formPane.addChild(new WColor(new Extent.Double(120, 20), Color.GRAY_DARK), FillConstraint.builder().coord(1, 5).fill().build());
        formPane.addChild(new WColor(new Extent.Double(40, 15), Color.GRAY_LIGHT), FillConstraint.builder().coord(0, 6).fill().build());
        formPane.addChild(new WColor(new Extent.Double(100, 20), Color.RED),       FillConstraint.builder().coord(1, 5).fill().build());
        paneLayouts.addTab(formPane, new WLabel(new Chars("Form")));

        final WContainer absolutePane = new WContainer(new AbsoluteLayout());
        absolutePane.setOverrideExtents(new Extents(400, 400));
        absolutePane.setEffectiveExtent(new Extent.Double(400, 400));

        WColor c1 = new WColor(new Extent.Double(20, 60), Color.RED);
        WColor c2 = new WColor(new Extent.Double(80, 80), Color.GREEN);
        WColor c3 = new WColor(new Extent.Double(400, 20), Color.BLUE);
        c1.getNodeTransform().setToTranslation(new double[]{30,10});
        c2.getNodeTransform().setToTranslation(new double[]{210,120});
        c3.getNodeTransform().setToTranslation(new double[]{10,60});

        absolutePane.getChildren().add(c1);
        absolutePane.getChildren().add(c2);
        absolutePane.getChildren().add(c3);
        paneLayouts.addTab(absolutePane, new WLabel(new Chars("Absolute")));


        final WSplitContainer splitPane = new WSplitContainer();
        splitPane.setOverrideExtents(new Extents(400, 400));
        splitPane.setEffectiveExtent(new Extent.Double(400, 400));

        WColor s1 = new WColor(new Extent.Double(20, 60), Color.RED);
        WColor s2 = new WColor(new Extent.Double(80, 80), Color.GREEN);
        splitPane.addChild(s1,SplitConstraint.TOP);
        splitPane.addChild(s2,SplitConstraint.BOTTOM);

        paneLayouts.addTab(splitPane, new WLabel(new Chars("Split")));

        final WContainer weightPane = new WContainer(new WeightLayout());
        weightPane.addChild(new WColor(), new WeightConstraint(30));
        weightPane.addChild(new WColor(), new WeightConstraint(20));
        weightPane.addChild(new WColor(), new WeightConstraint(10));
        weightPane.addChild(new WColor(), new WeightConstraint(8));
        weightPane.addChild(new WColor(), new WeightConstraint(4));
        weightPane.addChild(new WColor(), new WeightConstraint(2));



        paneLayouts.addTab(weightPane, new WLabel(new Chars("Weight")));

        return paneLayouts;
    }

    public static class WColor extends WSpace{

        private final Color color;

        public WColor() {
            this(new Extent.Double(10, 10),new ColorRGB((int)Maths.random(0, 255),
                              (int)Maths.random(0, 255),
                              (int)Maths.random(0, 255)));
        }

        public WColor(Extent ext, Color color) {
            super(ext);
            this.color = color;
            setView(new WidgetView(this){
                protected void renderSelf(Painter2D painter, BBox innerBBox) {
                    super.renderSelf(painter, innerBBox);
                    WColor.this.renderSelf(painter, innerBBox);
                }
            });
        }

        private void renderSelf(Painter2D painter, BBox innerBBox) {
            painter.setPaint(new ColorPaint(color));
            final Rectangle rect = new Rectangle(innerBBox.getMin(0), innerBBox.getMin(1), innerBBox.getSpan(0), innerBBox.getSpan(1));
            painter.fill(rect);
        }

    }

}