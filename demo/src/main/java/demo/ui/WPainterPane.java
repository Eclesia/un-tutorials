

package demo.ui;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.painter2d.LinearGradientPaint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.painter2d.RadialGradientPaint;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.image.api.color.Color;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.tuple.InterleavedTupleGrid1D;
import science.unlicense.geometry.api.Extent;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.display.api.painter2d.SpreadMode;
import science.unlicense.engine.ui.visual.WidgetView;
import science.unlicense.geometry.impl.Polyline;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.geometry.api.tuple.TupleGrid1D;

/**
 * demonstratate direct painting.
 *
 * @author Johann Sorel
 */
public class WPainterPane extends WContainer{

    public WPainterPane() {
        super(new FormLayout());
        int i=0;
        addChild(new WLabel(new Chars("Linear gradient paint")), FillConstraint.builder().coord(0, i++).build());
        addChild(new WLinearGradient1(), FillConstraint.builder().coord(0, i++).build());
        addChild(new WLabel(new Chars("Radial gradient paint")), FillConstraint.builder().coord(0, i++).build());
        addChild(new WRadialGradient1(), FillConstraint.builder().coord(0, i++).build());
        addChild(new WLabel(new Chars("Polylines")), FillConstraint.builder().coord(0, i++).build());
        addChild(new WPolyline(), FillConstraint.builder().coord(0, i++).build());


    }

    private static class WLinearGradient1 extends WSpace{

        public WLinearGradient1() {
            super(new Extent.Double(300, 100));
            setView(new WidgetView(this){
                protected void renderSelf(Painter2D painter, BBox innerBBox) {
                    super.renderSelf(painter, innerBBox);
                    WLinearGradient1.this.renderSelf(painter, innerBBox);
                }
            });
        }

        private void renderSelf(Painter2D painter, BBox innerBBox) {

            painter.setPaint(new LinearGradientPaint(20, 20, 280, 80,
                    new double[]{0,0.5,1},
                    new Color[]{Color.RED,Color.GREEN,Color.BLUE},SpreadMode.PAD));

            painter.fill(new Rectangle(
                    innerBBox.getMin(0), innerBBox.getMin(1),
                    innerBBox.getSpan(0), innerBBox.getSpan(0)));
        }

    }

    private static class WRadialGradient1 extends WSpace{

        public WRadialGradient1() {
            super(new Extent.Double(300, 100));
            setView(new WidgetView(this){
                protected void renderSelf(Painter2D painter, BBox innerBBox) {
                    super.renderSelf(painter, innerBBox);
                    WRadialGradient1.this.renderSelf(painter, innerBBox);
                }
            });
        }

        private void renderSelf(Painter2D painter, BBox innerBBox) {

            painter.setPaint(new RadialGradientPaint(150, 50, 200, 150, 50,
                    new double[]{0,0.5,1},
                    new Color[]{Color.RED,Color.GREEN,Color.BLUE},SpreadMode.PAD));
            painter.fill(new Rectangle(
                    innerBBox.getMin(0), innerBBox.getMin(1),
                    innerBBox.getSpan(0), innerBBox.getSpan(0)));
        }

    }


    private static class WPolyline extends WSpace{

        public WPolyline() {
            super(new Extent.Double(300, 100));
            setView(new WidgetView(this){
                protected void renderSelf(Painter2D painter, BBox innerBBox) {
                    super.renderSelf(painter, innerBBox);
                    WPolyline.this.renderSelf(painter, innerBBox);
                }
            });
        }

        private void renderSelf(Painter2D painter, BBox innerBBox) {

            painter.setPaint(new ColorPaint(Color.BLUE));
            painter.setBrush(new PlainBrush(10,
                    PlainBrush.LINECAP_ROUND,
                    PlainBrush.LINEJOIN_BEVEL,
                    0,0,
                    new float[]{20,20}));

            final TupleGrid1D buffer = InterleavedTupleGrid1D.create(
                    new float[]{
                    -110,40,
                    0,-40,
                    -30,20,
                     30,-20,
                     110,0
                    }, 2);
            final Polyline geom = new Polyline(buffer);

            painter.stroke(geom);

        }

    }
}
