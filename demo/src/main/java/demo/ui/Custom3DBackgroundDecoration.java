
package demo.ui;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.model.Column;
import science.unlicense.engine.ui.model.DefaultColumn;
import science.unlicense.engine.ui.model.DefaultRowModel;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.WTable;
import science.unlicense.engine.ui.widget.frame.WDesktop;
import science.unlicense.engine.ui.widget.frame.WFrameDecoration;
import science.unlicense.engine.ui.widget.frame.WFrameDrag;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.Angles;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 *
 * @author Johann Sorel
 */
public class Custom3DBackgroundDecoration {

    private static final Image ICON;
    static {
        try {
            ICON = Images.read(Paths.resolve(new Chars("mod:/images/unlicense.png")));
        } catch (IOException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    public static void main(String[] args) throws OperationException {

        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        frame.setSize(800, 600);
        buildScene(frame);

        frame.getContainer().setInlineStyle(new Chars("background:{fill-paint:#00000000;};"));

        final WDesktop desktop = new WDesktop();
        desktop.setInlineStyle(new Chars("background:{fill-paint:#FFFFFF44;};"));
        frame.getContainer().setLayout(new BorderLayout());
        frame.getContainer().addChild(desktop,BorderConstraint.CENTER);

        final Sequence datas = new ArraySequence();
        for(int i=0;i<30;i++){
            datas.add(Math.random()*1000);
        }

        final UIFrame subFrame1 = desktop.addFrame();
        subFrame1.setSize(200, 200);
        subFrame1.setOnScreenLocation(new Vector2f64(-300, -50));
        final Column col1 = new DefaultColumn(new Chars("Values"), null);
        col1.setBestWidth(FormLayout.SIZE_EXPAND);
        final WTable table1 = new WTable(new DefaultRowModel(datas), col1);
        subFrame1.getContainer().setLayout(new BorderLayout());
        subFrame1.getContainer().addChild(table1, BorderConstraint.CENTER);


        final UIFrame subFrame2 = desktop.addFrame();
        subFrame2.setSize(200, 200);
        subFrame2.setOnScreenLocation(new Vector2f64(50, -100));
        final Column col2 = new DefaultColumn(new Chars("Values"), null);
        col2.setBestWidth(FormLayout.SIZE_EXPAND);
        final WTable table2 = new WTable(new DefaultRowModel(datas), col2);
        subFrame2.getContainer().setLayout(new BorderLayout());
        subFrame2.getContainer().addChild(table2, BorderConstraint.CENTER);



        frame.setDecoration(new MyDecoration());
        frame.setVisible(true);
    }


    private static void buildScene(GLUIFrame frame) throws OperationException{
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        scene.getChildren().add(camera);
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera));

        //build a small scene
        final DefaultModel box = new DefaultModel();
        final DefaultMesh mesh = DefaultMesh.createFromGeometry(new BBox(new double[]{-5,-5,-5},new double[]{5,5,5}));
        box.setShape(mesh);
        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuse(Color.WHITE);
        box.getMaterials().add(material);
        //((GLTechnique)box.getRenderers().get(0)).getState().setPolygonMode(GLC.POLYGON_MODE.LINE);
        scene.getChildren().add(box);

        //keyboard and mouse control
        final MotionModel crosshair = DefaultMotionModel.createCrosshair3D();
        scene.getChildren().add(crosshair);
        crosshair.getChildren().add(camera);
        final OrbitUpdater controller = new OrbitUpdater(frame,camera,crosshair).configureDefault();
        controller.setDistance(20);
        controller.setHorizontalAngle(Angles.degreeToRadian(30));
        controller.setVerticalAngle(Angles.degreeToRadian(25));
        camera.getUpdaters().add(controller);
    }

    private static class MyDecoration extends WFrameDecoration{

        public MyDecoration() {
            final WContainer top = new WContainer(new FormLayout(10,10));
            final WButton close = new WButton(new Chars("X"),null,new EventListener() {
                public void receiveEvent(Event event) {
                    System.exit(0);
                }
            });

            final WFrameDrag dragTitle = new WFrameDrag(new BorderLayout());
            dragTitle.addChild(new WLabel(new Chars("Some frame title, or anything. you can drag the frame too.")),BorderConstraint.CENTER);
            top.addChild(close, FillConstraint.builder().coord(0, 0).build());
            top.addChild(dragTitle, FillConstraint.builder().coord(1, 0).build());
            top.setInlineStyle(new Chars(
                    "margin : 10;"+
                    "background : none;"+
                    "graphic : {"
                            + "radius : [40,40,40,40];"
                            + "brush : plainbrush(1,'round');"
                            + "brush-paint : $color-main;"
                            + "fill-paint : $color-main;"
                            + "margin : 10;"
                            + "};"));


            final WSpace left   = new WSpace(20, 20);
            final WSpace right  = new WSpace(20, 20);
            final WSpace bottom  = new WSpace(20, 20);

            //decoration
            WContainer deco = getWidget();
            deco.setInlineStyle(new Chars(
                    "margin : 0;"+
                    "background : none;"+
                    "graphic : {"
                            + "radius : [40,40,40,40];"
                            + "brush : plainbrush(1,'round');"
                            + "brush-paint : $color-main;"
                            + "fill-paint : $color-focus;"
                            + "margin : 0;"
                            + "};"));
            deco.addChild(top,BorderConstraint.TOP);
            deco.addChild(bottom,BorderConstraint.BOTTOM);
            deco.addChild(left,BorderConstraint.LEFT);
            deco.addChild(right,BorderConstraint.RIGHT);

        }

    }

}
