
package demo.ui;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.SplitConstraint;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.widget.WGLCanvas;
import science.unlicense.engine.ui.component.WButtonBar;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSplitContainer;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.Angles;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 * A GLCanvas inside a Frame.
 *
 * @author Johann Sorel
 */
public class WSubGLPane {

    public static void main(String[] args) {

        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false);

        final WGLCanvas canvas = new WGLCanvas();
        buildScene(canvas);
        canvas.startRendering();

        final WButtonBar bar = new WButtonBar();
        bar.addChild(new WButton(new Chars("Action 1")), FillConstraint.builder().coord(0, 0).build());
        bar.addChild(new WButton(new Chars("Action 2")), FillConstraint.builder().coord(1, 0).build());
        bar.addChild(new WButton(new Chars("Action 3")), FillConstraint.builder().coord(2, 0).build());

        final WSplitContainer split = new WSplitContainer();
        split.addChild(canvas, SplitConstraint.RIGHT);
        split.addChild(new WLabel(new Chars("some text")), SplitConstraint.LEFT);

        frame.getContainer().addChild(split,BorderConstraint.CENTER);
        frame.getContainer().addChild(bar,BorderConstraint.TOP);

        frame.setSize(1024, 768);
        frame.setVisible(true);

    }

    private static void buildScene(WGLCanvas canvas){

        final GLProcessContext context = canvas.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        scene.getChildren().add(camera);

        //prepare the output FBO
        final FBO fbo = new FBO(100, 100);
        fbo.addAttachment(null, GLC.FBO.Attachment.COLOR_0, new Texture2D(100, 100,Texture2D.COLOR_RGBA_CLAMPED()));
        fbo.addAttachment(null, GLC.FBO.Attachment.DEPTH_STENCIL, new Texture2D(100, 100,Texture2D.DEPTH24_STENCIL8()));

        context.getPhases().add(new ClearPhase(fbo,new float[]{1,0,0,1}));
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera,fbo));

        canvas.setFinalFbo(fbo);

        //build a small scene
        final DefaultModel box = new DefaultModel(new BBox(new double[]{-5,-5,-5},new double[]{5,5,5}));
        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuse(Color.WHITE);
        box.getMaterials().add(material);
        scene.getChildren().add(box);

        //keyboard and mouse control
        final MotionModel crosshair = DefaultMotionModel.createCrosshair3D();
        scene.getChildren().add(crosshair);
        crosshair.getChildren().add(camera);
        final OrbitUpdater controller = new OrbitUpdater(canvas,camera,crosshair).configureDefault();
        controller.setDistance(20);
        controller.setHorizontalAngle(Angles.degreeToRadian(30));
        controller.setVerticalAngle(Angles.degreeToRadian(25));
        camera.getUpdaters().add(controller);
    }

}
