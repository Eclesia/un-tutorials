
package demo.ui;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int32;
import science.unlicense.display.api.desktop.FrameManagers;
import science.unlicense.display.api.layout.LineLayout;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WGraphicText;

/**
 *
 * @author Johann Sorel
 */
public class WFontQuality {

    public static void main(String[] args) {

        final UIFrame frame = (UIFrame) FrameManagers.getFrameManager().createFrame(false);

        final WContainer pane = frame.getContainer();
        pane.setLayout(new LineLayout());
        pane.setInlineStyle(new Chars("margin:10;"));

        for(int i=1;i<=100;i++){

            final WGraphicText label = new WGraphicText(Int32.encode(i));

            label.setInlineStyle(new Chars("font:{family:font('mona',"+i+",'none');};"));

            pane.getChildren().add(label);
        }

        frame.setSize(800, 800);
        frame.setVisible(true);
    }

}
