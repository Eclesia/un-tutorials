

package demo.ui;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.SplitConstraint;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.engine.opengl.widget.WGLPlane;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSplitContainer;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.math.api.Angles;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.engine.ui.component.path.WMediaViewer;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 *
 * @author Johann Sorel
 */
public class ExtrudedWidget {

    public static void main(String[] args) throws IOException, StoreException {

        //create a rendering frame
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(true);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        scene.getChildren().add(camera);
        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera));

        //build a small scene
        final WGLPlane plane = new WGLPlane(800,800,camera);
        plane.getNodeTransform().getScale().localScale(0.01);
        plane.getNodeTransform().notifyChanged();
        scene.getChildren().add(plane);

        final WContainer container = plane.getContainer();
        container.setFrame(frame);
        frame.addEventListener(MouseMessage.PREDICATE, plane);
        frame.addEventListener(KeyMessage.PREDICATE, plane);
        container.setLayout(new BorderLayout());

        WContainer all = new WContainer(new BorderLayout());
        all.addChild(new WLabel(new Chars("Hello World")), BorderConstraint.TOP);
        all.addChild(new WLabel(new Chars("ToolBar")), BorderConstraint.BOTTOM);
        all.addChild(new WLabel(new Chars("lefffffffffft")), BorderConstraint.LEFT);
        all.addChild(new WLabel(new Chars("rightttttttt")), BorderConstraint.RIGHT);

        WMediaViewer preview = new WMediaViewer(Paths
                .resolve(new Chars("file:/images/GRACE_globe_animation.gif")));
        all.addChild(preview, BorderConstraint.CENTER);

        WSplitContainer split = new WSplitContainer(true);
        split.addChild(new WButton(new Chars("Hello World")), SplitConstraint.TOP);
        split.addChild(all, SplitConstraint.BOTTOM);
        container.addChild(split, BorderConstraint.CENTER);




        //keyboard and mouse control
        final MotionModel crosshair = DefaultMotionModel.createCrosshair3D();
        scene.getChildren().add(crosshair);
        final OrbitUpdater controller = new OrbitUpdater(frame,camera,crosshair);
        controller.configureDefault();
        controller.setDistance(20);
        controller.setHorizontalAngle(Angles.degreeToRadian(30));
        controller.setVerticalAngle(Angles.degreeToRadian(25));
        camera.getUpdaters().add(controller);

        //show the frame
        frame.setSize(640, 480);
        frame.setVisible(true);



    }

}
