
package demo.ui;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.desktop.FrameManagers;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.engine.ui.component.WImageAnalizer;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class WImageAnalyzeDemo {

    public static void main(String[] args) throws IOException {
        
        final UIFrame frame = (UIFrame) FrameManagers.getFrameManager().createFrame(false);
        final Image image = Images.read(Paths.resolve(new Chars("mod:/images/unlicense.png")));
        
        final WImageAnalizer analizer = new WImageAnalizer();
        analizer.setImage(image);
        
        frame.getContainer().setLayout(new BorderLayout());
        frame.getContainer().addChild(analizer, BorderConstraint.CENTER);
        
        frame.setSize(800, 600);
        frame.setVisible(true);
        
    }
    
}
