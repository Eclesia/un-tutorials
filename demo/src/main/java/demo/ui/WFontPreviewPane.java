
package demo.ui;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.font.Font;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.display.api.font.FontMetadata;
import science.unlicense.display.api.layout.GridLayout;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.engine.ui.visual.WidgetView;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLeaf;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.image.api.color.Color;

/**
 *
 * @author Johann Sorel
 */
public class WFontPreviewPane extends WContainer {

    public WFontPreviewPane() {
        super(new GridLayout(-1, 3));

        final int[] chars = new int[]{'a','B','M','g','l','w'};

        for(int c : chars) {
            final WLeaf leaf = new WSpace();
            leaf.setView(new GlyphView(leaf, c));

            getChildren().add(leaf);
        }

    }

    private final class GlyphView extends WidgetView {

        private final int unicode;

        public GlyphView(Widget widget, int unicode) {
            super(widget);
            this.unicode = unicode;
        }

        @Override
        protected void renderSelf(Painter2D painter, BBox innerBBox) {
            super.renderSelf(painter, innerBBox);

            int size = 180;
            Font font = painter.getFontStore().getFont(new Chars("Mona"));
            font = font.derivate(size);

            final FontMetadata metaData = font.getMetaData();

            final FontChoice choice = new FontChoice(new Chars[]{font.getFamily()}, size, Font.WEIGHT_NONE);
            painter.setFont(choice);

            painter.setPaint(new ColorPaint(Color.BLACK));
            painter.fill(new Chars(new int[]{unicode}), 0, 0);


            painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_ROUND));

            double ascent = -metaData.getAscent();
            double descent = metaData.getDescent();
            double height = -metaData.getHeight();
            double lineGap = metaData.getLineGap();
            double advanceMax = metaData.getAdvanceWidthMax();
            double minLeftSideBearing = metaData.getMinLeftSideBearing();
            double minRightSideBearing = metaData.getMinRightSideBearing();
            double xMaxExtent = metaData.getXMaxExtent();

            painter.setPaint(new ColorPaint(Color.BLUE));
            painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_ROUND));
            painter.stroke(new DefaultLine(-1000, ascent, 1000, ascent));
            painter.stroke(new DefaultLine(-1000, descent, 1000, descent));

            painter.setPaint(new ColorPaint(Color.BLUE));
            painter.setBrush(new PlainBrush(2, PlainBrush.LINECAP_ROUND));
            painter.stroke(new DefaultLine(-1000, height, 1000, height));

            painter.setPaint(new ColorPaint(Color.GREEN));
            painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_ROUND));
            painter.stroke(new DefaultLine(minLeftSideBearing, -1000, minLeftSideBearing, 1000));
            painter.stroke(new DefaultLine(minRightSideBearing, -1000, minRightSideBearing, 1000));

            painter.setPaint(new ColorPaint(Color.GREEN));
            painter.setBrush(new PlainBrush(2, PlainBrush.LINECAP_ROUND));
            painter.stroke(new DefaultLine(advanceMax, -1000, advanceMax, 1000));

            painter.setPaint(new ColorPaint(Color.RED));
            painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_ROUND));
            painter.stroke(new DefaultLine(-1000, lineGap, 1000, lineGap));


        }

    }


}
