package demo.ui;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.number.NumberType;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.engine.ui.style.GeomStyle;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WTabContainer;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.engine.ui.widget.chart.WBarChart3D;
import science.unlicense.engine.ui.widget.chart.WFunctionSpaceChart;
import science.unlicense.engine.ui.widget.chart.WLineChart;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.tuple.TupleSpace;
import science.unlicense.geometry.api.tuple.TupleSpaceCursor;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.EasingMethods;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.api.system.SampleSystem;
import science.unlicense.math.api.system.UndefinedSystem;
import science.unlicense.math.impl.Scalarf64;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.math.impl.VectorNf64;

/**
 *
 * @author Johann Sorel
 */
public class WChartsPane extends WContainer {

    public WChartsPane() {
        setLayout(new BorderLayout());

        final WTabContainer tabs = new WTabContainer();
        tabs.setTabPosition(WTabContainer.TAB_POSITION_LEFT);
        tabs.addTab(buildLineChart(), new WLabel(new Chars("LineChart 2D")));
        tabs.addTab(buildSpaceChart(), new WLabel(new Chars("Tuple space 2D")));
        tabs.addTab(buildBarChart(), new WLabel(new Chars("BarChart 3D")));

        addChild(tabs, BorderConstraint.CENTER);
    }

    private Widget buildLineChart() {

        TupleSpace space1 = new AbstractTupleSpace1D() {
            @Override
            protected double evaluate(double x) {
                return 0.5;
            }

            @Override
            public NumberType getNumericType() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public Geometry getCoordinateGeometry() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };

        TupleSpace space2 = new AbstractTupleSpace1D() {
            @Override
            protected double evaluate(double x) {
                return x*x;
            }

            @Override
            public NumberType getNumericType() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public Geometry getCoordinateGeometry() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };

        TupleSpace space3 = new AbstractTupleSpace1D() {
            @Override
            protected double evaluate(double x) {
                return EasingMethods.cubicEaseInOut(x, 0, 1);
            }

            @Override
            public NumberType getNumericType() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public Geometry getCoordinateGeometry() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };

        final Dictionary dico = new HashDictionary();
        dico.add(new Chars("one"), space1);
        dico.add(new Chars("two"), space2);
        dico.add(new Chars("three"), space3);

        final Dictionary styles = new HashDictionary();
        styles.add(new Chars("one"), new GeomStyle(new PlainBrush(3, PlainBrush.LINECAP_ROUND, PlainBrush.LINEJOIN_ROUND, 0, 0, new float[]{5,5}), new ColorPaint(Color.RED), null));
        styles.add(new Chars("two"), new GeomStyle(new PlainBrush(2, PlainBrush.LINECAP_ROUND, PlainBrush.LINEJOIN_ROUND, 0, 0, null), new ColorPaint(Color.GREEN), null));
        styles.add(new Chars("three"), new GeomStyle(new PlainBrush(1, PlainBrush.LINECAP_ROUND, PlainBrush.LINEJOIN_ROUND, 0, 0, null), new ColorPaint(Color.BLUE), null));

        final WLineChart chart = new WLineChart();
        chart.setDatas(dico);
        chart.setStyles(styles);
        chart.setViewBox(new BBox(new Vector2f64(-2.5, -2.5), new Vector2f64(2.5, 2.5)));

        return chart;
    }

    private Widget buildBarChart() {

        final WBarChart3D barchart = new WBarChart3D();
        barchart.setBars(new double[]{15,5,20});
        return barchart;
    }

    private WFunctionSpaceChart buildSpaceChart() {

        final WFunctionSpaceChart chart = new WFunctionSpaceChart();

        final TupleSpace space = null; //new TransformSampler(SimplexNoise.NOISE_2D, new Affine2(1, 0, 0, 0, 1, 0));

        chart.setFunctionSpace(space);
        chart.setViewBox(new BBox(new Vector2f64(0, 0), new Vector2f64(25, 25)));
        return chart;
    }


    private static abstract class AbstractTupleSpace1D implements TupleSpace {

        protected abstract double evaluate(double x);

        @Override
        public SampleSystem getCoordinateSystem() {
            return UndefinedSystem.create(1);
        }

        @Override
        public SampleSystem getSampleSystem() {
            return UndefinedSystem.create(1);
        }

        @Override
        public TupleRW createTuple() {
            return new Scalarf64();
        }

        @Override
        public TupleRW getTuple(Tuple coordinate, TupleRW buffer) {
            buffer.set(0, evaluate(coordinate.get(0)));
            return buffer;
        }

        @Override
        public TupleSpaceCursor cursor() {
            return new CC();
        }

        private class CC extends VectorNf64 implements TupleSpaceCursor {

            private final Scalarf64 coord = new Scalarf64();

            public CC() {
                super(1);
            }

            @Override
            public Tuple coordinate() {
                return coord;
            }

            @Override
            public void moveTo(Tuple coordinate) {
                coord.x = coordinate.get(0);
                set(0, evaluate(coord.x));
            }

            @Override
            public Tuple samples() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        }

    }
}
