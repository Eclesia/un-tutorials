
package demo.ui;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.desktop.FrameManagers;
import science.unlicense.display.api.desktop.cursor.ImageCursor;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.frame.WFrameDecoration;
import science.unlicense.engine.ui.widget.frame.WFrameDrag;
import science.unlicense.engine.ui.widget.frame.WFrameResize;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.math.impl.Vector2f64;

/**
 *
 * @author Johann Sorel
 */
public class CustomDecoration {

    private static final Image ICON;
    static {
        try {
            ICON = Images.read(Paths.resolve(new Chars("mod:/images/unlicense.png")));
        } catch (IOException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    public static void main(String[] args) {

        final UIFrame frame = (UIFrame) FrameManagers.getFrameManager().createFrame(true);
        frame.setSize(800, 600);

        frame.getContainer().setLayout(new BorderLayout());
        frame.getContainer().addChild(WidgetShowcase.createLabelPane(),BorderConstraint.CENTER);

        frame.setDecoration(new MyDecoration());

        //cursor
        frame.setCursor(new ImageCursor(ICON,new Vector2f64(12, 12)));

        frame.setVisible(true);
    }


    private static class MyDecoration extends WFrameDecoration{

        public MyDecoration() {
            final WContainer top = new WContainer(new FormLayout(10,10));
            final WButton close = new WButton(new Chars("X"),null,new EventListener() {
                public void receiveEvent(Event event) {
                    System.exit(0);
                }
            });

            final WFrameDrag dragTitle = new WFrameDrag(new BorderLayout());
            dragTitle.addChild(new WLabel(new Chars("Some frame title, or anything. you can drag the frame too.")),BorderConstraint.CENTER);
            top.addChild(close, FillConstraint.builder().coord(0, 0).build());
            top.addChild(dragTitle, FillConstraint.builder().coord(1, 0).build());
            top.setInlineStyle(new Chars(
                    "margin : 30;"+
                    "background : none;"+
                    "graphic : {"
                            + "radius : [20,20,0,0];"
                            + "brush : plainbrush(1,'round');"
                            + "brush-paint : none;"
                            + "fill-paint : $color-focus;"
                            + "margin : 30;"
                            + "};"));

            final WContainer bottom = new WContainer(new BorderLayout());
            final WFrameResize resize = new WFrameResize(new BorderLayout());
            resize.addChild(new WLabel(new Chars("Resize me.")),BorderConstraint.RIGHT);
            bottom.addChild(resize,BorderConstraint.RIGHT);


            final WSpace left   = new WSpace(new Extent.Double(40, 40));
            final WSpace right  = new WSpace(new Extent.Double(40, 40));

            //decoration
            WContainer deco = getWidget();
            deco.setInlineStyle(new Chars(
                    "margin : 0;"+
                    "background : none;"+
                    "graphic : {"
                            + "radius : [40,40,40,40];"
                            + "brush : plainbrush(1,'round');"
                            + "brush-paint : $color-main;"
                            + "fill-paint : lineargradientfill('%',0,0,1,1,0,$color-main,1,$color-main);"
                            + "margin : [0,0,0,0];"
                            + "};"));
            deco.addChild(top,BorderConstraint.TOP);
            deco.addChild(bottom,BorderConstraint.BOTTOM);
            deco.addChild(left,BorderConstraint.LEFT);
            deco.addChild(right,BorderConstraint.RIGHT);

        }

    }

}
