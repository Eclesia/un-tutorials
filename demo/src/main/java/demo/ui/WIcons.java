
package demo.ui;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Int32;
import science.unlicense.display.api.desktop.FrameManagers;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WGraphicText;
import science.unlicense.engine.ui.widget.WLabel;

/**
 *
 * @author Johann Sorel
 */
public class WIcons {

    public static void main(String[] args) {

        final UIFrame frame = (UIFrame) FrameManagers.getFrameManager().createFrame(false);

        final WContainer pane = frame.getContainer();
        pane.setLayout(new FormLayout(14, 14));
        pane.setInlineStyle(new Chars("margin:10;"));

        for(int i=0xE000,x=0,y=0;i<=0xE040;i++,y++){
            if(y>=10){y=0;x++;}

            final WLabel label = new WLabel(Int32.encodeHexa(i));
            label.setGraphicPlacement(WLabel.GRAPHIC_LEFT);
            label.setVerticalAlignment(WLabel.VALIGN_CENTER);

            final WGraphicText graphic = new WGraphicText(new Chars(new int[]{i}));
            graphic.setInlineStyle(new Chars("font:{family:font('Unicon',32,'none');};"));
            label.setGraphic(graphic);

            pane.addChild(label, FillConstraint.builder().coord(x, y).build());
        }

        frame.setSize(800, 600);
        frame.setVisible(true);
    }

}
