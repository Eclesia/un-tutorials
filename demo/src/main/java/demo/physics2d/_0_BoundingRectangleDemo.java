
package demo.physics2d;

import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.physic.WorldUpdatePhase;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.Circle;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.physics.api.DefaultWorld;
import science.unlicense.physics.api.World;
import science.unlicense.physics.api.body.RigidBody;

/**
 *
 * @author Johann Sorel
 */
public class _0_BoundingRectangleDemo {

    public static void main(String[] args) {
        new _0_BoundingRectangleDemo();
    }

    public _0_BoundingRectangleDemo(){

        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        final SceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        scene.getChildren().add(camera);
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera));

        frame.setSize(400, 300);

        final WContainer container = frame.getContainer();
        container.setLayout(new BorderLayout());


        //create the physics world
        final World world = createWorldCircle();
        frame.getContext().getPhases().add(new WorldUpdatePhase(world));

        //create the display panel
        final WPhysicWorld wpane = new WPhysicWorld(world);
        container.addChild(wpane, BorderConstraint.CENTER);
        world.addEventListener(PropertyMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                wpane.setDirty();
            }
        });

        frame.setVisible(true);
    }

    private World createWorldCircle(){
        final World world = new DefaultWorld(2);
        //create static bodies, with a mass of infinite (value 0)
        world.addBody(new RigidBody(
                new Circle(new Vector2f64(20, 50), 10), 0));
        world.addBody(new RigidBody(
                new Circle(new Vector2f64(20, 200), 10), 0));

        //create a super ball
        final RigidBody ball = new RigidBody(
                new Circle(new Vector2f64(20, 100), 10),1);
        //restitution of one will make it bounds back keeping all it's velocity
        ball.getMaterial().setRestitution(1);
        ball.getMotion().getVelocity().setY(40);
        world.addBody(ball);

        return world;
    }

    private World createWorldRectangle(){
        final World world = new DefaultWorld(2);
        //create static bodies, with a mass of infinite (value 0)
        world.addBody(new RigidBody(new Rectangle(20,50 ,10,10), 0));
        world.addBody(new RigidBody(new Rectangle(20,200,10,10), 0));

        //create a super ball
        final RigidBody ball = new RigidBody(new Rectangle(20,100,10,10), 1);
        //restitution of one will make it bounds back keeping all it's velocity
        ball.getMaterial().setRestitution(1);
        ball.getMotion().getVelocity().setY(40);
        world.addBody(ball);

        return world;
    }

}
