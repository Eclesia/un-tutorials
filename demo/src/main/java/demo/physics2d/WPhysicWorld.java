
package demo.physics2d;

import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.engine.ui.visual.WidgetView;
import science.unlicense.engine.ui.widget.WLeaf;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Geometry;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.api.operation.OperationException;
import science.unlicense.geometry.api.operation.Operations;
import science.unlicense.geometry.api.operation.TransformOp;
import science.unlicense.image.api.color.Color;
import science.unlicense.physics.api.World;
import science.unlicense.physics.api.body.Body;
import science.unlicense.physics.api.body.RigidBody;

/**
 *
 * @author Johann Sorel
 */
public class WPhysicWorld extends WLeaf{

    private World world;

    public WPhysicWorld(World world) {
        this.world = world;
        setView(new WidgetView(this){
            protected void renderSelf(Painter2D painter, BBox innerBBox) {
                super.renderSelf(painter, innerBBox);
                WPhysicWorld.this.renderSelf(painter, innerBBox);
            }
        });
    }

    private void renderSelf(Painter2D painter, BBox innerBBox) {

        painter.setPaint(new ColorPaint(Color.RED));

        final Iterator ite = world.getBodies().createIterator();
        while(ite.hasNext()){
            final Body body = (Body) ite.next();
            if(body instanceof RigidBody){
                final RigidBody gb = (RigidBody) body;
                Geometry geom = gb.getGeometry();

                final TransformOp trs = new TransformOp(geom, gb.getNodeToRootSpace());
                try {
                    geom = (Geometry) Operations.execute(trs);
                } catch (OperationException ex) {
                    Loggers.get().warning(ex);
                }

                if(geom instanceof PlanarGeometry){
                    final PlanarGeometry g2d = (PlanarGeometry) geom;
                    painter.fill(g2d);
                    painter.stroke(g2d);
                }
            }
        }

    }

}
