
package demo.physics2d.particle;

import science.unlicense.geometry.api.BBox;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.Maths;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.physics.api.DefaultWorld;
import science.unlicense.physics.api.World;
import science.unlicense.physics.api.body.Particle;
import science.unlicense.physics.api.force.Attraction;
import science.unlicense.physics.api.integration.RungeKuttaIntegrator;

/**
 *
 * @author jeffrey traer bernstein (original source code)
 * @author Johann Sorel (adapted to unlicense project)
 */
public class _1_Bouncy_Balls extends AbstractParticuleDemo {

    public static void main(String[] args) {
        new _1_Bouncy_Balls().run();
    }

    private final World world;
    private final Particle mouse;
    private final Particle b;
    private final Particle c;

    public _1_Bouncy_Balls() {

        world = new DefaultWorld(2);
        world.setIntegrator(new RungeKuttaIntegrator(2));
        mouse = new Particle(2,1);
        mouse.setFixed(true);
        b = new Particle(2,1);
        c = new Particle(2,1);
        b.setWorldPosition(new Vector2f64(10, 30));
        c.setWorldPosition(new Vector2f64(40, 10));

        world.addBody(mouse);
        world.addBody(b);
        world.addBody(c);

        world.addForce(new Attraction(mouse, b, 10000, 10));
        world.addForce(new Attraction(mouse, c, 10000, 10));
        world.addForce(new Attraction(b, c, -10000, 5));
    }

    protected void draw() {
        mouse.setWorldPosition(new Vector2f64(mouseX, mouseY));
        handleBoundaryCollisions(b);
        handleBoundaryCollisions(c);
        world.update(1);

        ellipse(mouse.getWorldPosition(null).getX(), mouse.getWorldPosition(null).getY(), 35, 35, null, Color.BLACK);
        ellipse(b.getWorldPosition(null).getX(), b.getWorldPosition(null).getY(), 35, 35, Color.GRAY_NORMAL, Color.BLACK);
        ellipse(c.getWorldPosition(null).getX(), c.getWorldPosition(null).getY(), 35, 35, Color.GRAY_NORMAL, Color.BLACK);
    }

    // really basic collision strategy:
    // sides of the window are walls
    // if it hits a wall pull it outside the wall and flip the direction of the velocity
    // the collisions aren't perfect so we take them down a notch too
    private void handleBoundaryCollisions(Particle p) {
        final BBox bbox = getBoundingBox();

        if (p.getWorldPosition(null).getX() < bbox.getMin(0) || p.getWorldPosition(null).getX() > bbox.getMax(0)) {
            p.getMotion().getVelocity().setXY(-0.9f * p.getMotion().getVelocity().getX(), p.getMotion().getVelocity().getY());
        }
        if (p.getWorldPosition(null).getY() < bbox.getMin(1) || p.getWorldPosition(null).getY() > bbox.getMax(1)) {
            p.getMotion().getVelocity().setXY(p.getMotion().getVelocity().getX(), -0.9f * p.getMotion().getVelocity().getY());
        }
        p.setWorldPosition(new Vector2f64(
                Maths.clamp(p.getWorldPosition(null).getX(), bbox.getMin(0), bbox.getMax(0)),
                Maths.clamp(p.getWorldPosition(null).getY(), bbox.getMin(1), bbox.getMax(1))));
    }

}
