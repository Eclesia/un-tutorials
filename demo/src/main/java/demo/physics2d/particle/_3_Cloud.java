
package demo.physics2d.particle;

import science.unlicense.common.api.character.Chars;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.physics.api.DefaultWorld;
import science.unlicense.physics.api.World;
import science.unlicense.physics.api.body.Particle;
import science.unlicense.physics.api.force.Attraction;
import science.unlicense.physics.api.integration.RungeKuttaIntegrator;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author jeffrey traer bernstein (original source code)
 * @author Johann Sorel (adapted to unlicense project)
 */
public class _3_Cloud extends AbstractParticuleDemo {

    public static void main(String[] args) throws IOException {
        new _3_Cloud().run();
    }

    private final World world;
    private final Particle mouse;
    private final Particle[] others;
    private final Image img;

    public _3_Cloud() throws IOException {
        final int width = 300;
        final int height = 300;

        img = Images.read(Paths.resolve(new Chars("mod:/images/fade.png")));

        world = new DefaultWorld(2,0.1);
        world.setIntegrator(new RungeKuttaIntegrator(2));
        mouse = new Particle(2,1);
        mouse.setFixed(true);
        world.addBody(mouse);

        others = new Particle[100];
        for (int i = 0; i < others.length; i++) {
            others[i] = new Particle(2,1.0f);
            others[i].setWorldPosition(new Vector2f64(random(-width, width), random(-height, height)));
            world.addBody(others[i]);
            world.addForce(new Attraction(mouse, others[i], 5000, 50));
        }
    }

    protected void draw() {
        mouse.setWorldPosition(new Vector2f64(mouseX, mouseY));
        world.update(1);

        for (int i = 0; i < others.length; i++) {
            final Particle p = others[i];
            image(img,
                    p.getWorldPosition(null).getX() - img.getExtent().get(0) / 2,
                    p.getWorldPosition(null).getY() - img.getExtent().get(1) / 2);
        }
    }

}
