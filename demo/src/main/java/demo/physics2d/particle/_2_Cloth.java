
package demo.physics2d.particle;

import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.geometry.impl.Path;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.physics.api.DefaultWorld;
import science.unlicense.physics.api.World;
import science.unlicense.physics.api.body.Particle;
import science.unlicense.physics.api.force.Gravity;
import science.unlicense.physics.api.force.Spring;
import science.unlicense.physics.api.integration.RungeKuttaIntegrator;

/**
 *
 * @author jeffrey traer bernstein (original source code)
 * @author Johann Sorel (adapted to unlicense project)
 */
public class _2_Cloth extends AbstractParticuleDemo {

    private static final float SPRING_STRENGTH = 0.2f;
    private static final float SPRING_DAMPING = 0.1f;
    private static final int GRID_SIZE = 10;

    public static void main(String[] args) {
        new _2_Cloth().run();
    }

    private final World world;
    private final Particle[][] particles;

    public _2_Cloth() {

        world = new DefaultWorld(2,0.01f);
        world.setIntegrator(new RungeKuttaIntegrator(2));
        world.getSingularities().add(new Gravity(new Vector2f64(0.0,0.1)));
        particles = new Particle[GRID_SIZE][GRID_SIZE];

        final float gridStepX = (float) (400 / GRID_SIZE);
        final float gridStepY = (float) (400 / GRID_SIZE);

        for (int i = 0; i < GRID_SIZE; i++) {
            for (int j = 0; j < GRID_SIZE; j++) {
                particles[i][j] = new Particle(2,0.2f);
                particles[i][j].setWorldPosition(new Vector2f64(j * gridStepX -150, i * gridStepY + 20));
                world.addBody(particles[i][j]);
                if (j > 0) {
                    final Spring s = new Spring(
                            particles[i][j - 1],
                            particles[i][j],
                            SPRING_STRENGTH, SPRING_DAMPING, gridStepX);
                    world.addForce(s);
                }
            }
        }

        for (int j = 0; j < GRID_SIZE; j++) {
            for (int i = 1; i < GRID_SIZE; i++) {
                final Spring s = new Spring(
                        particles[i - 1][j],
                        particles[i][j],
                        SPRING_STRENGTH, SPRING_DAMPING, gridStepY);
                world.addForce(s);
            }
        }

        particles[0][0].setFixed(true);
        particles[0][GRID_SIZE-1].setFixed(true);
    }

    protected void draw() {
        world.update(1);

        if (mousePressed) {
            particles[0][GRID_SIZE-1].setWorldPosition(new Vector2f64(mouseX, mouseY));
            particles[0][GRID_SIZE-1].getMotion().getVelocity().setAll(0.0);
        }

        for (int i = 0; i < GRID_SIZE; i++) {
            final Path shape = new Path();
            shape.appendMoveTo(particles[i][0].getWorldPosition(null).getX(), particles[i][0].getWorldPosition(null).getY());
            for (int j = 0; j < GRID_SIZE; j++) {
                shape.appendLineTo(particles[i][j].getWorldPosition(null).getX(), particles[i][j].getWorldPosition(null).getY());
            }
            shape.appendLineTo(particles[i][GRID_SIZE - 1].getWorldPosition(null).getX(), particles[i][GRID_SIZE - 1].getWorldPosition(null).getY());

            painter.setPaint(new ColorPaint(Color.BLACK));
            painter.stroke(shape);
        }
        for (int j = 0; j < GRID_SIZE; j++) {
            final Path shape = new Path();

            shape.appendMoveTo(particles[0][j].getWorldPosition(null).getX(), particles[0][j].getWorldPosition(null).getY());
            for (int i = 0; i < GRID_SIZE; i++) {
                shape.appendLineTo(particles[i][j].getWorldPosition(null).getX(), particles[i][j].getWorldPosition(null).getY());
            }
            shape.appendLineTo(particles[GRID_SIZE - 1][j].getWorldPosition(null).getX(), particles[GRID_SIZE - 1][j].getWorldPosition(null).getY());

            painter.setPaint(new ColorPaint(Color.BLACK));
            painter.stroke(shape);
        }
    }

    protected void mouseReleased() {
        particles[0][GRID_SIZE - 1].getMotion().getVelocity().setXY((mouseX - pmouseX), (mouseY - pmouseY));
    }

}
