
package demo.physics2d.particle;

import science.unlicense.common.api.collection.Sequence;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.physics.api.DefaultWorld;
import science.unlicense.physics.api.World;
import science.unlicense.physics.api.body.Particle;
import science.unlicense.physics.api.body.RigidBody;
import science.unlicense.physics.api.force.Attraction;
import science.unlicense.physics.api.force.Force;
import science.unlicense.physics.api.force.Spring;
import science.unlicense.physics.api.integration.RungeKuttaIntegrator;

/**
 *
 * @author jeffrey traer bernstein (original source code)
 * @author Johann Sorel (adapted to unlicense project)
 */
public class _4_Random_Arboretum extends AbstractParticuleDemo {

    private static final float NODE_SIZE = 10;
    private static final float EDGE_LENGTH = 20;
    private static final float EDGE_STRENGTH = 0.2f;
    private static final float SPACER_STRENGTH = 1000;

    public static void main(String[] args) {
        new _4_Random_Arboretum().run();
    }

    private final World physics;


    // PROCESSING /////////////////////////////////////
    public _4_Random_Arboretum() {

        physics = new DefaultWorld(2,0.1f);
        physics.setIntegrator(new RungeKuttaIntegrator(2));

        // Runge-Kutta, the default integrator is stable and snappy,
        // but slows down quickly as you add particles.
        // 500 particles = 7 fps on my machine

        // Try this to see how Euler is faster, but borderline unstable.
        // 500 particles = 24 fps on my machine
        //physics.setIntegrator( ParticleSystem.MODIFIED_EULER );

        // Now try this to see make it more damped, but stable.
        //physics.setDrag( 0.2 );

        physics.removeAll();
        physics.addBody(new Particle(2,1));
    }

    protected void draw() {
        physics.update(1);

        // draw vertices
        final Sequence particles = physics.getBodies();
        final int nbParticles = particles.getSize();
        for (int i = 0; i < nbParticles; ++i) {
            final Particle v = (Particle) particles.get(i);
            ellipse(v.getWorldPosition(null).getX(), v.getWorldPosition(null).getY(), NODE_SIZE, NODE_SIZE, Color.GRAY_LIGHT, null);
        }

        // draw edges
        final Sequence forces = physics.getForces();
        final int nbForces = forces.getSize();
        for (int i = 0; i < nbForces; ++i) {
            final Force f = (Force) forces.get(i);
            if (!(f instanceof Spring)) {
                continue;
            }
            final Spring e = (Spring) f;
            RigidBody a = e.getFirstEnd();
            RigidBody b = e.getSecondEnd();
            line(a.getWorldPosition(null).getX(), a.getWorldPosition(null).getY(),
                 b.getWorldPosition(null).getX(), b.getWorldPosition(null).getY(),
                 Color.GRAY_NORMAL);
        }
    }

    protected void mousePressed() {
        addNode();
    }

    protected void mouseDragged() {
        addNode();
    }

    // ME ////////////////////////////////////////////

    private void addSpacersToNode(Particle p, Particle r) {
        final Sequence particles = physics.getBodies();
        final int nbParticles = particles.getSize();
        for (int i = 0; i < nbParticles; ++i) {
            Particle q = (Particle) particles.get(i);
            if (p != q && p != r) {
                physics.addForce(new Attraction(p, q, -SPACER_STRENGTH, 20));
            }
        }
    }

    private void makeEdgeBetween(Particle a, Particle b) {
        final Spring s = new Spring(a, b, EDGE_STRENGTH, EDGE_STRENGTH, EDGE_LENGTH);
        physics.addForce(s);
    }

    private void addNode() {
        final Sequence particles = physics.getBodies();
        final int lastParticule = particles.getSize() - 1;
        Particle p = new Particle(2,1);
        physics.addBody(p);
        Particle q = (Particle) particles.get((int) random(0, lastParticule));
        while (q == p) {
            q = (Particle) particles.get((int) random(0, lastParticule));
        }
        addSpacersToNode(p, q);
        makeEdgeBetween(p, q);
        p.setWorldPosition(new Vector2f64(
                q.getWorldPosition(null).getX() + random(-1, 1),
                q.getWorldPosition(null).getY() + random(-1, 1)));
    }

}
