
package demo.physics2d.particle;

import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.geometry.api.operation.DistanceOp;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.api.VectorRW;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.physics.api.DefaultWorld;
import science.unlicense.physics.api.World;
import science.unlicense.physics.api.body.Particle;
import science.unlicense.physics.api.force.Spring;
import science.unlicense.physics.api.integration.RungeKuttaIntegrator;

/**
 *
 * @author jeffrey traer bernstein (original source code)
 * @author Johann Sorel (adapted to unlicense project)
 */
public class _5_Tendrils extends AbstractParticuleDemo {

    public static void main(String[] args) {
        new _5_Tendrils().run();
    }

    private final java.util.Vector tendrils;
    private final World world;
    private final Particle mouse;
    private int greyer;
    private boolean drawing;

    public _5_Tendrils() {

        world = new DefaultWorld(2,0.05f);
        world.setIntegrator(new RungeKuttaIntegrator(2));


        mouse = new Particle(2,1);
        mouse.setFixed(true);
        world.addBody(mouse);

        tendrils = new java.util.Vector();
        drawing = false;
        greyer = 255;
    }

    protected void draw() {
        mouse.setWorldPosition(new Vector2f64(mouseX, mouseY));

        if (!drawing) {
            world.update(1);
            if (greyer < 255) {
                greyer *= 1.11111;
            }
            if (greyer > 255) {
                greyer = 255;
            }
        } else {
            if (greyer >= 64) {
                greyer *= 0.9;
            }
        }

        drawOldGrey();
    }

    private void drawOldGrey() {
        for (int i = 0; i < tendrils.size() - 1; ++i) {
            final T3ndril t = (T3ndril) tendrils.get(i);
            drawElastic(t, new ColorRGB(255-greyer,255-greyer,255-greyer));
        }

        if (tendrils.size() - 1 >= 0) {
            drawElastic((T3ndril) tendrils.lastElement(), Color.BLACK);
        }
    }

    void drawElastic(T3ndril t, Color color) {
        double lastStretch = 1;
        for (int i = 0; i < t.particles.size() - 1; ++i) {
            final VectorRW firstPoint = ((Particle) t.particles.get(i)).getWorldPosition(null);
            final VectorRW firstAnchor = i < 1 ? firstPoint : ((Particle) t.particles.get(i - 1)).getWorldPosition(null);
            final VectorRW secondPoint = i + 1 < t.particles.size() ? ((Particle) t.particles.get(i + 1)).getWorldPosition(null) : firstPoint;
            final VectorRW secondAnchor = i + 2 < t.particles.size() ? ((Particle) t.particles.get(i + 2)).getWorldPosition(null) : secondPoint;

            //float springStretch = 2.5f/((Spring)t.springs.get( i )).stretch();
            final Spring s = (Spring) t.springs.get(i);
            final double springStretch = 2.5f * s.getRestLength() / s.getCurrentLength();

            // smooth out the changes in stroke width with filter
            painter.setBrush(new PlainBrush((float) ((springStretch + lastStretch) / 2.0f), PlainBrush.LINECAP_ROUND));
            lastStretch = springStretch;

            curve(firstAnchor.getX(), firstAnchor.getY(),
                    firstPoint.getX(), firstPoint.getY(),
                    secondPoint.getX(), secondPoint.getY(),
                    secondAnchor.getX(), secondAnchor.getY(),
                    color);
        }
    }

    protected void mousePressed() {
        drawing = true;
        tendrils.add(new T3ndril(world, new Vector2f64(mouseX, mouseY), mouse));
    }

    protected void mouseDragged() {
        ((T3ndril) tendrils.lastElement()).addPoint(new Vector2f64(mouseX, mouseY));
    }

    protected void mouseReleased() {
        drawing = false;
    }

    private class T3ndril{

        private final java.util.Vector particles = new java.util.Vector();
        private final java.util.Vector springs = new java.util.Vector();
        private final World physics;

        public T3ndril(World p, VectorRW firstPoint, Particle followPoint) {
            physics = p;

            Particle firstParticle = new Particle(2,1.0);
            firstParticle.setWorldPosition(firstPoint);
            p.addBody(firstParticle);
            particles.add(firstParticle);
            final Spring s = new Spring(followPoint, firstParticle, 0.1f, 0.1f, 5);
            physics.addForce(s);
        }

        public void addPoint(VectorRW p) {
            Particle thisParticle = new Particle(2,1.0);
            thisParticle.setWorldPosition(p);
            physics.addBody(thisParticle);
            final Spring s = new Spring(
                    (Particle) particles.lastElement(),
                    thisParticle,
                    1.0f,
                    1.0f,
                    DistanceOp.distance(
                    ((Particle) particles.lastElement()).getWorldPosition(null),
                    thisParticle.getWorldPosition(null)));
            physics.addForce(s);
            springs.add(s);
            particles.add(thisParticle);
        }
    }

}
