
package demo.physics2d.particle;

import science.unlicense.image.api.color.Color;
import science.unlicense.math.impl.Vector2f64;
import science.unlicense.physics.api.DefaultWorld;
import science.unlicense.physics.api.World;
import science.unlicense.physics.api.body.Particle;
import science.unlicense.physics.api.force.Gravity;
import science.unlicense.physics.api.force.Spring;
import science.unlicense.physics.api.integration.RungeKuttaIntegrator;

/**
 *
 * @author jeffrey traer bernstein (original source code)
 * @author Johann Sorel (adapted to unlicense project)
 */
public class _0_Simple_pendulum extends AbstractParticuleDemo {

    public static void main(String[] args) {
        new _0_Simple_pendulum().run();
    }

    private final World world;
    private final Particle pinch;
    private final Particle anchor;
    private final Spring spring;

    public  _0_Simple_pendulum(){
      world = new DefaultWorld(2, 0.05f);
      world.setIntegrator(new RungeKuttaIntegrator(2));

      pinch = new Particle(2,1);
      anchor = new Particle(2,1);
      anchor.setFixed(true);
      spring = new Spring(pinch, anchor, 0.5f, 0.1f, 75);

      world.addSingularity(new Gravity(new Vector2f64(0, 1)));
      world.addBody(pinch);
      world.addBody(anchor);
      world.addForce(spring);
    }

    protected void mousePressed() {
        pinch.setFixed(true);
        pinch.getNodeTransform().getTranslation().setXY(mouseX, mouseY);
        pinch.getNodeTransform().notifyChanged();
    }

    protected void mouseDragged() {
        pinch.getNodeTransform().getTranslation().setXY(mouseX, mouseY);
        pinch.getNodeTransform().notifyChanged();
    }

    protected void mouseReleased() {
        pinch.setFixed(false);
    }

    protected void draw() {
      world.update(1);

      double[] p = pinch.getWorldPosition(null).toDouble();
      double[] a = anchor.getWorldPosition(null).toDouble();

      line( p[0], p[1], a[0], a[1], Color.BLACK);
      ellipse( a[0], a[1], 5, 5, Color.GRAY_NORMAL, Color.BLACK );
      ellipse( p[0], p[1], 20, 20, Color.GRAY_NORMAL, Color.BLACK );

    }

}
