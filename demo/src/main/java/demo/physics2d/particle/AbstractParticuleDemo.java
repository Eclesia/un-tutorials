
package demo.physics2d.particle;

import science.unlicense.common.api.event.Event;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.display.api.desktop.FrameManagers;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.image.api.color.Color;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.impl.Ellipse;
import science.unlicense.geometry.impl.Path;
import science.unlicense.image.api.Image;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.engine.ui.visual.WidgetView;
import science.unlicense.engine.ui.widget.WLeaf;
import static java.lang.Thread.sleep;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.math.impl.Affine2;

/**
 *
 * @author Johann Sorel
 */
public abstract class AbstractParticuleDemo extends WLeaf{

    private final UIFrame frame = (UIFrame) FrameManagers.getFrameManager().createFrame(false);

    protected Painter2D painter;
    protected boolean mousePressed = false;
    protected float pmouseX;
    protected float pmouseY;
    protected float mouseX;
    protected float mouseY;

    private volatile boolean running = true;

    public AbstractParticuleDemo() {
        setView(new WidgetView(this){
            protected void renderSelf(Painter2D painter, BBox innerBBox) {
                super.renderSelf(painter, innerBBox);
                AbstractParticuleDemo.this.painter = painter;
                painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_ROUND));
                draw();
            }
        });


        final WContainer base = frame.getContainer();
        base.setLayout(new BorderLayout());
        base.addChild(this, BorderConstraint.CENTER);

        frame.setSize(600, 600);
    }

    public void run(){
        frame.setVisible(true);

        new Thread(){
            @Override
            public void run() {
                while(running){
                    try {
                        sleep(20);
                    } catch (InterruptedException ex) {
                        Loggers.get().warning(ex);
                    }
                    setDirty();
                }
            }

        }.start();
    }

    @Override
    protected void receiveEventParent(Event event) {
        super.receiveEventParent(event);

        if(event.getMessage() instanceof MouseMessage){
            final MouseMessage me = (MouseMessage) event.getMessage();
            pmouseX = mouseX;
            pmouseY = mouseY;
            mouseX = (float)me.getMousePosition().get(0);
            mouseY = (float)me.getMousePosition().get(1);
            final int type = me.getType();
            if(type == MouseMessage.TYPE_PRESS){
                mousePressed = true;
                mousePressed();
            }else if(type == MouseMessage.TYPE_MOVE){
                if(mousePressed){
                    mouseDragged();
                }
            }else if(type == MouseMessage.TYPE_RELEASE){
                mousePressed = false;
                mouseReleased();
            }
        }
    }

    protected void draw(){}

    protected void mousePressed(){}

    protected void mouseDragged(){}

    protected void mouseReleased(){}

    protected double random(double min, double max){
        return min + (Math.random() * (max-min));
    }

    protected void line(double x, double y, double x0, double y0, Color stroke) {
        if(stroke!=null){
            painter.setPaint(new ColorPaint(stroke));
            painter.stroke(new DefaultLine(x, y, x0, y0));
        }
    }

    protected void ellipse(double cx, double cy, double dx, double dy, Color fill, Color stroke) {
        if(fill != null){
            painter.setPaint(new ColorPaint(fill));
            painter.fill(new Ellipse(cx, cy, dx/2, dy/2));
        }
        if(stroke != null){
            painter.setPaint(new ColorPaint(stroke));
            painter.stroke(new Ellipse(cx, cy, dx/2, dy/2));
        }
    }

    protected void curve(double x, double y, double x0, double y0, double x1, double y1, double x2, double y2, Color color) {
        final Path path = new Path();
        path.appendMoveTo(x, y);
        path.appendCubicTo(x0, y0, x1, y1, x2, y2);
        if(color != null){
            painter.setPaint(new ColorPaint(color));
            painter.stroke(path);
        }
    }

    protected void image(Image img, double x, double y) {
        final Affine2 m = new Affine2();
        m.set(0, 3, x);
        m.set(1, 3, y);
        painter.paint(img, m);
    }

}
