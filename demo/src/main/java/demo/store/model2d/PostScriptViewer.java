
package demo.store.model2d;

import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.desktop.FrameManagers;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.visual.WidgetView;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.format.ps.ps.PSReader;
import science.unlicense.format.ps.ps.vm.PSVirtualMachine;
import science.unlicense.geometry.api.BBox;
import science.unlicense.math.impl.Affine2;

/**
 * Demo rendering a postscript file in a frame.
 *
 * @author Johann Sorel
 */
public class PostScriptViewer {

    public static void main(String[] args) {

        final Path postscriptFile = Paths.resolve(new Chars("file:..."));

        final UIFrame frame = (UIFrame) FrameManagers.getFrameManager().createFrame(false);
        frame.setTitle(new Chars("Unlicense-lib : Postscript demo"));

        final WSpace space = new WSpace();
        space.setInlineStyle(new Chars("background:{fill-paint:#CCCCCC;};"));
        space.setView(new WidgetView(space){
            @Override
            protected void renderSelf(Painter2D painter, BBox innerBBox) {
                super.renderSelf(painter, innerBBox);

                Affine2 trs = new Affine2();
                trs.set(0, 2, innerBBox.getMin(0)+10);
                trs.set(1, 1, -1);
                trs.set(1, 2, -innerBBox.getMin(1)-10);
                painter = painter.derivate(true);
                painter.setTransform(new Affine2(trs.multiply(painter.getTransform())));

                try {
                    final PSReader reader = new PSReader();
                    reader.setInput(postscriptFile);

                    final PSVirtualMachine vm = new PSVirtualMachine();
                    vm.setPainter(painter);
                    for(Object o = reader.next();o!=null;o=reader.next()){
                        vm.push(o);
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }


            }
        });

        frame.getContainer().setLayout(new BorderLayout());
        frame.getContainer().addChild(space, BorderConstraint.CENTER);

        frame.setSize(1024, 768);
        frame.setVisible(true);

    }

}
