
package demo.store.model3d;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.NodeType;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.format.blend.BlendStore;
import science.unlicense.format.blend.model.BlendFile;

/**
 *
 * @author Johann Sorel
 */
public class BlenderExplorerDemo {

    public static void main(String[] args) throws IOException, StoreException {

        final Path path = Paths.resolve(new Chars("mod:/store/model3d/SimpleScene.blend"));
        final BlendStore store = new BlendStore(path);
        final BlendFile blend = store.getBlendFile();

        //the sdna contain the structure of the file, like an XSD
        //fields with an * are references to other blocks
        final NodeType[] structures = blend.sdnaBlock.getStructures();
        for(int i=0;i<structures.length;i++){
            System.out.println(structures[i]);
        }

        //print all blocks in the file
        for(int i=0;i<blend.blocks.getSize();i++){
            System.out.println(blend.blocks.get(i));
        }
    }

}
