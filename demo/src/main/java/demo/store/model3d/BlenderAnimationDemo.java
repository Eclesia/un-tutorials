
package demo.store.model3d;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.game.phase.GamePhases;
import science.unlicense.engine.opengl.mesh.GLModel;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.technique.GLTechnique;
import science.unlicense.format.blend.BlendStore;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.model3d.impl.physic.SkeletonAnimation;
import science.unlicense.model3d.impl.physic.Skeletons;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 *
 * @author Johann Sorel
 */
public class BlenderAnimationDemo {

    public static void main(String[] args) throws IOException, StoreException {

        //open blender file
        final Path path = Paths.resolve(new Chars("file:/..."));
        final BlendStore store = new BlendStore(path);
        final Collection col = store.getElements();

        //make a small scene
        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GamePhases rendering = new GamePhases();
        frame.getContext().getPhases().add(new ClearPhase());
        frame.getContext().getPhases().add(rendering);


        final MotionModel crosshair = DefaultMotionModel.createCrosshair3D();
        final MonoCamera camera = new MonoCamera();
        final OrbitUpdater orbitController = new OrbitUpdater(frame, camera, crosshair);
        orbitController.configureDefault();
        orbitController.setDistance(1);
        camera.getUpdaters().add(orbitController);
        crosshair.getChildren().add(camera);

        rendering.setCamera(camera);
        rendering.getScene().getChildren().add(crosshair);


        //add models to the scene
        MotionModel mpm = null;
        SkeletonAnimation animation = null;
        final Iterator ite = col.createIterator();
        while(ite.hasNext()){
            final Object candidate = ite.next();
            if(candidate instanceof SkeletonAnimation){
                animation = (SkeletonAnimation) candidate;
            }else if(candidate instanceof SceneNode){
                mpm = (MotionModel) ((SceneNode)candidate).getChildren().get(0);
                GLModel mesh = (GLModel) mpm.getChildren().get(0);
                ((GLTechnique)mesh.getTechniques().get(0)).getState().setPolygonMode(GLC.POLYGON_MODE.LINE);
                //final DebugSkeletonNode sn = new DebugSkeletonNode(mpm);
                //sn.setJointVisible(true);
                //rendering.getScene().getChildren().add(sn);
            }
        }

        rendering.getScene().getChildren().add(mpm);

        if(animation!=null){
            Skeletons.mapAnimation(animation, mpm.getSkeleton(), mpm,null);
            mpm.getUpdaters().add(animation);
            final SkeletonAnimation ani = animation;
            frame.addEventListener(KeyMessage.PREDICATE, new EventListener() {
                public void receiveEvent(Event event) {
                    ani.start();
                }
            });
        }



        frame.setSize(800, 600);
        frame.setVisible(true);

    }

}
