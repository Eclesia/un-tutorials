
package demo.store.binding;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.common.api.model.doc.Field;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.desktop.FrameManagers;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WTextField;
import science.unlicense.format.json.JSONUtilities;

/**
 *
 * @author Johann Sorel
 */
public class JsonEditorDemo {

    public static void main(String[] args) throws IOException {

        final Path path = Paths.resolve(new Chars("file:/...json"));

        //read json
        final Document doc = JSONUtilities.readAsDocument(path, null);

        //change listener to write file
        final EventListener lst = new EventListener() {
                public void receiveEvent(Event event) {
                    try {
                        JSONUtilities.writeDocument(doc, path);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            };

        //json editor
        final UIFrame frame = (UIFrame) FrameManagers.getFrameManager().createFrame(false);
        final WContainer container = frame.getContainer();
        container.setLayout(new FormLayout(5,5));

        final Iterator ite = doc.getPropertyNames().createIterator();
        for(int i=0;ite.hasNext();i++) {
            final Chars fieldName = (Chars) ite.next();
            final Field field     = doc.getProperty(fieldName);
            final WLabel labl     = new WLabel(fieldName);
            final WTextField text = new WTextField();
            text.varText().sync(field,false);
            text.varText().addListener(lst);
            container.addChild(labl, FillConstraint.builder().coord(0, i).build());
            container.addChild(text, FillConstraint.builder().coord(1, i).build());
        }

        //show frame
        frame.setSize(800, 600);
        frame.setVisible(true);
    }


}
