
package demo;

import demo.geometry.Triangulation;
import demo.opengl._0_getting_started._0_FrameDemo;
import demo.opengl._0_getting_started._1_CubeDemo;
import demo.opengl._0_getting_started._2_ImageDemo;
import demo.opengl._8_controls._0_OrbitCameraDemo;
import demo.opengl._1_frame_states._0_FullscreenDemo;
import demo.opengl._1_frame_states._1_TranslucentFrameDemo;
import demo.opengl._1_frame_states._2_OffscreenDemo;
import demo.opengl._2_geometries._0_DefaultMeshDemo;
import demo.opengl._2_geometries._1_CustomMeshDemo;
import demo.opengl._2_geometries._2_BillboardDemo;
import demo.opengl._4_medias._0_VideoDemo;
import demo.opengl._5_effects._0_phases;
import demo.opengl._5_effects._1_noise;
import demo.opengl._5_effects._2_convolution;
import demo.opengl._5_effects._3_fxaa;
import demo.opengl._6_physics._0_BouncingBall;
import demo.opengl._7_other.ClockWidget;
import demo.physics2d.particle._0_Simple_pendulum;
import demo.physics2d.particle._1_Bouncy_Balls;
import demo.physics2d.particle._2_Cloth;
import demo.physics2d.particle._3_Cloud;
import demo.physics2d.particle._4_Random_Arboretum;
import demo.physics2d.particle._5_Tendrils;
import demo.ui.WidgetShowcase;
import java.lang.reflect.Method;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.common.api.model.tree.DefaultNamedNode;
import science.unlicense.common.api.model.tree.NamedNode;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.display.api.desktop.FrameManagers;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WTree;
import science.unlicense.engine.ui.model.TreeRowModel;

/**
 * A small dialog to choose which demo to run.
 *
 * @author Johann Sorel
 */
public class DemoChooser {

    public static void main(String[] args) throws IOException {

        final UIFrame frame = (UIFrame) FrameManagers.getFrameManager().createFrame(false);
        frame.setTitle(new Chars("Demo chooser"));
        frame.setSize(350, 500);

        final Node root = new DefaultNamedNode(new Chars("Demos"), true);

        final Node n2d = new DefaultNamedNode(new Chars("2D"), true);
        n2d.getChildren().add(createNode("Widgets", WidgetShowcase.class));
        n2d.getChildren().add(createNode("Triangulation", Triangulation.class));
        n2d.getChildren().add(createNode("Physics 2D - Pendulum", _0_Simple_pendulum.class));
        n2d.getChildren().add(createNode("Physics 2D - Balls", _1_Bouncy_Balls.class));
        n2d.getChildren().add(createNode("Physics 2D - Cloth", _2_Cloth.class));
        n2d.getChildren().add(createNode("Physics 2D - Cloud", _3_Cloud.class));
        n2d.getChildren().add(createNode("Physics 2D - Arboretum", _4_Random_Arboretum.class));
        n2d.getChildren().add(createNode("Physics 2D - Tendrils", _5_Tendrils.class));
        root.getChildren().add(n2d);

        final Node n3d = new DefaultNamedNode(new Chars("3Ds"), true);
        final Node n3_0 = new DefaultNamedNode(new Chars("getting started"), true);
        n3_0.getChildren().add(createNode("Frame", _0_FrameDemo.class));
        n3_0.getChildren().add(createNode("Cube", _1_CubeDemo.class));
        n3_0.getChildren().add(createNode("Image", _2_ImageDemo.class));
        n3_0.getChildren().add(createNode("Orbit camera", _0_OrbitCameraDemo.class));
        final Node n3_1 = new DefaultNamedNode(new Chars("frame states"), true);
        n3_1.getChildren().add(createNode("Fullscreen", _0_FullscreenDemo.class));
        n3_1.getChildren().add(createNode("Translucent", _1_TranslucentFrameDemo.class));
        n3_1.getChildren().add(createNode("Offscreen", _2_OffscreenDemo.class));
        final Node n3_2 = new DefaultNamedNode(new Chars("geometries"), true);
        n3_2.getChildren().add(createNode("Default mesh", _0_DefaultMeshDemo.class));
        n3_2.getChildren().add(createNode("Custom mesh", _1_CustomMeshDemo.class));
        n3_2.getChildren().add(createNode("Billboard", _2_BillboardDemo.class));
        final Node n3_3 = new DefaultNamedNode(new Chars("animations"), true);
        final Node n3_4 = new DefaultNamedNode(new Chars("medias"), true);
        n3_4.getChildren().add(createNode("Video", _0_VideoDemo.class));
        final Node n3_5 = new DefaultNamedNode(new Chars("effects"), true);
        n3_5.getChildren().add(createNode("Phases", _0_phases.class));
        n3_5.getChildren().add(createNode("Noise", _1_noise.class));
        n3_5.getChildren().add(createNode("Convolution", _2_convolution.class));
        n3_5.getChildren().add(createNode("FXAA", _3_fxaa.class));
        final Node n3_6 = new DefaultNamedNode(new Chars("physics"), true);
        n3_6.getChildren().add(createNode("Bouncing ball", _0_BouncingBall.class));
        //n3_6.addChild(createNode("Falling ball", WidgetShowcase.class));
        //n3_6.addChild(createNode("Cloth", WidgetShowcase.class));
        final Node n3_7 = new DefaultNamedNode(new Chars("other"), true);
        n3_7.getChildren().add(createNode("Clock", ClockWidget.class));

        n3d.getChildren().add(n3_0);
        n3d.getChildren().add(n3_1);
        n3d.getChildren().add(n3_2);
        n3d.getChildren().add(n3_3);
        n3d.getChildren().add(n3_4);
        n3d.getChildren().add(n3_5);
        n3d.getChildren().add(n3_6);
        n3d.getChildren().add(n3_7);
        root.getChildren().add(n3d);

        final WTree tree = new WTree();
        final TreeRowModel rowModel = new TreeRowModel(root);
        tree.setRowModel(rowModel);
        rowModel.setFold(root, true);
        rowModel.setFold(n2d, true);
        rowModel.setFold(n3d, true);
        rowModel.setFold(n3_0, true);
        rowModel.setFold(n3_1, true);
        rowModel.setFold(n3_2, true);
        rowModel.setFold(n3_3, true);
        rowModel.setFold(n3_4, true);
        rowModel.setFold(n3_5, true);
        rowModel.setFold(n3_6, true);
        rowModel.setFold(n3_7, true);


        final WButton button = new WButton(new Chars("run"), null,new EventListener() {
            public void receiveEvent(Event event) {
                final Sequence selected = tree.getSelection();
                if(selected.isEmpty()) return;
                final Class demoClass = (Class) ((NamedNode)selected.get(0)).getValue();
                if(demoClass == null) return;
                try {
                    Method main = demoClass.getMethod("main", String[].class);
                    main.invoke(null, (Object)new String[0]);
                } catch (Exception ex) {
                    Loggers.get().log(ex, Logger.LEVEL_WARNING);
                }
            }
        });

        final WContainer pane = frame.getContainer();
        pane.setLayout(new BorderLayout());
        pane.addChild(tree, BorderConstraint.CENTER);
        pane.addChild(button, BorderConstraint.BOTTOM);

        frame.setVisible(true);
    }

    private static Node createNode(String name,Class demoClass){
        final DefaultNamedNode n = new DefaultNamedNode(new Chars(name), false);
        n.setValue(demoClass);
        return n;
    }

}
