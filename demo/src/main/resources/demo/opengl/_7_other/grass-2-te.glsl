
<STRUCTURE>

<LAYOUT>
layout (triangles, equal_spacing, ccw) in;

<UNIFORM>

<VARIABLE_IN>
vec4 position_model;
vec4 normal_model;
vec2 uv;

<VARIABLE_OUT>
vec4 position_model;
vec4 normal_model;
vec2 uv;

<FUNCTION>

<OPERATION>
    outData.position_model = vec4(
                    gl_TessCoord.x * inData[0].position_model.xyz
                  + gl_TessCoord.y * inData[1].position_model.xyz
                  + gl_TessCoord.z * inData[2].position_model.xyz
                  , 1);
    gl_Position = outData.position_model;

    outData.normal_model = vec4(
                    gl_TessCoord.x * inData[0].normal_model.xyz
                  + gl_TessCoord.y * inData[1].normal_model.xyz
                  + gl_TessCoord.z * inData[2].normal_model.xyz
                  , 0);

    outData.uv =    gl_TessCoord.x * inData[0].uv
                  + gl_TessCoord.y * inData[1].uv
                  + gl_TessCoord.z * inData[2].uv;