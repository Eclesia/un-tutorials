
<STRUCTURE>

<LAYOUT>
layout(triangles, invocations = 3) in;
layout(triangle_strip, max_vertices = 9) out;

<UNIFORM>
uniform mat3 M_3x3_INV_TRANSP;
uniform sampler2D textureSampler0;
uniform sampler2D textureWind;
uniform float time;

<VARIABLE_IN>
vec4 position_model;
vec4 normal_model;
vec2 uv;

<VARIABLE_OUT>
vec4 position_model;
vec4 position_world;
vec4 position_proj;
vec4 normal_model;
vec4 normal_world;
vec4 normal_proj;
vec3 normal_var;
vec2 uv;

<FUNCTION>
void buildCorner(vec4 pos, vec4 nor, vec2 uv){
    outData.uv = uv;

    outData.position_model = pos;
    outData.position_world = M * pos;
    outData.position_proj = MVP * pos;
    outData.normal_model = nor;
    outData.normal_world = M * nor;
    outData.normal_proj = MVP * nor;
    outData.normal_var = normalize(M_3x3_INV_TRANSP * nor.xyz);
    gl_Position = MVP * pos;
    EmitVertex();
}

void buildTriangle(vec4 a, vec4 b, vec4 c, vec2 uva, vec2 uvb, vec2 uvc){
    vec4 ab = b - a;
    vec4 ac = c - a;
    vec4 normal = vec4( normalize(cross(ab.xyz,ac.xyz)), 0);

    buildCorner(a,normal,uva);
    buildCorner(b,normal,uvb);
    buildCorner(c,vec4(0,1,0,0),uvc);

    EndPrimitive();
}

<OPERATION>

    //cheat on the hight using the texture green component
    float green = texture(textureSampler0, inData[0].uv).g;
    green *= 2;

    float wind = texture(textureWind, inData[0].uv*time).r;
    wind =  wind * 2 - 1; //range -1 ... +1
    wind *= 1;

    vec4 center = vec4(
                    0.333333 * inData[0].position_model.xyz
                  + 0.333333 * inData[1].position_model.xyz
                  + 0.333333 * inData[2].position_model.xyz
                  , 1) + vec4(wind,green,wind,0);

    vec2 uvc =
                    0.333333 * inData[0].uv
                  + 0.333333 * inData[1].uv
                  + 0.333333 * inData[2].uv;

    buildTriangle(inData[0].position_model, inData[1].position_model, center, inData[0].uv, inData[1].uv, uvc);
    buildTriangle(inData[1].position_model, inData[2].position_model, center, inData[1].uv, inData[2].uv, uvc);
    buildTriangle(inData[2].position_model, inData[0].position_model, center, inData[2].uv, inData[0].uv, uvc);

