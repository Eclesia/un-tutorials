
<STRUCTURE>

<LAYOUT>
layout (vertices = 3) out;

<UNIFORM>
uniform int innerfactor;
uniform int outterfactor;

<VARIABLE_IN>
vec4 position_model;
vec4 normal_model;

<VARIABLE_OUT>
vec4 position_model;
vec4 normal_model;

<FUNCTION>

<OPERATION>
    outData[gl_InvocationID].position_model = inData[gl_InvocationID].position_model;
    outData[gl_InvocationID].normal_model   = inData[gl_InvocationID].normal_model;

    //tessellation factor
    gl_TessLevelInner[0] = 100;
    gl_TessLevelOuter[0] = 100;
    gl_TessLevelOuter[1] = 100;
    gl_TessLevelOuter[2] = 100;