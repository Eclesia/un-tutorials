
<STRUCTURE>

<LAYOUT>
layout(points) in;
layout(line_strip, max_vertices=2) out;

<UNIFORM>

<VARIABLE_IN>
vec4 position_model;
vec4 normal_model;

<VARIABLE_OUT>
vec4 position_proj;
vec4 normal_model;

<FUNCTION>

<OPERATION>
    int i;

    gl_Position = MVP * inData[0].position_model;
    EmitVertex();
    gl_Position = MVP * (inData[0].position_model + vec4(0,1,0,0));
    EmitVertex();
    EndPrimitive();