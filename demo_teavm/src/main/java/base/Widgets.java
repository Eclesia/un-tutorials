package base;

import demo.ui.WidgetShowcase;
import org.teavm.jso.browser.Window;
import org.teavm.jso.dom.events.Event;
import org.teavm.jso.dom.events.EventListener;
import org.teavm.jso.dom.html.HTMLCanvasElement;
import org.teavm.jso.dom.html.HTMLDocument;
import org.teavm.jso.dom.html.HTMLElement;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.system.teavm.TeaVMFrame;

public final class Widgets {

    public static void run() throws Exception {

        final HTMLDocument document = Window.current().getDocument();
        final HTMLCanvasElement canvas = (HTMLCanvasElement) document.getElementById("canvas");

        final TeaVMFrame frame = new TeaVMFrame(canvas);
        final WContainer container = frame.getContainer();

//        final WLabel lbl = new WLabel(new Chars("Hello world"));
//        final WButton button = new WButton(new Chars("Click Click"));
//        final WButton button2 = new WButton(new Chars("Click Click 2"));
//
//        final WSplitContainer split = new WSplitContainer();
//        split.addChild(button, SplitConstraint.LEFT);
//        split.addChild(WidgetShowcase.createTreeTable(), SplitConstraint.RIGHT);

        container.setLayout(new BorderLayout());
        container.addChild(WidgetShowcase.create(), BorderConstraint.CENTER);

        //maximize
        frame.setSize(Window.current().getInnerWidth(), Window.current().getInnerHeight());

        Window.current().addEventListener("resize", new EventListener<Event>() {
            @Override
            public void handleEvent(Event evt) {
                frame.setSize(Window.current().getInnerWidth(), Window.current().getInnerHeight());
            }
        });

        final HTMLElement preview = document.getElementById("preview");
        preview.getStyle().setCssText("display:none;");
    }

}
