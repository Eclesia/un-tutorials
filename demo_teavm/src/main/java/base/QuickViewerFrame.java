
package base;

import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.event.EventSource;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.scene.SceneUtils;
import science.unlicense.display.impl.DisplayTimerState;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.light.AmbientLight;
import science.unlicense.display.impl.light.Attenuation;
import science.unlicense.display.impl.light.SpotLight;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.display.impl.updater.Updater;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.Angles;
import science.unlicense.math.api.Maths;
import science.unlicense.math.impl.Vector3f64;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 *
 * @author Johann Sorel
 */
public class QuickViewerFrame {

    protected static void updateNode(final DisplayTimerState context, final GraphicNode node){
        final Iterator iteup = node.getUpdaters().createIterator();
        while (iteup.hasNext()){
            final Updater up = (Updater) iteup.next();
            if (up==null) continue;
            up.update(context);
        }
    }

    public static void main(String[] args) {
        new QuickViewerFrame();
    }


    private final GraphicNode scene = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
    private final MonoCamera camera = new MonoCamera();
    private final MotionModel crosshair = DefaultMotionModel.createCrosshair3D(true);
    private final OrbitUpdater controller = new OrbitUpdater((EventSource) null, camera, crosshair);

    private QuickViewerFrame() {

        final DefaultModel plan = new DefaultModel();
        plan.setShape(DefaultMesh.createPlan(
                new Vector3f64(-10, 0, -10),
                new Vector3f64(-10, 0, 10),
                new Vector3f64(10, 0, 10),
                new Vector3f64(10, 0,-10)));
        GLEngineUtils.makeCompatible(plan);
        plan.getTechniques().removeAll();
        plan.getMaterials().add(SimpleBlinnPhong.newMaterial());
        plan.getTechniques().add(new SimpleBlinnPhong());

//        base.getChildren().add(model);
        scene.getChildren().add(plan);
        scene.getChildren().add(crosshair);

        final BBox bbox = SceneUtils.calculateBBox(scene, null);
        focusOn(bbox);

        camera.setNearPlane(1);
        camera.setFarPlane(10000);
        crosshair.getChildren().add(camera);

        controller.setMinDistance(0.1);
        controller.setDistance(10);
        controller.setVerticalAngle(Angles.degreeToRadian(15));
        controller.setHorizontalAngle(Angles.degreeToRadian(15));
        controller.configureDefault();
        camera.getUpdaters().add(controller);

        GLEngineUtils.makeCompatible(scene);
        camera.setVerticalFlip(false);

        final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, false);
        final GLProcessContext context = frame.getContext();
        frame.addEventListener(MouseMessage.PREDICATE, controller);

        context.getPhases().add(new ClearPhase());
        context.getPhases().add(new UpdatePhase(scene));
        context.getPhases().add(new RenderPhase(scene,camera));

        //show the frame
        frame.setSize(1024, 768);
        frame.setVisible(true);

    }

    private void focusOn(BBox bbox) {
        if (bbox == null) return;

        //create a target node at the center of the bbox
        crosshair.getNodeTransform().getTranslation().set(bbox.getMiddle());
        crosshair.getNodeTransform().notifyChanged();


        //set camera at good distance
        final double fov = 45;
        final double spanX = bbox.getSpan(0);
        final double distX = spanX / Math.tan(fov);
        final double spanY = bbox.getSpan(1);
        final double distY = spanY / Math.tan(fov);
        // x2 because screen space is [-1...+1]
        // x1.2 to compensate perspective effect
        final float dist = (float) (Maths.max(distX,distY) * 2.0 * 1.2);

        controller.setDistance(dist);

        //some lightning
        scene.getChildren().add(new AmbientLight(Color.GRAY_LIGHT,Color.GRAY_LIGHT));

        final SpotLight spot = new SpotLight();
        spot.setAttenuation(new Attenuation(1, 0.2f, 0.2f));
        spot.getNodeTransform().getTranslation().setXYZ(0,12,-4);
        spot.getNodeTransform().getTranslation().localScale(1.1);
        spot.getNodeTransform().notifyChanged();
        scene.getChildren().add(spot);

    }

}
