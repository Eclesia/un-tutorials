
package base;

/**
 *
 * @author Johann Sorel
 */
public class Main {

    public static void main(String[] args) throws Exception {
        science.unlicense.runtime.Runtime.init();
        widgets();
    }

    public static void widgets() throws Exception {
        System.out.println("laaaaa1");
        Widgets.run();
    }

    public static void webgl() throws Exception {
        System.out.println("laaaaa2");
        WebGL.run();
    }
}
